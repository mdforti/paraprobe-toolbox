#!/bin/bash

# MAKE SURE to have step 01. and 02. completed before you call this script!
# EXECUTE starting from the paraprobe-toolbox directory

cd code
cd thirdparty/mandatory

# if the cmake version on the host system is >=3.18.4 nothing has to be done
# otherwise cmake should be installed, preferentially using the newest version
# this can be achieved with a local installation as follows

# versions of tools
export MYHDF_VERSION_MAJOR="1"
export MYHDF_VERSION_MINOR="14"
export MYHDF_VERSION_RELEASE="2"
# export MYHDF_VERSION_BUILD="2"
export MYHDF_VERSION="${MYHDF_VERSION_MAJOR}.${MYHDF_VERSION_MINOR}.${MYHDF_VERSION_RELEASE}"
echo "Using HDF5 $MYHDF_VERSION"
export MYFFTW_VERSION_MAJOR="3"
export MYFFTW_VERSION_MINOR="3"
export MYFFTW_VERSION_RELEASE="10"
export MYFFTW_VERSION="${MYFFTW_VERSION_MAJOR}.${MYFFTW_VERSION_MINOR}.${MYFFTW_VERSION_RELEASE}"
echo "Using FFTW $MYFFTW_VERSION"
export MYBOOST_VERSION_MAJOR="1"
export MYBOOST_VERSION_MINOR="85"
export MYBOOST_VERSION_RELEASE="0"
export MYBOOST_VERSION="${MYBOOST_VERSION_MAJOR}.${MYBOOST_VERSION_MINOR}.${MYBOOST_VERSION_RELEASE}"
export MYBOOST_VERSION_UDSCR="${MYBOOST_VERSION_MAJOR}_${MYBOOST_VERSION_MINOR}_${MYBOOST_VERSION_RELEASE}"
echo "Using boost $MYBOOST_VERSION"
export MYEIGEN_VERSION_MAJOR="3"
export MYEIGEN_VERSION_MINOR="4"
export MYEIGEN_VERSION_RELEASE="0"
export MYEIGEN_VERSION="${MYEIGEN_VERSION_MAJOR}.${MYEIGEN_VERSION_MINOR}.${MYEIGEN_VERSION_RELEASE}"
echo "Using eigen $MYEIGEN_VERSION"
export MYCGAL_VERSION_MAJOR="5"
export MYCGAL_VERSION_MINOR="6"
export MYCGAL_VERSION_RELEASE="1"
export MYCGAL_VERSION="${MYCGAL_VERSION_MAJOR}.${MYCGAL_VERSION_MINOR}.${MYCGAL_VERSION_RELEASE}"
echo "Using CGAL $MYCGAL_VERSION"


mkdir -p hdf5 && cd hdf5
wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-$MYHDF_VERSION_MAJOR.$MYHDF_VERSION_MINOR/hdf5-$MYHDF_VERSION/src/CMake-hdf5-$MYHDF_VERSION.tar.gz
# unfortunately oftentimes these release tar.gz archives are not proper tar.gz archives but double-packed
# in this case a two-stepped unpacking procedure is required like so
mkdir CMake-hdf5-$MYHDF_VERSION
gzip -d CMake-hdf5-$MYHDF_VERSION.tar.gz  # for some <1.14.3 version needed
tar -xvf CMake-hdf5-$MYHDF_VERSION.tar.gz
cd CMake-hdf5-$MYHDF_VERSION
./build-unix.sh 2>&1 | tee PARAPROBE.HDF5.Build.STDOUTERR.txt
cd build && ./HDF5-$MYHDF_VERSION-Linux.sh --include-subdir --skip-license 2>&1 | tee ../PARAPROBE.HDF5.Install.STDOUTERR.txt
cd ../../../


mkdir -p fftw && cd fftw
wget www.fftw.org/fftw-$MYFFTW_VERSION.tar.gz
tar -xvf fftw-$MYFFTW_VERSION.tar.gz && cd fftw-$MYFFTW_VERSION
mkdir -p localinstallation
./configure --prefix=$PWD/localinstallation --enable-threads --enable-openmp --enable-avx2 --enable-avx512 2>&1 | tee FFTW.Configure.STDOUTERR.txt  # --enable-float --enable-sse
make -j8 2>&1 | tee FFTW.Make.STDOUTERR.txt
make check 2>&1 | tee FFTW.MakeCheck.STDOUTERR.txt
make install 2>&1 | tee FFTW.Install.STDOUTERR.txt
cd ../../


mkdir -p boost && cd boost
wget https://boostorg.jfrog.io/artifactory/main/release/$MYBOOST_VERSION/source/boost_$MYBOOST_VERSION_UDSCR.tar.gz
tar -xvf boost_$MYBOOST_VERSION_UDSCR.tar.gz && cd boost_$MYBOOST_VERSION_UDSCR
cd ../../


mkdir -p eigen && cd eigen
wget https://gitlab.com/libeigen/eigen/-/archive/$MYEIGEN_VERSION/eigen-$MYEIGEN_VERSION.tar.gz
tar -xvf eigen-$MYEIGEN_VERSION.tar.gz && cd eigen-$MYEIGEN_VERSION
cd ../../


mkdir -p voroxx && cd voroxx
git clone https://github.com/chr1shr/voro.git && cd voro
git checkout 56d619faf3479313399516ad71c32773c29be859
# ##MK::improve this version handshaking in the future
cd ../../


mkdir -p cgal && cd cgal
wget https://github.com/CGAL/cgal/releases/download/v$MYCGAL_VERSION/CGAL-$MYCGAL_VERSION.tar.xz
tar -xvf CGAL-$MYCGAL_VERSION.tar.xz && cd CGAL-$MYCGAL_VERSION
cd ../../

# the parsers for the atom probe file formats were already installed into the
# paraprobe conda environment via the ifes-apt-tc-data-modeling repository

# functionalities for TetGen have been deactivated in the code as they are meant
# for expert users only
# users who wish to use these functionalities should open up a GitLab issue
# and shortly explain for which data analysis tasks they would like to use
# tetrahedralization capabilities, alternatively users should write me an email

# paraprobe-crystalstructure has been removed with v0.4
# it may become reintroduced with later version
# users who wish to use these functionalities should open up a GitLab issue
# and shortly explain for which data analysis tasks they would like to use
# crystal-structure analysis, alternatively users should write me an email

cd ../../
cd ../ && echo $PWD

# finally we should load the NeXus definitions which the particular version of paraprobe-toolbox uses
echo "Loading specific NeXus data models used in this paraprobe-toolbox version"
git submodule update --init --recursive


echo "Setting up all prerequisites for paraprobe-toolbox was successful!"
echo "Using the following paraprobe-toolbox version" && git rev-parse HEAD
