/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef __PARAPROBE_SPATSTAT_METADATA_H__
#define __PARAPROBE_SPATSTAT_METADATA_H__

#include "../../../paraprobe-utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"

/*
#define MYSPS									"/SpatialStatisticsID"
	#define MYSPS_META								"/Metadata"
	#define MYSPS_DATA								"/Data"

	#define MYSPS_DATA_TSKS							"/Data/SpatialStatisticsTask"
	//for each task
	    #define MYSPS_DATA_TSKS_ITYPRND				"IonTypeRandomized"
	    //##MK::add windowing information
	    #define MYSPS_DATA_TSKS_SRCQTYP				"IonTypeSourceInterpretation"
	    #define MYSPS_DATA_TSKS_TRGQTYP				"IonTypeTargetInterpretation"
	    #define MYSPS_DATA_TSKS_SRC					"SourceFilter"
	    //for each ion
	    #define MYSPS_DATA_TSKS_TRG					"TargetFilter"
	    //for each ion
	    #define MYSPS_DATA_TSKS_DEDG				"EdgeDistance"
	    #define MYSPS_DATA_TSKS_DFET				"FeatureDistance"
	    #define MYSPS_DATA_TSKS_WHAT				"WhichStatistic"
	    #define MYSPS_DATA_TSKS_KNN					"KThNearestNeighbor"
	    //for each a detailed setting
	    	#define MYSPS_DATA_TSKS_KNN_KTH			"KThOrder"
	    	#define MYSPS_DATA_TSKS_KNN_BIN			"Binning"

	    	#define MYSPS_DATA_TSKS_KNN_HST			"PdfAndCdf"
			#define MYSPS_DATA_TSKS_KNN_ERR			"ErrorCounts"

		#define MYSPS_DATA_TSKS_RDF					"RadialDistributionFunction"
		//for each a detailed setting
	    	#define MYSPS_DATA_TSKS_RDF_BIN			"Binning"
	    	#define MYSPS_DATA_TSKS_RDF_HST			"PdfAndCdf"
			#define MYSPS_DATA_TSKS_RDF_ERR			"ErrorCounts"
*/

#endif
