/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SpatstatStructs.h"


ostream& operator << (ostream& in, query_info const & val)
{
	in << "ionidx " << val.ionidx << "\n";
	in << "src_mult " << (int) val.src_mult << "\n";
	in << "trg_mult " << (int) val.trg_mult << "\n";
	if ( val.far_from_dedge == true )
		in << "far_from_dedge " << "yes" << "\n";
	else
		in << "far_from_dedge " << "no" << "\n";
	if ( val.close_to_dfeat == true )
		in << "close_to_dfeat " << "yes" << "\n";
	else
		in << "close_to_dfeat " << "no" << "\n";
	return in;
}
