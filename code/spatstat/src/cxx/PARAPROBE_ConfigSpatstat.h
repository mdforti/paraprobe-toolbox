/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_CONFIG_SPATSTAT_H__
#define __PARAPROBE_CONFIG_SPATSTAT_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"


enum SPATIAL_STATISTIC {
	SPATSTAT_NONE,
	SPATSTAT_KNN,
	SPATSTAT_RDF,
	SPATSTAT_RPK,
	SPATSTAT_TPS
};
//k-nearest neighbor, radial distribution function, Ripley K, two-point statistics


struct knn_info
{
	apt_uint kth;							//neighbor order, e.g. kth=1 for first order nearest order, kth=0 is special case that gets ignored
	lival<apt_real> hist1d;
	knn_info() : kth(1), hist1d(lival<apt_real>(MYZERO, 0.001, MYZERO)) {}
	knn_info( const apt_uint _kth, const apt_real _rincr, const apt_real _rmax ) :
		kth(_kth), hist1d(lival<apt_real>(MYZERO, _rincr, _rmax)) {}
};

ostream& operator << (ostream& in, knn_info const & val);


struct rdf_info
{
	lival<apt_real> hist1d;
	rdf_info() : hist1d(lival<apt_real>(MYZERO, MYZERO, MYZERO)) {}
	rdf_info( const apt_real _rincr, const apt_real _rmax ) :
		hist1d(lival<apt_real>(MYZERO, _rincr, _rmax)) {}
};

ostream& operator << (ostream& in, rdf_info const & val);


//implement RipleyK and two-point statistics

enum ION_TYPE_RANDOMIZED
{
	RANDOMIZE_NO,
	RANDOMIZE_YES
};


enum ION_QUERY_TYPE {
	RESOLVE_NONE,
	RESOLVE_ALL,
	RESOLVE_UNKNOWN,
	RESOLVE_ION,
	RESOLVE_ELEMENT,
	RESOLVE_ISOTOPE
	//none, ignore ion, regardless its type, multiplicity is 0 for all
	//all, consider ion, regardless its type, multiplicity is 1 for all
	//unknowntype, consider ion only if it is of the unknown type, multiplicity is 1 for ions of unknown type, 0 for ions of all other types
	//ion, consider ion "not atomically decomposed", i.e. consider an ion if it has exactly the same isotope vector (and charge if specified),
	//		multiplicity of ions is 1 is considered, 0 otherwise
	//element, consider ion "atomically decomposed", an ion is taken (multiplicity) as many times as it has elements in isotope vector"
	//isotope, ions "atomically decomposed, an ion is taken (multiplicity) as many times as it has isotopes in isotope vector"
};


struct spatstat_task
{
	apt_uint taskid;							//taskID

	WINDOWING_METHOD WdwMethod;
	vector<roi_sphere> WdwSpheres;
	vector<roi_rotated_cylinder> WdwCylinders;
	vector<roi_rotated_cuboid> WdwCuboids;
	//vector<roi_polyhedron> WdwPolyhedra;
	lival<apt_uint> LinearSubSamplingRange;
	match_filter<unsigned char> IontypeFilter;
	match_filter<unsigned char> HitMultiplicityFilter;

	ION_QUERY_TYPE src_qtyp;				//how to interpret iontypes on the source side (i.e. where ROIs are placed) into a counting multiplicity
	ION_QUERY_TYPE trg_qtyp;				//how to interpret iontypes on the target side (the neighbors within ROIs) into a counting multiplicity
	vector<ion> src;						//reference object(s) in the center for which to compute the statistics
	vector<ion> trg;						//target object(s) in the vicinity to hunt after
	ION_TYPE_RANDOMIZED randomized;			//will the original or randomized labels be used?
	SPATIAL_STATISTIC statstyp;				//which type of statistics?
	knn_info knn_ifo;
	rdf_info rdf_ifo;
	apt_real far_from_dedge;				//distance to the edge of the dataset, only consider src ions if their ion-to-edge distance is >= far_from_dedge,
											//if far_from_dedge -> MYZERO basically taking every ion, which creates bias/an edge effect
	apt_real close_to_dfeat;				//distance to the feature, only consider src ions if their ion-to-feature distance <= close_to_dfeat,
											//if distance = close_to_dfeat still take such ions but if distance = close_to_dfeat+epsilon or larger do not take such ions
	bool IOPdfAndCdf;						//default report e.g. histogram and cdf for kNN
	bool IODescriptor;						//optional, different interpretation for different statistics, e.g. enables to report shortest distance to neighbor in case of statstyp == SPATSTAT_KNN
	spatstat_task() :
		taskid(UMX),
		WdwMethod(ENTIRE_DATASET),
		WdwSpheres(vector<roi_sphere>()),
		WdwCylinders(vector<roi_rotated_cylinder>()),
		WdwCuboids(vector<roi_rotated_cuboid>()),
		LinearSubSamplingRange(lival<apt_uint>( 0, 1, UMX )),
		IontypeFilter(match_filter<unsigned char>()),
		HitMultiplicityFilter(match_filter<unsigned char>()),
		src_qtyp(RESOLVE_NONE), trg_qtyp(RESOLVE_NONE),
		src(vector<ion>()), trg(vector<ion>()),
		randomized(RANDOMIZE_NO),
		statstyp(SPATSTAT_NONE),
		knn_ifo(knn_info()),
		rdf_ifo(rdf_info()),
		far_from_dedge(MYZERO),
		close_to_dfeat(MYZERO),
		IOPdfAndCdf(true), IODescriptor(false) {}
};

ostream& operator << (ostream& in, spatstat_task const & val);


class ConfigSpatstat
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	static string InputfileIonToEdgeDistances;
	static string IonToEdgeDistancesDatasetName;
	static string InputfileIonToFeatureDistances;
	static string IonToFeatureDistancesDatasetName;

	static vector<spatstat_task> SpatialStatisticsTasks;

	//sensible defaults
	static string PRNGType;
	static size_t PRNGWarmup;
	static long PRNGWorldSeed;
};

#endif
