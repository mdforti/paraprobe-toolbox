/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SPATSTAT_HDL_H__
#define __PARAPROBE_SPATSTAT_HDL_H__


#include "PARAPROBE_SpatstatXDMF.h"


class spatstatHdl : public mpiHdl
{
public:
	spatstatHdl();
	~spatstatHdl();

	bool read_relevant_input();
	bool read_ion2edge_distances();
	bool read_ion2feature_distances();
	void randomize_iontype_labels();

	void crop_reconstruction_to_analysis_window( spatstat_task const & ifo );
	void analyse_role_of_ions( spatstat_task const & ifo );
	void build_query_kdtree( spatstat_task const & ifo );
	void compute_statistics( spatstat_task const & ifo );

	void execute_spatial_statistics_workpackage();

	int write_knn_results( histogram & hst, spatstat_task const & ifo );
	int write_rdf_results( histogram & hst, spatstat_task const & ifo );

	bool* window;

	vector<apt_real> ions_ion2feat;

	vector<query_info> cand;
	kdTree<p3dm5> kauri;

	profiler spatstat_tictoc;
};

#endif
