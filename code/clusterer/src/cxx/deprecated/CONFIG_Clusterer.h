//##MK::GPLV3

#ifndef __PARAPROBE_CONFIG_CLUSTERER_H__
#define __PARAPROBE_CONFIG_CLUSTERER_H__

#include "PARAPROBE_ClustererMetadata.h"


class ConfigClusterer
{
public:
	static bool read_config_from_h5( const string h5fn );
	static bool check_config();
	static void report_config( vector<pparm> & res );
	
	static string InputfileClusterDefs;
	static apt_uint DatasetIDClusterDefs;
	static string InputfileRecon;
	static apt_uint DatasetIDRecon;
	static string OutputfilePrefix;
	static apt_uint DatasetID;

	static bool IOComputeConvexHull;
	static bool IORecoverEvaporationSequenceID;

	static apt_real DefaultEpsBallRadius;
};

#endif
