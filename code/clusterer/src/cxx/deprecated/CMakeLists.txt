cmake_minimum_required(VERSION 3.18.4)

################################################################################################################################
##DEVELOPER SECTION#############################################################################################################
##in this section software developers need to make changes when debugging#######################################################
################################################################################################################################
#please name your project accordingly
set(MYPROJECTNAME "paraprobe_clusterer")
message([STATUS] "Projectname is ${MYPROJECTNAME}")
project(${MYPROJECTNAME} LANGUAGES CXX)
set(CMAKE_BUILD_DIR "build")


#pull general information about external libraries and paths related to paraprobe
include("../PARAPROBE.Dependencies.cmake")
message([STATUS] "MYPROJECTPATH: ${MYPROJECTPATH}")
message([STATUS] "MYUTILSPATH: ${MYUTILSPATH}")
message([STATUS] "MYHDF5PATH: ${MYHDFPATH}")
message([STATUS] "MYBOOSTPATH: ${MYBOOSTPATH}")
message([STATUS] "MYCGALPATH: ${MYCGALPATH}")
message([STATUS] "MYEIGENPATH: ${MYEIGENPATH}")
message([STATUS] "MYVOROXXPATH: ${MYVOROXXPATH}")

#compose a tool-specific path
set(MYTOOLPATH "${MYPROJECTPATH}/code/paraprobe-clusterer/")
message([STATUS] "MYTOOLPATH: ${MYTOOLPATH}")

set(EMPLOY_GNUCOMPILER ON)
message([STATUS] "MYCCC_COMPILER: __${CMAKE_C_COMPILER}__")
message([STATUS] "MYCXX_COMPILER: __${CMAKE_CXX_COMPILER}__")

#define which parallelization layers are used
set(EMPLOY_PARALLELISM_CUDA OFF)

#choose optimization level
##-O0 nothing, debug purposes, -O1 moderate optimization, -O2 -O3 for production level up to aggressive architecture specific non-portable optimization
if(EMPLOY_GNUCOMPILER)
	set(MYOPTLEVEL "-O2")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MYOPTLEVEL}")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MYOPTLEVEL}")
	message([STATUS] "We utilize optimization level ${MYOPTLEVEL}")
endif()


################################################################################################################################
##END OF INTERACTION FOR NON PRO USERS##########################################################################################
##here advanced users might want/need to make modifications if they use non default places for thirdparty libraries#############
################################################################################################################################
#HDF5 local installation for advanced I/O, collecting metadata and bundle analysis results together
include_directories("${MYHDFPATH}/include")
link_directories("${MYHDFPATH}/lib")
set(MYHDFLINKFLAGS "-L${MYHDFPATH}/lib/ ${MYHDFPATH}/lib/libhdf5_hl.a ${MYHDFPATH}/lib/libhdf5.a ${MYHDFPATH}/lib/libz.a ${MYHDFPATH}/lib/libszaec.a ${MYHDFPATH}/lib/libaec.a -ldl")

set(Boost_INCLUDE_DIR "${MYBOOSTPATH}")
#include_directories("${MYCGALPATH}")
set(CGAL_DIR "${MYCGALPATH}")
include_directories("${MYEIGENPATH}")
include_directories("${MYVOROXXPATH}/src")

#GMP and MPFR is included and found alongside the CGAL library
#connect with ExternalLibraries.cmake via MYCGALHOME and MYEIGENHOME
find_package(CGAL QUIET)
if( CGAL_FOUND )
	include( ${CGAL_USE_FILE} )
	if (EMPLOY_MYEIGEN)
		find_package(Eigen3 3.1.0 REQUIRED) #(3.1.0 or greater)
		include(CGAL_Eigen3_support)
		if(NOT TARGET CGAL::Eigen3_support)
			message(STATUS "This project requires the Eigen library, and will not be compiled.")
			return()
		endif()
	endif()
endif()

#connect with ExternalLibraries.cmake via MYBOOSTHOME
#include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR})
find_package(Boost)

#setting up compiler-specifics
if(EMPLOY_GNUCOMPILER)
	set(MYVERBOSE "-fopt-info")
	add_definitions("${MYOPTLEVEL} ${MYVERBOSE}")
	set(MYOMP "-fopenmp -lpthread")
	add_definitions(${MYOMP})
	add_definitions("-std=c++1z")
	add_definitions("-Wall -Warray-bounds -Wchar-subscripts -Wcomment -Wenum-compare -Wformat 
		-Wuninitialized -Wmaybe-uninitialized -Wmain -Wnonnull -Wparentheses -Wreorder -Wreturn-type -Wsign-compare -Wsequence-point 
		-Wtrigraphs -Wunused-function -Wunused-but-set-variable -Wunused-variable") #-Wnarrowing
endif()

#parallelization - MPI process-level
#query location of MPI library
message([STATUS] "MPI_INCLUDE_PATH ${MPI_INCLUDE_PATH}")
if(EMPLOY_PARALLELISM_MPI)
	find_package(MPI REQUIRED)
	include_directories(${MPI_INCLUDE_PATH})
endif()

#specific paths of dependencies for this tool
set(MYTOOLSRCPATH "${MYTOOLPATH}/src/cxx")

#list firstly the precompiled shared aka utils, secondly the tool-specific components, lastly the tool-specific main
add_executable(${MYPROJECTNAME}
	${MYUTILSPATH}/../../${MYPROJECTPATH}code/thirdparty/mandatory/hdf5cxxwrapper/src/cxx/hdf5_wrapper_cxx_sequ_core.cpp.o
	${MYUTILSPATH}/../../${MYPROJECTPATH}code/thirdparty/mandatory/hdf5cxxwrapper/src/cxx/hdf5_wrapper_cxx_structs.cpp.o
	${MYUTILSPATH}/CONFIG_Shared.cpp.o
	${MYUTILSPATH}/PARAPROBE_CiteMe.cpp.o
	${MYUTILSPATH}/PARAPROBE_Verbose.cpp.o
	${MYUTILSPATH}/PARAPROBE_Profiling.cpp.o
	${MYUTILSPATH}/PARAPROBE_Math.cpp.o
	${MYUTILSPATH}/PARAPROBE_PrimsContinuum.cpp.o
	${MYUTILSPATH}/PARAPROBE_PrimsDiscrete.cpp.o
	${MYUTILSPATH}/PARAPROBE_Geometry.cpp.o
	${MYUTILSPATH}/PARAPROBE_UserStructs.cpp.o
	${MYUTILSPATH}/PARAPROBE_UserStructsSorting.cpp.o
	${MYUTILSPATH}/PARAPROBE_MPIStructs.cpp.o
	${MYUTILSPATH}/PARAPROBE_PeriodicTable.cpp.o
	${MYUTILSPATH}/PARAPROBE_Crystallography.cpp.o
	${MYUTILSPATH}/PARAPROBE_OriMath.cpp.o
	${MYUTILSPATH}/PARAPROBE_Histogram2D.cpp.o
	${MYUTILSPATH}/PARAPROBE_TwoPointStats3D.cpp.o
	${MYUTILSPATH}/PARAPROBE_ROIs.cpp.o
	${MYUTILSPATH}/PARAPROBE_AABBTree.cpp.o
	${MYUTILSPATH}/PARAPROBE_KDTree.cpp.o
	${MYUTILSPATH}/PARAPROBE_VolumeBinning.cpp.o
	${MYUTILSPATH}/PARAPROBE_IonCloudMemory.cpp.o
	${MYUTILSPATH}/PARAPROBE_HDF5BaseHdl.cpp.o
	${MYUTILSPATH}/PARAPROBE_XDMFBaseHdl.cpp.o
	${MYUTILSPATH}/PARAPROBE_TriangleSoupHdl.cpp.o
	${MYUTILSPATH}/PARAPROBE_ToolsBaseHdl.cpp.o
	${MYUTILSPATH}/PARAPROBE_ConfigBaseHDF5.cpp.o

	#${MYUTILSPATH}/../../${MYPROJECTPATH}code/thirdparty/mandatory/hornus2017/intersection-detection-master/vec.cpp
	#${MYUTILSPATH}/../../${MYPROJECTPATH}code/thirdparty/mandatory/hornus2017/intersection-detection-master/convexes.cpp
	${MYTOOLSRCPATH}/CONFIG_Clusterer.cpp
	${MYTOOLSRCPATH}/PARAPROBE_ClustererStructs.cpp
	#${MYTOOLSRCPATH}/PARAPROBE_ClustererGPU.cu
	${MYTOOLSRCPATH}/PARAPROBE_ClustererMeshing.cpp
	${MYTOOLSRCPATH}/PARAPROBE_ClustererHDF5.cpp
	${MYTOOLSRCPATH}/PARAPROBE_ClustererXDMF.cpp
	${MYTOOLSRCPATH}/PARAPROBE_ClustererHdl.cpp
	${MYTOOLSRCPATH}/PARAPROBE_Clusterer.cpp
)

#linking process, the target link libraries command is specific for each tool of the toolbox
#target_link_libraries( ${MYPROJECTNAME} PUBLIC CGAL::Eigen3_support -lgomp -lpthread ${MPI_LIBRARIES} ${MYTETGENLINKFLAGS} ${MYHDFLINKFLAGS} -lgmp -lm ${Boost_LIBRARIES}  ) #${MYOPENACCLINK} ${MYCUFFTLINKFLAGS}

target_link_libraries(${MYPROJECTNAME} ${MYOMP} ${MPI_LIBRARIES} ${MYHDFLINKFLAGS}) #-lgomp -lpthread -lm ${Boost_LIBRARIES}

#MPI compilation settings
if(MPI_COMPILE_FLAGS)
	set_target_properties(${MYPROJECTNAME} PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
	set_target_properties(${MYPROJECTNAME} PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()
