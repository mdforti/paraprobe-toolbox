//##MK::GPLV3


#include "PARAPROBE_ClustererHdl.h"


clustererHdl::clustererHdl()
{
}


clustererHdl::~clustererHdl()
{
	for( auto it = objs.begin(); it != objs.end(); it++ ) {
		if ( it->second != NULL ) {
			delete it->second;
			it->second = NULL;
		}
	}
	objs = map<apt_uint,cluster*>();
}


bool clustererHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigClusterer::DatasetIDRecon, ConfigClusterer::InputfileRecon ) == true  ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//##MK::ions.ionifo is not needed

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		//clust_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;

}


bool clustererHdl::read_relevant_input_ametek()
{
	//load cluster from H5 file that was created using Python parsed from the result of IVAS/AP Suite cluster.indexed.pos / cluster.indexed.rrng files
	double tic = MPI_Wtime();
	double toc = 0.;

	string fwslash = "/";
	string prefix = "AMETEKClusterAnalysisID" + to_string(ConfigClusterer::DatasetIDClusterDefs) + fwslash + "Data";
	string dsnm = "";
	h5seqHdl h5r;
	h5r.use_file( ConfigClusterer::InputfileClusterDefs );

	//get to know the number of cluster
	dsnm = prefix + fwslash + "Cluster" + fwslash + "NumberOfDisjointCluster"; //##MK::assume for now cluster are indexed on [1, 1, NumberOfDisjointCluster]
	vector<unsigned int> u32;
	if ( h5r.read_contiguous_array1d( dsnm,  H5_U32, u32 ) != MYHDF5_SUCCESS ) {
		cerr << "Loading " << dsnm << " failed !" << "\n";
	}
	if ( u32.size() < 1 ) {
		cerr << "The dataset indicates there are no cluster !" << "\n";
		return true;
	}

	apt_uint ncluster = u32.at(0);
	objs = map<apt_uint,cluster*>();
	for( apt_uint clid = 1; clid <= ncluster; clid++ ) {
		cluster* thisone = NULL;
		thisone = new cluster;

		if ( thisone != NULL ) {
			thisone->clusterid = clid;
			objs[clid] = thisone;
		}
		else {
			cerr << "Allocation of a cluster object failed !" << "\n"; return false;
		}
	}

	//load cluster.indexed.pos ion positions
	//##MK::the number of ions in cluster is normally an order of magnitude if not more lower than the number of ions in the dataset
	//##MK::so load xyz, and mq here traditionally into memory
	dsnm = prefix + fwslash + "IonPositions";
	vector<apt_real> r_xyz;
#ifdef EMPLOY_SINGLE_PRECISION
	if ( h5r.read_contiguous_array2d( dsnm,  H5_F32, r_xyz) != MYHDF5_SUCCESS ) {
#else
	if ( h5r.read_contiguous_array2d( dsnm,  H5_F64, r_xyz) != MYHDF5_SUCCESS ) {
#endif
		cerr << "Reading " << dsnm << " failed !" << "\n"; return false;
	}

	dsnm = prefix + fwslash + "MassToChargeRatio";
	vector<apt_real> r_mq;
#ifdef EMPLOY_SINGLE_PRECISION
	if ( h5r.read_contiguous_array1d( dsnm,  H5_F32, r_mq) != MYHDF5_SUCCESS ) {
#else
	if ( h5r.read_contiguous_array1d( dsnm,  H5_F64, r_mq) != MYHDF5_SUCCESS ) {
#endif
		cerr << "Reading " << dsnm << " failed !" << "\n"; return false;
	}

	if ( r_xyz.size()/3 != r_mq.size() ) {
		cerr << "The number of ion positions is inconsistent with the number of mass-to-charge ratios !" << "\n";
		return false;
	}

	ametek_ion_in_cluster = vector<pair<p3d,apt_real>>();
	ametek_ion_in_cluster.reserve( r_xyz.size()/3 );
	for( size_t i = 0; i < r_xyz.size(); i += 3 ) {
		ametek_ion_in_cluster.push_back( pair<p3d,apt_real>( p3d(r_xyz[i+0], r_xyz[i+1], r_xyz[i+2]), r_mq[i/3]) );
	}

	return true;
}


bool clustererHdl::reconstruct_clusters_computed_ametek()
{
	//##MK::assume for now that for cluster.indexed.pos and cluster.indexed.rrng AMETEK makes the convention that the mass-to-charge ratio encodes
	//##MK::the cluster ID in the following way clusterid = int( mq );
	//##MK::load mass-to-charge ratio interval definitions and build a BVH from it, query it then
	for( auto it = ametek_ion_in_cluster.begin(); it != ametek_ion_in_cluster.end(); it++ ) {

		apt_uint clid = (apt_uint) it->second;	//##MK::clusterID start with index one
		map<apt_uint,cluster*>::iterator thisone = objs.find( clid );
		if ( thisone != objs.end() ) {
			if ( thisone->second != NULL ) {
				thisone->second->ions.push_back( p3dm1(it->first.x, it->first.y, it->first.z, UMX) ); //##MK::UMX evaporation ID not yet identified
			}
			else {
				cerr << "No memory was allocated for cluster " << thisone->second->clusterid <<  " !" << "\n"; return false;
			}
		}
		else {
			cerr << "Cluster was not found in dictionary for cluster " << clid << " with mq " << it->second << " !" << "\n"; return false;
		}
	} //next ion not assigned a cluster
	return true;
}


bool clustererHdl::recover_evaporation_sequence_id()
{
	//POS files do not store the evaporation ID, therefore we cannot simply search ions based on ids
	//instead we need to check for each ion (query ion) in a cluster whether there is or not exactly a single ion position from the reconstruction inside an
	//which is in an eps ball about the query ion
	//MK::we must not naively query floating point values directly like
	//query.x == cand.x && query.y = cand.y && query.z == cand.z because this is not robust numerically
	//the situation can be very efficiently solved with a kdtree though because the query region is very small compared to the volume of the point cloud
	//and hence provided the kd tree is well balanced and the number of ions in each leaf small only a few candidates will be returned so that the
	//complexity of the evaporation sequence id recovery algorithm should be O(N_ions_in_cluster * log(N_ions_in_recon))

	double tic = MPI_Wtime();
	double toc = 0.;

	//build a kdtree of the ions in ions.ionpp3
	kdtree bvh;
	if ( bvh.build( ions ) == true ) {
		cout << "Successfully built a kd_tree for all ions in the reconstruction" << "\n";
	}
	else {
		cerr << "Building a kd_tree for all ions in the reconstructions failed !" << "\n";
		return false;
	}

	for( auto it = objs.begin(); it != objs.end(); it++ ) {

		apt_uint clid = it->first;	//##MK::clusterID start with index one
		if ( it->second != NULL ) {
			for( auto jt = it->second->ions.begin(); jt != it->second->ions.end(); jt++ ) {
				p3d here = p3d(jt->x, jt->y, jt->z);

				//query a kd_tree
				vector<p3dm1> cand;
				bvh.query( here, SQR(ConfigClusterer::DefaultEpsBallRadius), cand );

				if ( cand.size() > 0 ) {
					apt_real BestDistSQR = RMX;
					apt_uint BestIonIdx = UMX;
					for( auto kt = cand.begin(); kt != cand.end(); kt++ ) {
//cout << "\t\t" << *kt << "\n";
						apt_real CurrDistSQR = SQR(here.x - kt->x)+SQR(here.y - kt->y)+SQR(here.z - kt->z);
						if ( CurrDistSQR <= BestDistSQR ) {
							BestDistSQR = CurrDistSQR;
							BestIonIdx = kt->m;
						}
					}
//cout << "BestDistSQR\t\t" << BestDistSQR << " cand.size() " << cand.size() << " bestionidx " << BestIonIdx << "\n";
					jt->m = BestIonIdx;
				}
				else {
					cerr << "There is an inconsistence between the datasets, because we do not find an ion in the reconstruction within the query distance" << "\n";
					return false;
				}
			} //next ion in the same cluster
		}
	} //next cluster

	//release resources
	bvh.clear();

	toc = MPI_Wtime();
	cout << "Recovering evaporation IDs took " << (toc-tic) << " s" << "\n";
	return true;
}


void clustererHdl::execute_cluster_analysis_workpackage()
{
	double tic = MPI_Wtime();
	double toc = 0.;

	//##MK::development version does not support distributing of the work so far
	if ( get_nranks() > 1 ) {
		cerr << "Rank " << get_myrank() << " current implementation does not support multi-process parallelism for characterizing the geometry of cluster !" << "\n";
		return;
	}


	if ( ConfigClusterer::IOComputeConvexHull == true ) {

		//load ion positions and mass-to-charge coming from the results of a clustering analysis performed withinclusterer detected using methods executed inside commercial IVAS or AP Suite software
		//IVAS/APSuite
		if ( read_relevant_input_ametek() == true ) {
			//
		}
		else {
			cerr << "Reading relevant cluster ion positions and their mass-to-charge state values failed !" << "\n";
			return;
		}


		if ( ConfigClusterer::IORecoverEvaporationSequenceID == true ) {

			//load ion positions and mass-to-charge from the reconstruction for which the clustering analysis was performed
			if ( read_relevant_input() == true ) {

			}
			else {
				cerr << "Reading original reconstruction for which the clustering analysis was performed failed !" << "\n";
				return;
			}
		}


		if ( reconstruct_clusters_computed_ametek() == true ) {
			//
		}
		else {
			cerr << "Reconstructing cluster from an AMETEK software analysis failed !" << "\n";
			return;
		}


		if ( ConfigClusterer::IORecoverEvaporationSequenceID == true ) {

			if ( recover_evaporation_sequence_id() == true ) {
				//
			}
			else {
				cerr << "Recovering evaporation IDs for each ion in a cluster failed !" << "\n";
				return;
			}
		}

		//characterize these clusterer
		for( auto it = objs.begin(); it != objs.end(); it++ ) {
			if ( it->second != NULL ) {
				int status = it->second->compute_quickhull3_convex_hull();
			}
		}


		if ( write_config_cluster_analysis_task_h5() == true ) {
			//
		}
		else{
			cerr << "Writing the configuration for the cluster analysis failed !" << "\n"; return;
		}

		if ( write_results_cluster_analysis_task_h5() == MYHDF5_SUCCESS ) {
			//
		}
		else {
			cerr << "Writing the results for the cluster analysis failed !" << "\n"; return;
		}
	}

	toc = MPI_Wtime();
	memsnapshot mm = clusterer_tictoc.get_memoryconsumption();
	clusterer_tictoc.prof_elpsdtime_and_mem( "ClusterAnalysisWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc);
}


bool clustererHdl::init_results_h5()
{
	double tic = MPI_Wtime();

	string h5fn = ConfigClusterer::OutputfilePrefix + ".h5";

	if ( debug_h5Hdl.create_group_structure( h5fn, ConfigClusterer::DatasetID ) == MYHDF5_SUCCESS ) {

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		clusterer_tictoc.prof_elpsdtime_and_mem( "InitResultsGroupStructure", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	cerr << "Creating results group structure in " << h5fn << " failed !" << "\n";
	return false;
}


bool clustererHdl::write_config_cluster_analysis_task_h5()
{
	return true;
}


int clustererHdl::write_results_cluster_analysis_task_h5()
{
	double tic = MPI_Wtime();
	double toc = 0.;

	if ( objs.size() >= (size_t) UMX ) {
		cerr << "The total number of objects to write out is larger than currently supported by the implementation !" << "\n";
		return MYHDF5_FAILED;
		//##MK::change wuibuf to a 64-bit type , e.g. ui64 size_t
	}
	if ( objs.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return MYHDF5_SUCCESS;
	}

	string fnm = ConfigClusterer::OutputfilePrefix + ".h5";
	string grpnm = "";
	string dsnm = "";
	string fwslash = "/";
	string str = "";
	int status = MYHDF5_SUCCESS;

	h5seqHdl h5w = h5seqHdl();
	status = h5w.use_file( fnm );

	string prefix = MYCLST + to_string(ConfigClusterer::DatasetID) + MYCLST_DATA;
	//export convex hull for each cluster
	//##MK::IOStoreEvaporationIDs
	grpnm = prefix + MYCLST_DATA_CVHL_TSKS;
	if ( h5w.add_group( grpnm ) != MYHDF5_SUCCESS ) {
		cerr << "Creating group " << grpnm << " failed !" << "\n"; return MYHDF5_FAILED;
	}

	for( auto it = objs.begin(); it != objs.end(); it++ ) {
		if ( it->second != NULL ) {
			if ( it->second->vrts_cvh.size() >= 4 && it->second->fcts_cvh.size() >= 4 ) {

				string subgrpnm = grpnm + fwslash + "Object" + to_string(it->second->clusterid);
				if ( h5w.add_group( subgrpnm ) != MYHDF5_SUCCESS ) {
					cerr << "Creating group " << subgrpnm << " failed !" << "\n"; return MYHDF5_FAILED;
				}

				dsnm = subgrpnm + fwslash + MYCLST_DATA_CVHL_TSKS_VXYZ;
				vector<apt_real> r;
				for( auto jt = it->second->vrts_cvh.begin(); jt != it->second->vrts_cvh.end(); jt++ ) {
					r.push_back( jt->x ); r.push_back( jt->y ); r.push_back( jt->z );
				}
#ifdef EMPLOY_SINGLE_PRECISION
				if ( h5w.add_contiguous_array2d( dsnm, H5_F32, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
#else
				if ( h5w.add_contiguous_array2d( dsnm, H5_F64, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
#endif
					cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
				}
				r = vector<apt_real>();


				/*
				//##does the order of the normals work ???
				dsnm = subgrpnm + fwslash + MYCHM_DATA_ISRF_TSKS_TSCL_OBJ_OUNRM;
				for( auto jt = (*it)->ply_mesh_fcts_ounrm.begin(); jt != (*it)->ply_mesh_fcts_ounrm.end(); jt++ ) {
					r.push_back( jt->second.x() ); r.push_back( jt->second.y() ); r.push_back( jt->second.z() );
				}
#ifdef EMPLOY_SINGLE_PRECISION
				if ( h5w.add_contiguous_array2d( dsnm, H5_F32, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
#else
				if ( h5w.add_contiguous_array2d( dsnm, H5_F64, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
#endif
					cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
				}
				r = vector<apt_real>();
				*/

				dsnm = subgrpnm + fwslash + MYCLST_DATA_CVHL_TSKS_FCTS;
				vector<unsigned int> u32;
				for( auto jt = it->second->fcts_cvh.begin(); jt != it->second->fcts_cvh.end(); jt++ ) {
					u32.push_back( jt->v1 ); u32.push_back( jt->v2 ); u32.push_back( jt->v3 );
				}
				if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32.size()/3, 3, u32 ) != MYHDF5_SUCCESS ) {
					cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
				}
				u32 = vector<unsigned int>();

				/*
				dsnm = subgrpnm + fwslash + MYCLST_DATA_CVHL_TSKS_TOPO;
				u32 = vector<unsigned int>( it->second->fcts_cvh.size()*(1+1+3), 3 ); //xdmf keyword, three facets
				size_t i = 2;
				for( auto jt = it->second->fcts_cvh.begin(); jt != it->second->fcts_cvh.end(); jt++ ) {
					u32[i+0] = jt->v1; u32[i+1] = jt->v2; u32[i+2] = jt->v3;
					i += (1+1+3);
				}
				if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32.size(), 1, u32 ) != MYHDF5_SUCCESS ) {
					cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
				}
				u32 = vector<unsigned int>();
				*/

				/*
				dsnm = subgrpnm + fwslash + MYCHM_DATA_ISRF_TSKS_TSCL_OBJ_VFCTID;
				for( size_t i = 0; i < (*it)->ply_mesh_fcts_trgls.size(); i++ ) {
					u32.push_back( (apt_uint) i );
				}
				if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32.size(), 1, u32 ) != MYHDF5_SUCCESS ) {
					cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
				}
				u32 = vector<unsigned int>();
				*/

				if ( ConfigClusterer::IORecoverEvaporationSequenceID == true ) {
					//report recovered evaporation ID of each ion from which the convex hull was computed
					dsnm = subgrpnm + fwslash + MYCLST_DATA_CVHL_TSKS_EVAPID;
					for( auto jt = it->second->ions.begin(); jt != it->second->ions.end(); jt++ ) {
						u32.push_back( jt->m );
					}
					if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32.size()/1, 1, u32 ) != MYHDF5_SUCCESS ) {
						cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
					}
					u32 = vector<unsigned int>();
				}
			}
		}
	} //next cluster

	//collect volume and id of each cluster in a summary matrix
	vector<unsigned int> u32;
	vector<apt_real> r;

	for( auto it = objs.begin(); it != objs.end(); it++ ) {
		if ( it->second != NULL ) {
			u32.push_back( it->second->clusterid );
			r.push_back( it->second->volume );
		}
	}
	dsnm = prefix + fwslash + MYCLST_DATA_CVHL_ID;
	if ( h5w.add_contiguous_array2d( dsnm,  H5_U32, u32.size()/1, 1, u32 ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n";
	}
	u32 = vector<unsigned int>();

	dsnm = prefix + fwslash + MYCLST_DATA_CVHL_VOL;
#ifdef EMPLOY_SINGLE_PRECISION
	if ( h5w.add_contiguous_array2d( dsnm,  H5_F32, r.size()/1, 1, r ) != MYHDF5_SUCCESS ) {
#else
	if ( h5w.add_contiguous_array2d( dsnm,  H5_F64, r.size()/1, 1, r ) != MYHDF5_SUCCESS ) {
#endif
		cerr << "Writing " << dsnm << " failed !" << "\n";
	}
	r = vector<apt_real>();

	//check if the total number of vertices for all objs can be represented using unsigned int
	size_t nvrts_total = 0;
	size_t nfcts_total = 0;
	for( auto it = objs.begin(); it != objs.end(); it++ ) {
		if ( it->second != NULL ) {
			nvrts_total += it->second->vrts_cvh.size();
			nfcts_total += it->second->fcts_cvh.size();
		}
	}
	if ( (nvrts_total < (size_t) UMX) && (3*nfcts_total < (size_t) UMX) ) {

		//xdmf visualization support for all convex hulls
		xdmfBaseHdl xml = xdmfBaseHdl();
		xml.add_grid_uniform( "quickhulls3" );

		//collect vertices of all convex hulls in a summary matrix
		dsnm = prefix + fwslash + MYCLST_DATA_CVHL_VXYZ;
		for( auto it = objs.begin(); it != objs.end(); it++ ) {
			if ( it->second != NULL ) {
				for( auto jt = it->second->vrts_cvh.begin(); jt != it->second->vrts_cvh.end(); jt++ ) {
					r.push_back(jt->x); r.push_back(jt->y); r.push_back(jt->z);
				}
			}
		}
#ifdef EMPLOY_SINGLE_PRECISION
		if ( h5w.add_contiguous_array2d( dsnm,  H5_F32, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
#else
		if ( h5w.add_contiguous_array2d( dsnm,  H5_F64, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
#endif
			cerr << "Writing " << dsnm << " failed !" << "\n";
		}
		r = vector<apt_real>();

		vector<size_t> dims = { nvrts_total, 3 };
		xml.add_geometry_xyz( dims, H5_F32, ConfigClusterer::OutputfilePrefix + ".h5" + ":" + dsnm );

		//collect facet indices
		dsnm = prefix + fwslash + MYCLST_DATA_CVHL_TOPO;
		apt_uint idoffset = 0;
		u32.reserve((1+1+3)*nfcts_total);
		//MK::we can store more than UMX unsigned ints in an array but not represent more than UMX disjoint ids with an unsigned int
		for( auto it = objs.begin(); it != objs.end(); it++ ) {
			if ( it->second != NULL ) {
				for( auto jt = it->second->fcts_cvh.begin(); jt != it->second->fcts_cvh.end(); jt++ ) {

					u32.push_back(3); //xdmf prim keyword
					u32.push_back(3); //number of verts per polygon (triangle)
					u32.push_back( idoffset + jt->v1 );
					u32.push_back( idoffset + jt->v2 );
					u32.push_back( idoffset + jt->v3 );
					//MK::we must not naively idoffset +=3 now because vrts_cvh are the disjoint vertices only
				}
				idoffset = idoffset + (apt_uint) it->second->vrts_cvh.size();
			}
		}
		if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32.size()/1, 1, u32 ) != MYHDF5_SUCCESS ) {
			cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
		}
		u32 = vector<unsigned int>();

		dims = { nfcts_total*(1+1+3), 1 }; //xdmf keyword, xdmf number of vertices, three vertex IDs for each triangle
		xml.add_topology_mixed( nfcts_total, dims, H5_U32, ConfigClusterer::OutputfilePrefix + ".h5" + ":" + dsnm );

		//collect facet cluster/convex hull IDs
		dsnm = prefix + fwslash + MYCLST_DATA_CVHL_CLID;
		for( auto it = objs.begin(); it != objs.end(); it++ ) {
			if ( it->second != NULL ) {
				apt_uint clid = it->second->clusterid;
				for( size_t j = 0; j < it->second->fcts_cvh.size(); j++ ) {
					u32.push_back(clid);
				}
			}
		}
		if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32.size()/1, 1, u32 ) != MYHDF5_SUCCESS ) {
			cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
		}
		u32 = vector<unsigned int>();

		dims = { nfcts_total, 1 };
		xml.add_attribute_scalar( "clusterid", "Cell", dims, H5_U32, ConfigClusterer::OutputfilePrefix + ".h5" + ":" + dsnm );

		const string xmlfn = ConfigClusterer::OutputfilePrefix + ".h5" + ".ConvexHulls.xdmf";
		xml.write( xmlfn );
	}
	else {
		cerr << "The current implementation does not support that there are so many objs with convex hull support vertices that their IDs exceed the range of unsigned int !" << "\n";
		//##MK::nothing will be reported, can be changed easily just exchange unsigned int for size_t but who wants to visualize more than 4.2 billion vertices in one image ...
	}

	toc = MPI_Wtime();
	cout << "Writing convex hull geometries and properties took " << (toc-tic) << " s" << "\n";
	return true;
}


/*
bool clustererHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigNanochem::DatasetIDRecon, ConfigNanochem::InputfileRecon ) == true &&
			read_ranging_from_h5( ConfigNanochem::DatasetIDRanging, ConfigNanochem::InputfileRanging ) == true &&
				read_ionlabels_from_h5( ConfigNanochem::DatasetIDRanging,  ConfigNanochem::InputfileRanging ) == true ) {
		//read_mq_from_h5( ConfigNanochem::DatasetIDRanging, ConfigNanochem::InputfileRanging ) == true &&

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		clusterer_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}
*/
