//##MK::GPLV3

#include "PARAPROBE_ClustererHdl.h"


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigClusterer::read_config_from_h5( pargv[CONTROLFILE] );
	}
	catch (exception& e) {
		cerr << "Parsing configuration failed !" << "\n";
		return false;
	}
	if ( ConfigClusterer::check_config() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void execute_cluster_analysis_tasks( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a distancerHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	clustererHdl* cls = NULL;
	int localhealth = 1;
	try {
		cls = new clustererHdl;
		cls->set_myrank(r);
		cls->set_nranks(nr);
		//cls->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating clustererHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	if ( cls->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << cls->get_myrank() << " has recognized that not all processes are on board!" << "\n";
		delete cls; cls = NULL; return;
	}

	//we have all on board, now read the dataset that we want to process
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	//if ( ConfigDistancer::DistancingMethod != DISTANCING_NONE ) {
		//master reads first all relevant input, slaves wait
		if ( cls->get_myrank() == MASTER ) {

			if ( cls->init_results_h5() == true ) {
				cout << "Initializing the results file was successful" << "\n";
			}
			else {
				cerr << "Initializing the results file failed !" << "\n"; localhealth = 0;
			}
		}
		//MPI_Barrier( MPI_COMM_WORLD );
		if ( cls->all_healthy_still( localhealth ) == false ) {
			cerr << "Rank " << cls->get_myrank() << " has recognized that not all data have arrived at the master !" << "\n";
			delete cls; cls = NULL; return;
		}

		//master broadcasts relevant pieces of information to all slaves
		/*
		if ( isct->get_nranks() > SINGLEPROCESS ) {
			if ( isct->broadcast_relevant_input() == false ) {
				cerr << "Rank " << isct->get_myrank() << " failed participating in broadcasting of the relevant input !" << "\n";
				localhealth = 0;
			}

			MPI_Barrier( MPI_COMM_WORLD );
			if ( isct->all_healthy_still( localhealth ) == false ) {
				cerr << "Rank " << isct->get_myrank() << " has recognized that not all data have arrived at the slaves !" << "\n";
				delete isct; isct = NULL; return;
			}
		}
		*/

		cls->execute_cluster_analysis_workpackage();
	//}
	
	//release resources
	delete cls; cls = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	tool_startup( "paraprobe-clusterer" );
	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks !" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else {
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		execute_cluster_analysis_tasks( r, nr, argv );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-distancer is implemented for a single process only !" << "\n";
	}

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	double toc = omp_get_wtime();
	cout << "paraprobe-intersector took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}


/*
#define OBJECT_IS_POLYHEDRON							0xFF
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <iostream>
typedef CGAL::Simple_cartesian<double>     MyKernel;
typedef MyKernel::Point_3                  MyPoint_3;
typedef CGAL::Polyhedron_3<MyKernel>       MyPolyhedron;
typedef MyPolyhedron::Vertex_iterator        Vertex_iterator;
*/

/*
#include <CGAL/Exact_integer.h>
#include <CGAL/Extended_homogeneous.h>
#include <CGAL/Polyhedron_3.h>
//#include <CGAL/Homogeneous.h>
#include <CGAL/Nef_polyhedron_3.h>
//#include <CGAL/IO/Nef_polyhedron_iostream_3.h>
//#include <iostream>
typedef CGAL::Extended_homogeneous<CGAL::Exact_integer>  YourKernel;
typedef YourKernel::Point_3                  YourPoint_3;
typedef CGAL::Polyhedron_3<YourKernel>  YourPolyhedron;
typedef CGAL::Nef_polyhedron_3<YourKernel> Nef_polyhedron;
typedef YourKernel::Vector_3  Vector_3;
typedef YourKernel::Aff_transformation_3  Aff_transformation_3;
*/


/*
int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	double toc = 0.;

	MyPoint p1( 1.0, 0.0, 0.0);
	MyPoint q1( 0.0, 1.0, 0.0);
	MyPoint s1( 1.0, 0.0, 2.0); //0.0, 0.0, 1.0
	MyPoint r1( -0.5, -0.5, -0.5); //0.0, 0.0, 0.0);
    YourPolyhedron P1;
    P1.make_tetrahedron( p1, q1, r1, s1);
    if ( P1.is_closed() == false ) {
    	cerr << "P1.is_closed() == false" << "\n";
    	return 0;
    }
    cout << "PMP::volume(P1) " << setprecision(32) << PMP::volume(P1) << "\n";
    MyPoint p2( 2.0, 0.0, 0.0); //-2, 0, 0
    MyPoint q2( 0.0, 2.0, 0.0); //0, -2, 0
    MyPoint r2( 0.0, 0.0, 2.0); //1.5, 0, 2 is a toucher
    MyPoint s2( 0.0, 0.0, 0.0);
    YourPolyhedron P2;
    P2.make_tetrahedron( p2, q2, r2,  s2 );
    cout << "PMP::volume(P2) " << setprecision(32) << PMP::volume(P2) << "\n";
    if ( P2.is_closed() == false ) {
    	cerr << "P2.is_closed() == false" << "\n";
    	return 0;
    }

    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";
    tic = omp_get_wtime();

    Nef_polyhedron N1(P1);
    Nef_polyhedron N2(P2);
    //boolean intersection of two NEF polyhedra
    Nef_polyhedron N1isectN2 = N1 * N2;
    //N1isectN2.is_bounded(), N1isectN2.is_empty()

    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";
    tic = omp_get_wtime();

    Surface_mesh myout;
    CGAL::convert_nef_polyhedron_to_polygon_mesh(N1isectN2, myout);
    if ( CGAL::is_closed(myout) == true ) {
    	cout << "PMP::volume(myout) " << setprecision(32) << PMP::volume(myout) << "\n";
    }
    else {
    	cerr << "CGAL::is_closed(myout) == false !" << "\n";
    	return 0;
    }
    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";

    cout << myout;
    cout << endl;

    //if ( N1isectN2.is_simple() == true ) {
    //	cout << "N1isectN2.is_simple() == true " << "\n";
    //	YourPolyhedron P1isectP2;
    //	N1isectN2.convert_to_polyhedron( P1isectP2 );
    //	if ( P1isectP2.is_closed() == true ) {
    //		cout << "P1isectP2.is_closed() == true" << "\n";
    //		cout << "P1isectP2.volume() " << PMP::volume(P1isectP2) << "\n";
    //	}
    //}

    return 0;
}

#else

//intersection volume and mesh of intersection region using nef polyhedra
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include <CGAL/Polygon_mesh_processing/measure.h>
#include <fstream>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel EK;
typedef CGAL::Surface_mesh<K::Point_3> Mesh;
typedef Mesh::Vertex_index vertex_descriptor;
typedef Mesh::Face_index face_descriptor;
typedef boost::graph_traits<Mesh>::vertex_descriptor vertex_descriptor;
typedef Mesh::Property_map<vertex_descriptor,EK::Point_3> Exact_point_map;
namespace PMP = CGAL::Polygon_mesh_processing;
namespace params = PMP::parameters;

#include <CGAL/Polygon_mesh_processing/self_intersections.h>
typedef boost::graph_traits<Mesh>::face_descriptor          bface_descriptor;
#include <CGAL/tags.h>

#include <CGAL/Polygon_mesh_processing/distance.h>
#include <CGAL/Polygon_mesh_processing/remesh.h>




#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/Polygon_mesh_processing/repair_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
typedef CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3>      Polyhedron;
typedef K::FT                                                   FT;
#include<array>
typedef std::array<FT, 3>                                       Custom_point;
#include <boost/property_map/property_map.hpp>

#include <CGAL/convex_hull_3.h>

struct Array_traits
{
	struct Equal_3
	{
		bool operator()(const Custom_point& p, const Custom_point& q) const {
			return (p == q);
		}
	};
	struct Less_xyz_3
	{
		bool operator()(const Custom_point& p, const Custom_point& q) const {
			return std::lexicographical_compare(p.begin(), p.end(), q.begin(), q.end());
		}
	};
	Equal_3 equal_3_object() const {
		return Equal_3();
	}
	Less_xyz_3 less_xyz_3_object() const {
		return Less_xyz_3();
	}
};


struct Exact_vertex_point_map
{
  // typedef for the property map
  typedef boost::property_traits<Exact_point_map>::value_type value_type;
  typedef boost::property_traits<Exact_point_map>::reference reference;
  typedef boost::property_traits<Exact_point_map>::category category;
  typedef boost::property_traits<Exact_point_map>::key_type key_type;
  // exterior references
  Exact_point_map exact_point_map;
  Mesh* tm_ptr;
  // Converters
  CGAL::Cartesian_converter<K, EK> to_exact;
  CGAL::Cartesian_converter<EK, K> to_input;
  Exact_vertex_point_map()
    : tm_ptr(nullptr)
  {}
  Exact_vertex_point_map(const Exact_point_map& ep, Mesh& tm)
    : exact_point_map(ep)
    , tm_ptr(&tm)
  {
    for (Mesh::Vertex_index v : vertices(tm))
      exact_point_map[v]=to_exact(tm.point(v));
  }
  friend
  reference get(const Exact_vertex_point_map& map, key_type k)
  {
    CGAL_precondition(map.tm_ptr!=nullptr);
    return map.exact_point_map[k];
  }
  friend
  void put(const Exact_vertex_point_map& map, key_type k, const EK::Point_3& p)
  {
    CGAL_precondition(map.tm_ptr!=nullptr);
    map.exact_point_map[k]=p;
    // create the input point from the exact one
    map.tm_ptr->point(k)=map.to_input(p);
  }
};


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	double toc = 0.;

//strategy 1 - approximate (##MK::need to compute symmetric) Hausdorff distance, works but very expensive and approximate ...
	//Mesh tm1, tm2;
	//CGAL::make_tetrahedron(	K::Point_3( 0., 0., 0. ),
	//						K::Point_3(2., 0., 0.),
	//						K::Point_3(1., 1., 1.),
	//						K::Point_3(1., 0., 2.),  tm1 );
	//tm2 = tm1;
	//CGAL::Polygon_mesh_processing::isotropic_remeshing(tm2.faces(),.05, tm2);
	//cout << "Approximated Hausdorff distance: " <<
	//		PMP::approximate_Hausdorff_distance<CGAL::Sequential_tag>(tm1, tm2,
	//				PMP::parameters::number_of_points_per_area_unit(stoul(argv[1]))) << "\n";

	//toc = omp_get_wtime();
	//cout << (toc-tic) << " s" << "\n";
	//tic = omp_get_wtime();
	//return 0;

//strategy 2 - polygon mesh processing clipping two tetrahedra, not as robust for edge touches, robustness ?

	Mesh m1;
	CGAL::make_tetrahedron(	K::Point_3(1.0, 0.0, 0.0),
							K::Point_3(0.0, 1.0, 0.0),
							K::Point_3(1.0, 0.0, 2.0),
							K::Point_3(-0.5, -0.5, -0.5), m1 );

	//vertex_descriptor p1 = m1.add_vertex(K::Point_3(1.0, 0.0, 0.0));
	//vertex_descriptor p2 = m1.add_vertex(K::Point_3(0.0, 1.0, 0.0));
	//vertex_descriptor p3 = m1.add_vertex(K::Point_3(1.0, 0.0, 2.0)); //0.0, 0.0, 1.0
	//vertex_descriptor p4 = m1.add_vertex(K::Point_3(-0.5, -0.5, -0.5)); //0.0, 0.0, 0.0);
	//m1.add_face(p1, p2, p3);
	//face_descriptor f243 = m1.add_face(p2, p4, p3);
	//if ( f243 == Mesh::null_face()) {
	//	f243 = m1.add_face( p2, p3, p4 ); assert( f243 != Mesh::null_face());
	//}
	//face_descriptor f413 = m1.add_face( p4, p1, p3 );
	//if ( f413 == Mesh::null_face()) {
	//	f413 = m1.add_face( p4, p3, p1 ); assert( f413 != Mesh::null_face());
	//}
	//face_descriptor f214 = m1.add_face( p2, p1, p4 );
	//if ( f214 == Mesh::null_face()) {
	//	f214 = m1.add_face( p2, p4, p1 ); assert( f214 != Mesh::null_face());
	//}
    cout << "PMP::volume(m1) " << setprecision(32) << PMP::volume(m1) << "\n";

    Mesh m2;
	CGAL::make_tetrahedron(	K::Point_3(2.0, 0.0, 0.0),
							K::Point_3(0.0, 2.0, 0.0),
							K::Point_3(0.0, 0.0, 2.0),
							K::Point_3(0.0, 0.0, 0.0), m2 );

	//vertex_descriptor q1 = m2.add_vertex(K::Point_3(2.0, 0.0, 0.0));
	//vertex_descriptor q2 = m2.add_vertex(K::Point_3(0.0, 2.0, 0.0));
	//vertex_descriptor q3 = m2.add_vertex(K::Point_3(0.0, 0.0, 2.0));
	//vertex_descriptor q4 = m2.add_vertex(K::Point_3(0.0, 0.0, 0.0));
	//m2.add_face(q1, q2, q3);
	//f243 = m1.add_face(q2, q4, q3);
	//if ( f243 == Mesh::null_face()) {
	//	f243 = m2.add_face( q2, q3, q4 ); assert( f243 != Mesh::null_face());
	//}
	//f413 = m1.add_face( q4, q1, q3 );
	//if ( f413 == Mesh::null_face()) {
	//	f413 = m2.add_face( q4, q3, q1 ); assert( f413 != Mesh::null_face());
	//}
	//f214 = m2.add_face( q2, q1, q4 );
	//if ( f214 == Mesh::null_face()) {
	//	f214 = m2.add_face( q2, q4, q1 ); assert( f214 != Mesh::null_face());
	//}
	cout << "PMP::volume(m2) " << setprecision(32) << PMP::volume(m2) << "\n";

	if ( CGAL::is_triangle_mesh(m1) == true )
		cout << "m1 is triangle mesh" << "\n";
	if ( CGAL::is_triangle_mesh(m2) == true )
		cout << "m2 is triangle mesh" << "\n";

	bool m1isect = PMP::does_self_intersect( m1, CGAL::parameters::vertex_point_map(get(CGAL::vertex_point, m1)));
	cout << (m1isect ? "m1 There are self-intersections." : "m1 There is no self-intersection.") << endl;

	bool m2isect = PMP::does_self_intersect( m2, CGAL::parameters::vertex_point_map(get(CGAL::vertex_point, m2)));
	cout << (m2isect ? "m2 There are self-intersections." : "m2 There is no self-intersection.") << endl;

	bool m1bound = PMP::does_bound_a_volume( m1 );
	cout << (m1bound ? "m1 Does bound a volume" : "m1 Does not bound a volume") << endl;

	bool m2bound = PMP::does_bound_a_volume( m2 );
	cout << (m2bound ? "m2 Does bound a volume" : "m2 Does not bound a volume") << endl;

	toc = omp_get_wtime();
	cout << (toc-tic) << " s" << "\n";
	tic = omp_get_wtime();

	Exact_point_map m1_exact_points = m1.add_property_map<vertex_descriptor,EK::Point_3>("v:exact_point").first;
	Exact_point_map m2_exact_points = m2.add_property_map<vertex_descriptor,EK::Point_3>("v:exact_point").first;
	Exact_vertex_point_map m1_vpm(m1_exact_points, m1);
	Exact_vertex_point_map m2_vpm(m2_exact_points, m2);

	toc = omp_get_wtime();
	cout << (toc-tic) << " s" << "\n";
	tic = omp_get_wtime();

	//now compute intersection region and store its mesh in-place inside m1 replacing m1 such with the result
	//Mesh isect;
	//bool status = PMP::corefine_and_compute_intersection( m1, m2, m1,
    //        params::vertex_point_map(m1_vpm),
    //        params::vertex_point_map(m2_vpm),
	//		params::vertex_point_map(m1_vpm) );
	//cout << ((status == true) ? "Intersection successfully computed" : "Intersection failed!") << "\n";

	Mesh m1m2union;
	bool valid_union = PMP::corefine_and_compute_union( m1, m2, m1m2union);
	cout << ((valid_union == true) ? "Unionn successfully computed" : "Union failed!") << "\n";

	toc = omp_get_wtime();
	cout << (toc-tic) << " s" << "\n";
	tic = omp_get_wtime();

	//cout << "PMP::volume(m1 (modified)) " << setprecision(32) << PMP::volume(m1) << "\n";
	//cout << m1 << endl;

	cout << "PMP::volume(m1m2union) " << setprecision(32) << PMP::volume(m1m2union) << "\n";
	cout << m1m2union << endl;

	double Vm1 = fabs(CGAL::to_double(PMP::volume(m1)));
	double Vm2 = fabs(CGAL::to_double(PMP::volume(m2)));
	double Vm1m2 = fabs(CGAL::to_double(PMP::volume(m1m2union)));
	double V = 0.5*((Vm1+Vm2) - Vm1m2);
	cout << "V " << setprecision(32) << V << "\n";

    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";
    tic = omp_get_wtime();

	std::vector<std::array<FT, 3> > points;

	points.push_back(CGAL::make_array<FT>(0., 1., 0. ));
    points.push_back(CGAL::make_array<FT>(1., 0., 0.));
	points.push_back(CGAL::make_array<FT>(1., -0., 1.));
	points.push_back(CGAL::make_array<FT>(0.5, 0.5, 1.));
	points.push_back(CGAL::make_array<FT>(0.63636363636363646456572951137787, 0., 1.3636363636363637574788754136534));
	points.push_back(CGAL::make_array<FT>(0., 0., 0.25));
    points.push_back(CGAL::make_array<FT>(0., 0., 0.));

    cout << "Convex hull" << "\n";
    vector<K::Point_3> cpoints;
    cpoints.push_back(K::Point_3(0., 1., 0. ));
    cpoints.push_back(K::Point_3(1., 0., 0.));
    cpoints.push_back(K::Point_3(1., -0., 1.));
    cpoints.push_back(K::Point_3(0.5, 0.5, 1.));
    cpoints.push_back(K::Point_3(0.63636363636363646456572951137787, 0., 1.3636363636363637574788754136534));
    cpoints.push_back(K::Point_3(0., 0., 0.25));
    cpoints.push_back(K::Point_3(0., 0., 0.));
    Polyhedron chull;
    CGAL::convex_hull_3(cpoints.begin(), cpoints.end(), chull);
    cout << PMP::volume(chull) << "\n";

    vector<vector<size_t>> polygons;
    vector<size_t> p;
    p.clear();
    p.push_back(2); p.push_back(3); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(3); p.push_back(5); p.push_back(0);
    polygons.push_back(p);
    p.clear();
    p.push_back(3); p.push_back(0); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(4); p.push_back(5); p.push_back(3);
    polygons.push_back(p);
    p.clear();
    p.push_back(5); p.push_back(2); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(6); p.push_back(1); p.push_back(0);
    polygons.push_back(p);
    p.clear();
    p.push_back(6); p.push_back(5); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(4); p.push_back(2); p.push_back(5);
    polygons.push_back(p);
    p.clear();
    p.push_back(0); p.push_back(5); p.push_back(6);
    polygons.push_back(p);
    p.clear();
    p.push_back(3); p.push_back(2); p.push_back(4);
    polygons.push_back(p);

    PMP::repair_polygon_soup(points, polygons, CGAL::parameters::geom_traits(Array_traits()));
    PMP::orient_polygon_soup(points, polygons);
    Polyhedron p1;
    PMP::polygon_soup_to_polygon_mesh(points, polygons, p1);
    // Number the faces because 'orient_to_bound_a_volume' needs a face <--> index map
    int index = 0;
	for( Polyhedron::Face_iterator ply_fb = p1.facets_begin(), ply_fe = p1.facets_end(); ply_fb != ply_fe; ++ply_fb ) {
		ply_fb->id() = index++;
	}
	bool p1closed = CGAL::is_closed(p1);
	cout << ((p1closed == true ) ? "p1 closed" : "p1 not closed!") << "\n";
	PMP::orient_to_bound_a_volume(p1);
    cout << "PMP volume(p1) " << CGAL::to_double(PMP::volume(p1)) << "\n";

    return 0;

//strategy 3 with nef polyhedra, works and is robust but expensive
    //MK::see the above example
}
*/
