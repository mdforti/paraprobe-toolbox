//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_XDMF_HDL_H__
#define __PARAPROBE_CLUSTERER_XDMF_HDL_H__

#include "PARAPROBE_ClustererHDF5.h"


class clusterer_xdmf : public xdmfBaseHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	clusterer_xdmf();
	~clusterer_xdmf();

	/*
	int create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref );
	int create_phaseresults_file( const string xmlfn, const size_t ndir, const string h5ref,
			const unsigned int phcandid, vector<int> const & mpids );
	*/

	/*
	int create_scalarfield_file( const string xmlfn, isosurfaceHdl const & ifo  );
	*/

	/*
	int create_trianglesoup_file( const string xmlfn, const unsigned int tskid, const size_t topo_nelements,
			const size_t topo_dims, const size_t geom_dims, const size_t attr_dims, const string h5iso );
	int create_triangleclusters_file( const string xmlfn, const unsigned int tskid, const size_t topo_nelements,
			const size_t topo_dims, const size_t geom_dims, const size_t attr_dims, const string h5iso );
	int create_autorois_hexahedra_file( const string xmlfn, const unsigned int tskid, const size_t topo_nelements,
				const size_t topo_dims, const size_t geom_dims, const size_t attr_dims, const string h5iso );
	*/
};

#endif
