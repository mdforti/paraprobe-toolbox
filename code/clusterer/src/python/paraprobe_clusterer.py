#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#

import os
import time
import datetime as dt
import numpy as np
import h5py
from joblib import cpu_count
from ifes_apt_tc_data_modeling.utils.utils import hash_to_isotope
from utils.src.python.iontype_handling import IonCloud, IonType
from utils.src.python.numerics \
    import MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, RMX, MYHDF5_COMPRESSION_DEFAULT
from clusterer.src.python.paraprobe_configclusterer import ConfigClusterer
from utils.src.python.hdf_five_string_io import write_strings_to_attribute
from utils.src.python.hashing import get_simulation_id, get_file_hashvalue
from utils.src.python.gitinfo import PARAPROBE_VERSION, NEXUS_VERSION


use_gpu = False
# set the above boolean to True if you have a CUDA-capable GPU and use the rapids environment
# https://gist.github.com/s-mawjee/ad0d8e0c7e07265cae097899fe48c023
if use_gpu is True:
    from cuml.cluster import DBSCAN
    from cuml.cluster import HDBSCAN
else:
    from sklearn.cluster import DBSCAN
    from sklearn.cluster import OPTICS
    from hdbscan import HDBSCAN


class ParaprobeClusterer():
    def __init__(self, nxs_config_file_name):
        """Initialize the tool with configurations."""
        self.config_file = nxs_config_file_name
        self.entry_id = 1
        self.task_id = 1
        self.simid = get_simulation_id(nxs_config_file_name)
        self.start_time = dt.datetime.now(dt.timezone.utc).isoformat().replace("+00:00", "Z")
        self.n_ions = 0
        self.ions = IonCloud()
        self.config = ConfigClusterer(self.simid)
        self.io = True
        self.use_gpu = use_gpu
        # https://gist.github.com/s-mawjee/ad0d8e0c7e07265cae097899fe48c023
        if self.use_gpu is True:
            self.back_end = "gpu"
        else:
            self.back_end = "cpu"
        print(f"paraprobe-clusterer started with SimID {self.simid}")
        if self.use_gpu is True:
            print("Using GPU-based algorithms version")
        else:
            print("Using CPU-based algorithms")

    def read_reconstruction(self, tskid):
        """Load reconstructed positions and mass to charge."""
        print("Loading reconstructed positions...")
        with h5py.File(self.config.clustering_tasks[tskid].dataset["file_path"], "r") as h5r:
            dsnm = self.config.clustering_tasks[tskid].dataset["dset_position"]
            if dsnm not in h5r:
                raise KeyError(f"Dataset {dsnm} does not exist in input file!")
            self.ions.ionpp3 = np.asarray(h5r[dsnm][:, :], np.float32)
            self.n_ions = np.shape(self.ions.ionpp3)[0]
            if self.n_ions > 0:
                print(f"The dataset contains a total of {self.n_ions} ions")
                print(np.shape(self.ions.ionpp3))
                print(self.ions.ionpp3.dtype)

                # optional, mass-to-charges no needed provided data are ranged already
                # dsnm = f"{recon_grpnm}/atom_probe/mass_to_charge_conversion/mass_to_charge"
                # assert dsnm in h5r, f"Dataset {dsnm} does not exist in {recon_file} !"
                # self.ions.ionmq = np.asarray(h5r[dsnm][:, 0], np.float32)
                # print(np.shape(self.ions.ionmq))
                # print(self.ions.ionmq.dtype)
                # assert np.shape(self.ions.ionmq)[0] == self.n_ions, \
                #     "Ion position and ion mass-to-charge array need to have the same length !"
            else:
                print("WARNING: The reconstruction contains no ions, stopping now !")

    def read_ranging(self, tskid):
        """Load relevant ion types from existent ranging definitions."""
        print("Loading ranging definitions...")
        with h5py.File(self.config.clustering_tasks[tskid].iontypes["file_path"], "r") as h5r:
            grpnm = self.config.clustering_tasks[tskid].iontypes["grpnm_iontypes"]
            if grpnm not in h5r:
                raise KeyError(f"Group {grpnm} does not exist in input file!")
            n_max = h5r[f"{grpnm}/maximum_number_of_atoms_per_molecular_ion"][()]
            print(f"Maximum number of atoms per molecular ion is {n_max}")
            if n_max != MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION:
                raise ValueError(f"This code was tested only when n_max is "
                                 f"{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION} !")
            # range_grpnm = "/entry/process0/use_existent_ranging"
            self.rng = {}
            for grp in h5r[f"{grpnm}"].keys():
                subgrpnm = f"{grpnm}/{grp}"
                if grp[0:3] != "ion" or grp == "iontypes":
                    continue
                else:
                    ion_type_id = int(grp.replace("ion", ""))
                # not continued so add this ion in the dictionary
                dsnm = f"{subgrpnm}/charge_state"
                chrg = np.int8(h5r[dsnm][()])
                dsnm = f"{subgrpnm}/nuclide_hash"
                ivec = np.asarray(h5r[dsnm][:], np.uint16)
                self.rng[ion_type_id] = IonType()
                self.rng[ion_type_id].charge = chrg
                self.rng[ion_type_id].ivec = ivec
            print("Ranging definitions were successfully interpreted resulting in the following dictionary")
            # print(self.rng)
            print(f"{len(self.rng.keys())} different iontypes are distinguished.")

            dsnm = f"{grpnm}/iontypes"
            self.ions.ionifo_i_org = np.asarray(h5r[dsnm][:], np.uint8)

            print(np.shape(self.ions.ionifo_i_org))
            print(self.ions.ionifo_i_org.dtype)
            if self.n_ions != np.shape(self.ions.ionifo_i_org)[0]:
                raise ValueError("The number of iontype_identifier has to be the same as the number of ions !")

    def read_ion_to_edge_distances(self, fpath=None, grpnm=None):
        """Load distances of ion-to-triangulated-surface-mesh of edge."""
        print("Loading ion-to-edge distances...")
        if fpath is None or grpnm is None:
            print("WARNING: No distance information given setting all distances to maximum !")
            self.ions_ionifo_dist = np.asarray(np.repeat(RMX, self.n_ions), np.float32)
        else:
            with h5py.File(fpath, "r") as h5r:
                if grpnm not in h5r:
                    raise KeyError(f"{grpnm} does not exist in NeXus/HDF5 file {fpath} !")
                self.ions.ionifo_dist = np.asarray(h5r[f"{grpnm}/distance"], np.float32)
                if np.shape(self.ions.ionifo_dist)[0] != self.n_ions:
                    print("WARNING: The length of the distance array does not match with that of the ions !")
                    print("WARNING: Resetting all distances to maximum !")
                self.ions_ionifo_dist = np.asarray(np.repeat(RMX, self.n_ions), np.float32)

    def init_evaporation_identifier(self):
        """Create support index array."""
        print("WARNING: paraprobe-clusterer currently assumes that all ions are processed !")
        self.ions.ionifo_evap_id = np.asarray(
            np.linspace(0, self.n_ions - 1, num=self.n_ions, endpoint=True), np.uint32)
        print(np.shape(self.ions.ionifo_evap_id))
        print(self.ions.ionifo_evap_id.dtype)
        print(f"First evaporation (sequence) id {np.min(self.ions.ionifo_evap_id)}")
        print(f"Last evaporation (sequence) id {np.max(self.ions.ionifo_evap_id)}")

    def apply_ion_type_id_filter(self, ion_type_id_whitelist=[], ion_type_mltply_dct={}):
        """Return a boolean filter mask of all those ions with i_org matching on from ion_type_id."""
        print("Applying ion_type_id filter...")
        print("Whitelist contains the following ion_type_ids")
        print(ion_type_id_whitelist)
        msk = None
        if (ion_type_id_whitelist is not None) and (np.shape(ion_type_id_whitelist)[0] > 0):
            # assert isinstance(ion_type_id_whitelist, np.ndarray), \
            #       "Argument ion_type_id_whitelist needs to be a numpy ndarray!"
            msk = np.repeat(False, self.n_ions)  # here explicitly assuming window is entire_dataset!
            # print(f"Mask state {np.sum(mask == True)}")
            for ityp_id in ion_type_id_whitelist:
                msk |= self.ions.ionifo_i_org == ityp_id  # print(f"Mask state {np.sum(mask == True)}")
        print(f"Filter accepted {np.sum(msk)} ions out of {self.n_ions}")
        print(f"This is {np.around(np.sum(msk) / self.n_ions * 100., decimals=3)} % of the entire dataset")
        self.ions.ionifo_mltply = np.asarray(np.repeat(0, self.n_ions), np.uint8)
        print(np.shape(self.ions.ionifo_mltply))
        for ion_type_id, multiplicity in ion_type_mltply_dct.items():
            if multiplicity < 0 or multiplicity >= 256:
                raise ValueError("Argument multiplicity is not mappable onto an np.uint8 !")
            self.ions.ionifo_mltply[self.ions.ionifo_i_org == ion_type_id] = np.uint8(multiplicity)
        return (msk, ion_type_id_whitelist)

    def apply_ion_type_filter(self, tskid):
        print("Applying ion_type filter...")
        print("The symbol_whitelist contains")
        symbol_whitelist = self.config.clustering_tasks[tskid].ion_query_nuclide_vector
        print(symbol_whitelist)
        # the symbol_whitelist is translated into a whitelist of isotope hash values
        # all those ions of ion_type whose ivec contains at least one of these isotopes is considered
        # all other ions are discarded
        # hash_values = []
        elements = []
        for hashval in symbol_whitelist:
            # hashval = element_or_isotope_to_hash(symbol)
            # hash_values.append(hashval)
            n_protons, n_neutrons = hash_to_isotope(hashval)
            # unhash_to_proton_neutron_pair(hashval)
            if n_protons > 0:
                elements.append(n_protons)
        # hash_values = np.unique(hash_values)
        # print("The isotope_whitelist contains the following hash_values")
        # print(hash_values)
        elements = np.unique(elements)
        print("The element_whitelist contains the following elements")
        print(elements)
        # implement multiplicity resolving logic
        # below implemented is the example implementation exclusively for resolve_elements
        ion_type_id_whitelist = []
        multiplicity = {}
        for ion_type_id, ion_info in self.rng.items():
            mltply = 0
            isin = False
            # solve more elegantly using isin = np.any(np.isin(ion_info.ivec, hash_values))
            # the multiplicity for a given iontype wrt to the whitelist is how many of the
            # nuclids in the molecular ion of the iontype match to an element from the whitelist
            for j in np.arange(0, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION):
                n_protons, n_neutrons = hash_to_isotope(ion_info.ivec[j])
                if n_protons in elements:
                    isin = True
                    mltply += 1
                    # must not break
            if isin is True:
                ion_type_id_whitelist.append(ion_type_id)
                multiplicity[ion_type_id] = mltply
        print("This ion_type_id_whitelist caused that ions of the following type are analyzed")
        print(np.unique(ion_type_id_whitelist))
        print("The multiplicity of each iontype with the constraint of the current ion_type_id_whitelist is")
        print(multiplicity)
        return self.apply_ion_type_id_filter(ion_type_id_whitelist, multiplicity)

    def execute_dbscan_tasks(self, tskid):
        if self.io == True:
            with h5py.File(self.config.output_file_name, "a") as h5w:
                grpnm = f"/entry{self.entry_id}/cluster_analysis{tskid}"
                if tskid != 1:
                    grp = h5w.create_group(grpnm)
                    write_strings_to_attribute(grp, "NX_class", "NXprocess")
                dsnm = f"{grpnm}/ion_multiplicity"
                dst = h5w.create_dataset(
                    dsnm,
                    data=np.asarray(self.ions.ionifo_mltply, np.uint8),
                    chunks=True, compression="gzip",
                    compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                dsnm = f"{grpnm}/ion_type_whitelist"
                dst = h5w.create_dataset(
                    dsnm,
                    data=np.asarray(self.ityp_whitelist, np.uint8),
                    chunks=True, compression="gzip",
                    compression_opts=MYHDF5_COMPRESSION_DEFAULT)

        for identifier, dbscan_task in self.config.clustering_tasks[tskid].dbscan.items():
            model = None
            if self.back_end == "gpu":
                # https://docs.rapids.ai/api/cuml/stable/api.html#cuml.DBSCAN
                model = DBSCAN(
                    eps=dbscan_task.config["eps"],
                    handle=None,
                    min_samples=dbscan_task.config["min_pts"],
                    metric="euclidean",
                    verbose=False,
                    output_type=None,
                    calc_core_sample_indices=True).fit(self.ions.ionpp3[self.targets])
                # max_mbytes_per_batch=(optional, int64)
            else:  # self.back_end == "cpu":
                # the run-time but not memory-optimized version
                model = DBSCAN(
                    eps=dbscan_task.config["eps"],
                    min_samples=dbscan_task.config["min_pts"],
                    metric="euclidean",
                    metric_params=None,
                    algorithm="kd_tree",
                    leaf_size=30,
                    p=None,
                    n_jobs=-1).fit(self.ions.ionpp3[self.targets])
                # the memory but not run-time optimized version
                # essentially that took forever compared to the above-mentioned version of DBScan
                # but is the 1 + correct ?
                """
                model = OPTICS(
                    min_samples=1 + dbscan_task.config["min_pts"],
                    max_eps=dbscan_task.config["eps"],
                    metric="euclidean",
                    cluster_method="dbscan",
                    eps=dbscan_task.config["eps"],
                    algorithm="kd_tree",
                    leaf_size=30,
                    n_jobs=-1).fit(self.ions.ionpp3[self.targets])
                """
            # sklearn and rapidsai have the advantage that the use the same terminology and naming conventions
            # otherwise the following code would be specific depending on the backend used

            if (model is not None) and (self.io == True):
                with h5py.File(self.config.output_file_name, "a") as h5w:
                    grpnm = f"/entry{self.entry_id}/cluster_analysis{tskid}/dbscan{identifier}"
                    grp = h5w.create_group(grpnm)
                    write_strings_to_attribute(grp, "NX_class", "NXsimilarity_grouping")
                    dst = h5w.create_dataset(
                        f"{grpnm}/model_labels",
                        data=np.asarray(model.labels_, np.int32),
                        chunks=True, compression="gzip",
                        compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                    dst = h5w.create_dataset(
                        f"{grpnm}/core_sample_indices",
                        data=np.asarray(model.core_sample_indices_, np.int32),
                        chunks=True, compression="gzip",
                        compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                    dst = h5w.create_dataset(
                        f"{grpnm}/targets",
                        data=np.asarray(self.ions.ionifo_evap_id[self.targets], np.uint32),
                        chunks=True, compression="gzip",
                        compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                    dst = h5w.create_dataset(
                        f"{grpnm}/eps",
                        data=dbscan_task.config["eps"])
                    write_strings_to_attribute(dst, "unit", "nm")
                    dst = h5w.create_dataset(
                        f"{grpnm}/min_pts",
                        data=dbscan_task.config["min_pts"])
                    dst = h5w.create_dataset(
                        f"{grpnm}/cardinality",
                        data=np.uint32(np.shape(self.targets)[0]))

                    # the following code snippets need to be checked in detail to assure that the
                    # remapping of the minimum set of indices and labels for targets is correct
                    # to understand from the perspective of the entire dataset which points are
                    # noise, core points, or clusters and if so which
                    verified = True  # the following branch would profit from more checks
                    if verified is True:
                        # masks have only the length of target and not for all ions !
                        # here we expand all of them to the entire dataset, for convenience
                        # ##MK::the price to pay are extra costs for compressing and decompressing
                        noise_pts_mask = np.zeros_like(self.ions.ionifo_evap_id, dtype=bool)
                        # has self.n_ions entries, all initialized to false
                        noise_trg_mask = np.zeros_like(model.labels_, dtype=bool)
                        noise_trg_mask[model.labels_ == -1] = True
                        np.put(noise_pts_mask, self.ions.ionifo_evap_id[self.targets], noise_trg_mask)
                        dst = h5w.create_dataset(
                            f"{grpnm}/is_noise",
                            data=np.asarray(noise_pts_mask, np.uint8),
                            chunks=True, compression="gzip",
                            compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                        del noise_trg_mask, noise_pts_mask
                        core_pts_mask = np.zeros_like(self.ions.ionifo_evap_id, dtype=bool)
                        core_trg_mask = np.zeros_like(model.labels_, dtype=bool)
                        core_trg_mask[model.core_sample_indices_] = True
                        np.put(core_pts_mask, self.ions.ionifo_evap_id[self.targets], core_trg_mask)
                        dst = h5w.create_dataset(
                            f"{grpnm}/is_core",
                            data=np.asarray(core_pts_mask, np.uint8),
                            chunks=True, compression="gzip",
                            compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                        del core_trg_mask, core_pts_mask  # ##MK::use bit-packing for is_noise and is_core masks
                        label_pts = np.zeros_like(self.ions.ionifo_evap_id, dtype=np.uint32)
                        label_trg = model.labels_ + 1  # all noise point information removed
                        np.put(label_pts, self.ions.ionifo_evap_id[self.targets], label_trg)
                        dst = h5w.create_dataset(
                            f"{grpnm}/numeric_label",
                            data=np.asarray(label_pts, np.uint32),
                            chunks=True, compression="gzip",
                            compression_opts=MYHDF5_COMPRESSION_DEFAULT)
                        dst = h5w.create_dataset(
                            f"{grpnm}/identifier_offset", data=np.uint32(1))
                        del label_pts
                        # do not delete label_trg will be used later
                        # ##MK::some common rule for DBScan often is that core points of clusters, marked with cluster identifier negative
                        # noise points are zero reset and rest are non-core points of clusters ??
                        # but it seems this is here not the case, instead sklearn hold a boolean array for which points are core points
                        # and all noise points are -1 and the first cluster has index 0, but we do not want this
                        # instead we want that noise points have label 0 and the first cluster starts at 1 so cluster_identifier
                        # follow the Fortran/Matlab +1 indexing convention
                        # cluster_identifiers = np.asarray(np.unique(label_trg), np.uint32)
                        # grpnm = f"/entry/process1/cluster_analysis{tskid}/dbscan{identifier}/statistics"
                        # grp = h5w.create_group(grpnm)
                        # grp.attrs["NX_class"] = "NXprocess"

                n_clusters_ = len(set(model.labels_)) - (1 if -1 in model.labels_ else 0)
                n_noise_ = list(model.labels_).count(-1)
                print(f"Executed DBScan task {identifier} found {n_clusters_} clusters {n_noise_} noise points")
                del model
            # next dbscan task

    def execute_hdbscan_tasks(self, tskid):
        return

    def init_results_nxs(self):
        with h5py.File(self.config.output_file_name, "w") as h5w:
            grpnm = f"/entry{self.entry_id}"
            grp = h5w.create_group(grpnm)
            write_strings_to_attribute(grp, "NX_class", "NXentry")
            dst = h5w.create_dataset(
                f"{grpnm}/definition", data="NXapm_paraprobe_clusterer_results")
            write_strings_to_attribute(dst, "version", NEXUS_VERSION)

            grpnm = f"/entry{self.entry_id}/common"
            grp = h5w.create_group(grpnm)
            write_strings_to_attribute(grp, "NX_class", "NXapm_paraprobe_tool_common")

            dst = h5w.create_dataset(f"{grpnm}/analysis_identifier", data=self.simid)
            grpnm = f"{grpnm}/config"
            grp = h5w.create_group(grpnm)
            write_strings_to_attribute(grp, "NX_class", "NXserialized")
            dst = h5w.create_dataset(f"{grpnm}/path", data=self.config_file)
            dst = h5w.create_dataset(f"{grpnm}/type", data="file")
            dst = h5w.create_dataset(f"{grpnm}/algorithm", data="sha256")
            dst = h5w.create_dataset(f"{grpnm}/checksum", data=get_file_hashvalue(self.config_file))

            grpnm = f"/entry{self.entry_id}/common/program1"
            grp = h5w.create_group(grpnm)
            write_strings_to_attribute(grp, "NX_class", "NXprogram")
            dst = h5w.create_dataset(
                f"{grpnm}/program", data="paraprobe-toolbox-clusterer")
            write_strings_to_attribute(dst, "version", PARAPROBE_VERSION)
            grpnm = f"/entry{self.entry_id}/common/profiling"
            grp = h5w.create_group(grpnm)
            write_strings_to_attribute(grp, "NX_class", "NXcs_profiling")
            dst = h5w.create_dataset(f"{grpnm}/start_time", data=self.start_time)
            dst = h5w.create_dataset(f"{grpnm}/current_working_directory", data=os.getcwd())

    def finish_results_nxs(self, tic):
        with h5py.File(self.config.output_file_name, "a") as h5w:
            grpnm = f"/entry{self.entry_id}/common/profiling"
            dst = h5w.create_dataset(
                f"{grpnm}/number_of_processes", data=np.uint(1))
            dst = h5w.create_dataset(
                f"{grpnm}/number_of_threads", data=np.uint(cpu_count()))
            n_gpus_used = 0
            if self.use_gpu == True:
                n_gpus_used = 1
            dst = h5w.create_dataset(f"{grpnm}/number_of_gpus", data=np.uint32(n_gpus_used))
            dst = h5w.create_dataset(f"/entry{self.entry_id}/common/status", data="success")
            dst = h5w.create_dataset(
                f"{grpnm}/end_time",
                data=dt.datetime.now(dt.timezone.utc).isoformat().replace("+00:00", "Z"))
            dst = h5w.create_dataset(f"{grpnm}/total_elapsed_time", data=time.perf_counter() - tic)
            write_strings_to_attribute(dst, "unit", "s")

    def execute(self):
        tic = time.perf_counter()
        self.init_results_nxs()

        for taskid in self.config.clustering_tasks.keys():
            print(f"Processing taskid {taskid}...")
            self.n_ions = 0
            self.read_reconstruction(taskid)
            self.read_ranging(taskid)
            self.read_ion_to_edge_distances(taskid)
            self.init_evaporation_identifier()
            self.targets = None
            self.ityp_whitelist = None
            self.targets, self.ityp_whitelist = self.apply_ion_type_filter(taskid)
            # print('targets is a mask which is True for each ion that is of requested type and False for all other ions')
            # compute multiplicity for each ion_type with respect to chosen
            self.execute_dbscan_tasks(taskid)
            self.execute_hdbscan_tasks(taskid)
            print(f"Processed taskid {taskid}")

        self.finish_results_nxs(tic)
        print("paraprobe-clusterer success")
        return self.config.output_file_name
