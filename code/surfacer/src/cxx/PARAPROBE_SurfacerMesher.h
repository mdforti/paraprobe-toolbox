/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SURFACER_MESHER_H__
#define __PARAPROBE_SURFACER_MESHER_H__


#include "PARAPROBE_OutsideInsideMask.h"

//using at least double precision for alpha shape computations
//through promoting/upcasting float to double


class alphaShape
{
public:
	alphaShape();
	/*
	alphaShape( vector<p3d> const & p, surface_detection_task const & tsk );
	*/
	~alphaShape();

	void alpha_shape_perform_facet_filtration( Alpha_shape_3 & as );
	void alpha_shape_extract_interior_tetrahedra( Alpha_shape_3 & as );
	void alpha_shape_extract_exterior_facets( Alpha_shape_3 & as );
	void surface_mesh_extract_exterior_facets( Surface & msh );
	void polyhedron_extract_exterior_facets();
	void mesh_check_closure();
	void polyhedron_check_closure();

	void compute_convex_hull( vector<p3d> const & p, surface_detection_task const & tsk, const bool mesh_refinement );
	void compute_single_alpha_shape( vector<p3d> const & p, surface_detection_task const & tsk );
	void compute_multiple_alpha_shapes( vector<p3d> const & p, surface_detection_task const & tsk );
	void compute_multiple_alpha_wrappings( vector<p3d> const & p, surface_detection_task const & tsk );
	bool write_alphashape_geometry( surface_detection_task const & tsk, apt_uint const identifier );

	void clear();

	vector<p3d64> vrts;			//disjoint vertices
	vector<tri3u> fcts;			//disjoint triangle facets of the complex in action
	ashape_info info;

	vector<CGAL::Object> the_objects;
	vector<Alpha_shape_3::FT> the_ft;

	Polyhedron poly;
	Surface cvh;
	//Alpha_shape_3 as;
	Surface wrap;
};

#endif
