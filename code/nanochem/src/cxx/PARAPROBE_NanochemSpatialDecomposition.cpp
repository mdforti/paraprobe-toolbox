/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemSpatialDecomposition.h"

ostream& operator<<(ostream& in, gridportioninfo const & val)
{
	in << "gmi " << val.gmi;
	in << "gmx " << val.gmx;
	in << "lmi " << val.lmi;
	in << "lmx " << val.lmx;
	in << "gnxy " << val.gnxy;
	in << "gnxyz " << val.gnxyz;
	in << "lnxy " << val.lnxy;
	in << "lnxyz " << val.lnxyz;
	in << "halo " << val.halo << "\n";
	in << "laabb " << val.laabb << "\n";
	return in;
}


threadedDelocalizer::threadedDelocalizer()
{
	myions = vector<binidx_ionidx>();
	bvh_mgn = vector<bin_mgn>();
	sortedpoints = vector<p3dm1>();
	points = vector<p3d>();

	cnts_f64 = vector<double>();
	kernel_halfsize = 0;
	kernel_sigma = 0.;

	glo_grid_info = voxelgrid();
	loc_grid_info = voxelgrid();

	zbounds_halo = pair<apt_int, apt_int>(0, 0);
	zbounds_ions = pair<apt_int, apt_int>(0, 0);
	nions_nelements = pair<size_t, size_t>(0, 0);
	ion2element_multiplier = vector<double>();
	status = MYDELOC_SUCCESS;
	participating = true;
	healthy = true;
}


threadedDelocalizer::~threadedDelocalizer()
{
}


void threadedDelocalizer::init_equilibrate_number_of_ions( voxelgrid const & entire_grid, vector<apt_uint> const & ions_per_z,
		const apt_int halfsize, const double sigma, const int thread_num, const int num_threads,
		pair<size_t, size_t> const & nionsnelements, vector<double> const & mltply )
{
	//split the 3D voxelgrid entire_grid into a set of thread-local voxel grids, the idea is we split along the longest, i.e. z axis
	//all voxel of the entire_grid are part of the local_grids, the local_grids have an additional to and bottom halo in along z direction
	//to store smeared out intensity of the kernel /within kernel halfsize
	//thereby one can get the accumulated intensities by successively adding values from thread MASTER up to thread NTHREADS-1

	kernel_halfsize = halfsize;
	kernel_sigma = sigma;

	glo_grid_info = voxelgrid( entire_grid );
	#pragma omp master
	{
		cout << "global grid " << "\n";
	}
	//cout << glo_grid_info << "\n";

	//find a job partitioning whereby the threads get a sufficient amount of work
	//apt_int nz_halo = glo_grid_info.nz;
	apt_uint nz_ions = 0;
	for( auto it = ions_per_z.begin(); it != ions_per_z.end(); ++it ) {
		nz_ions += *it;
	}
	apt_uint nz_target = ( nz_ions / (apt_uint) num_threads >= 1 ) ? (nz_ions / (apt_uint) num_threads) : 1;
	apt_int zcurrent = 1 + halfsize; //in the global grid the first layer with ions is at this z position
	//this is by virtue of construction because a box about the point cloud has been equipped with a guard zone of the halfsize + 1
	participating = false; //convince each thread that it should take part in solving the task
	zbounds_halo = pair<apt_int, apt_int>( 0, 0 );
	zbounds_ions = pair<apt_int, apt_int>( 0, 0 );
	for( apt_int thr = 0; thr < num_threads; thr++ ) {
		//we partition the tasks starting from the master with its sub-grid at the top of the tip and define further regions by successively moving to the bottom of the tip
		if ( thr < (num_threads-1) ) { //not the last thread who takes the remainder
			apt_int myion_budget = 0;
			apt_int iz = zcurrent;
			for(      ; iz <= (glo_grid_info.nz - (halfsize + 1));    ) {
				if ( (myion_budget + ions_per_z[iz]) <= nz_target ) { //accept the next layer
					myion_budget += ions_per_z[iz];
					iz++;
				}
				else { //take at least that one even if it is larger than target
					//otherwise say we have a given total the larger the number of threads the lower becomes their target causing all but the last thread to reject
					//work which is again though unbalance, so better take a layer
					iz++;
					break;
				}
			}

			if ( thr == thread_num ) {
				participating = true;
				zbounds_ions = pair<apt_int, apt_int>( zcurrent, iz );
				zbounds_halo = pair<apt_int, apt_int>( zcurrent - halfsize, iz + halfsize );
				break;
			}
			zcurrent = iz;
		}
		else { //take the remainder
			//catch the case that not everything was distributed, if there is sth take it
			if ( (zcurrent + 1) <= (glo_grid_info.nz - (halfsize + 1)) ) { //still enough for me and the next one
				if ( thr == thread_num ) { //found my portion
					participating = true;
					zbounds_ions = pair<apt_int, apt_int>( zcurrent, (glo_grid_info.nz - (halfsize + 1)) );
					zbounds_halo = pair<apt_int, apt_int>( zcurrent - halfsize, glo_grid_info.nz - 1 );
					break;
				}
			}
			//catch the case that nothing is there any longer
			else { //(zcurrent + nz_target) > (glo_grid_info.nz - (halfsize + 1))
				//nix mehr da :)
			}
			break;
		}
	}

	loc_grid_info = voxelgrid( entire_grid ); //only because we have only a single thread !

	nions_nelements = nionsnelements;
	//cout << "nions_nelements.first / .second " << nions_nelements.first << ", " << nions_nelements.second << "\n";

	ion2element_multiplier = mltply;
	//cout << "ion2element_multiplier.size() " << ion2element_multiplier.size() << "\n";

	if( participating == true ) {
		try {
			//cnts_f64 = vector<double>( nions_nelements.second*((size_t) loc_grid_info.nxyz), 0. );
			cnts_f64 = vector<double>( nions_nelements.second*((size_t) glo_grid_info.nxy*(zbounds_halo.second-zbounds_halo.first)), 0. );
		}
		catch(bad_alloc &croak)
		{
			cerr << "Allocation of cnts_f64 for thread " << thread_num << " failed !" << "\n";
			healthy = false;
		}
	}
}


apt_uint threadedDelocalizer::get_ion_budget( vector<apt_uint> const & ions_per_z )
{
	apt_uint retval = 0;
	for( apt_int iz = zbounds_ions.first; iz < zbounds_ions.second; iz++ ) {
		retval += ions_per_z[iz];
	}
	return retval;
}


void threadedDelocalizer::build_bvh( vector<unsigned char> const & window, vector<p3d> const & pos, vector<p3dinfo> const & ifo )
{
	if ( participating == true ) {

		myions = vector<binidx_ionidx>();

		for( size_t i = 0; i < window.size(); i++ ) {
			if ( window[i] != WINDOW_ION_EXCLUDE ) { //this is only a spatial filter, not an ion type filter !

				if ( glo_grid_info.aabb.is_inside( pos[i] ) == true ) {

					size_t binxyz = glo_grid_info.where_xyz( pos[i] );
					if ( binxyz != -1 ) {

						//aabb covers guard zones and processed region of the point cloud, include only those ions
						//which are in bins of a particular z range
						apt_int iz = glo_grid_info.where_z_no_checks( binxyz );
						if ( iz >= zbounds_ions.first && iz < zbounds_ions.second ) {
							myions.push_back( binidx_ionidx((apt_uint) binxyz, i) ); //collect data to reduce cache misses in kernel computation phase
						}
						else {
							if ( iz == -1 ) {
								#pragma omp critical
								{
									cerr << "Thread " << omp_get_thread_num() << " ion " << i << " ends up in bin -1 from where_z_no_checks, this indicates there is a problem with the local binning procedure !" << "\n";
								}
							}
						}
					}
					else {
						#pragma omp critical
						{
							cerr << "Thread " << omp_get_thread_num() << " ion " << i << " ends up in bin -1 from where_xyz, this indicates there is a problem with the local binning procedure !" << "\n";
						}
						healthy = false;
						return;
					}
				}
			}
		}
		//cout << "myions.size() " << myions.size() << "\n";

		//find how many ions per bin sorting first by binidx thereafter by ionidx to improve access on pp3 array
		sort( myions.begin(), myions.end(), SortBinIdxThenIonIdxAscending );

		bvh_mgn = vector<bin_mgn>();
		sortedpoints = vector<p3dm1>();
		if ( myions.size() > 0 ) {

			//##MK::workpackages is at least 1
			bin_mgn current = bin_mgn( 0, 0, myions.at(0).binidx );
			for( size_t j = 0; j < myions.size();    ) {
				if ( myions[j].binidx == current.binidx ) {
					current.onepastend++;
				}
				else {
					bvh_mgn.push_back( current );
					current = bin_mgn( current.onepastend, current.onepastend, myions.at(j).binidx );
					current.onepastend++;
				}
				size_t ii = (size_t) myions[j].ionidx;
				//we concentrate the cache misses here, so that while machining off the ions evaluating the kernel
				//we do not want to trash us so frequently the cache, and most importantly, we want to have the data closer to the executing core in memory
				sortedpoints.push_back( p3dm1(pos[ii].x, pos[ii].y, pos[ii].z, (apt_uint) ifo[ii].i_org) );
				j++;
			}
			//dont forget to push the last one
			bvh_mgn.push_back( current );
		}

		//cout << "bvh_mgn.size() " << bvh_mgn.size() << "\n";
		//cout << "sortedpoints.size() " << sortedpoints.size() << "\n";
	}
}


void threadedDelocalizer::compute_delocalization()
{
	if ( participating == true ) {
		size_t nelements = nions_nelements.second;
		apt_real l = glo_grid_info.dx; //##MK::always using cubic voxel currently
		double L = (double) l;
		apt_int a = kernel_halfsize;
		apt_int kernelwidth = (2*a)+1;
		double aL = (double) a * L;
		double sigma = kernel_sigma;
		double sqrtHalfPi = sqrt(MYPI/2.0);
		double sqrt2 = sqrt(2.0);
		//normalization constant to assure that the contribution of the entire truncated Gaussian in the kernel adds up to 1 so that
		//the "essence" of each ion is completely accounted for
		//IVAS/AP Suite does a so called atomic decomposition so that one C:2 molecular iontype gives 2 counts for C
		//##MK::here is where the multiplier should come
		double A = 1.0 / (   	(sqrtHalfPi*2*1.0*sigma * erf(aL/(sqrt2*1.0*sigma))) *
								(sqrtHalfPi*2*1.0*sigma * erf(aL/(sqrt2*1.0*sigma))) *
								(sqrtHalfPi*2*0.5*sigma * erf(aL/(sqrt2*0.5*sigma)))  );

		for( size_t bin = 0; bin < bvh_mgn.size(); bin++ ) {
			bin_mgn thisbin = bvh_mgn[bin];

			vector<apt_int> ixiyiz = glo_grid_info.where_xyz_no_checks( thisbin.binidx );

			//define cache-friendly thread local kernel which we fill with contributions from all the ions which have an "essence" to contribute in this kernel region
			vector<double> f64 = vector<double>( nelements*CUBE(kernelwidth), 0. );

			//now we have a bunch of ions in the same bin so we can reuse some temporaries during the kernel evaluation
			for( size_t idx = thisbin.start; idx < thisbin.onepastend; idx++ ) {

				double ddx = glo_grid_info.where_x_no_checks_f64( sortedpoints[idx].x );
				double ddy = glo_grid_info.where_y_no_checks_f64( sortedpoints[idx].y );
				double ddz = glo_grid_info.where_z_no_checks_f64( sortedpoints[idx].z );
				apt_uint thisityp = sortedpoints[idx].m;
				size_t mltplyrow = nelements * (size_t) thisityp;

				//delocalize this ion via analytical integration of the 3D integral for each voxel in the kernel
				for( apt_int z = -1*a; z <= a; z++ ) {
					double zz = (double) z;
					double zmi = ( (zz - ddz)*L > -aL ) ? (zz - ddz)*L : -aL;
					double zmx = ( (zz + 1.0 - ddz)*L < aL ) ? (zz + 1.0 - ddz)*L  : aL;
					apt_int zoff = (1*a + z)*SQR(kernelwidth);
					for( apt_int y = -1*a; y <= a; y++ ) {
						double yy = (double) y;
						double ymi = ( (yy - ddy)*L > -aL ) ? (yy - ddy)*L : -aL;
						double ymx = ( (yy + 1.0 - ddy)*L < aL ) ? (yy + 1.0 - ddy)*L  : aL;
						apt_int yzoff = (1*a + y)*kernelwidth + zoff;
						for( apt_int x = -1*a; x <= a; x++ ) {
							double xx = (double) x;
							double xmi = ( (xx - ddx)*L > -aL ) ? (xx - ddx)*L : -aL;
							double xmx = ( (xx + 1.0 - ddx)*L < aL ) ? (xx + 1.0 - ddx)*L  : aL;
							apt_int xyzpos = (1*a + x) + yzoff;
							//integrated essence of the ion in this voxel
							double vl = A *
								(sqrtHalfPi*1.0*sigma*( erf(xmx/(sqrt2*1.0*sigma)) - erf(xmi/(sqrt2*1.0*sigma)) )  ) *
								(sqrtHalfPi*1.0*sigma*( erf(ymx/(sqrt2*1.0*sigma)) - erf(ymi/(sqrt2*1.0*sigma)) )  ) *
								(sqrtHalfPi*0.5*sigma*( erf(zmx/(sqrt2*0.5*sigma)) - erf(zmi/(sqrt2*0.5*sigma)) )  );

							//bookkeeping
							size_t kernelpos = (size_t) xyzpos;
							size_t vlrow = nelements * kernelpos;
							for( size_t elj = 0; elj < nelements; elj++ ) {
								/*
								if ( ion2element_multiplier[mltplyrow+elj] > EPSILON ) { //EPSILON > 0.
									f64[vlrow+elj] += ion2element_multiplier[mltplyrow+elj] * vl;
								}
								*/
								if ( ion2element_multiplier[mltplyrow+elj] <= EPSILON ) { //EPSILON > 0., most multiplicities are zero in the row
									continue;
								}
								else {
									f64[vlrow+elj] += ion2element_multiplier[mltplyrow+elj] * vl;
								}
							} //next element
						} //next kernel x
					} //next kernel y
				} //next kernel z
			} //next ion also in this bin

			//transfer all result for the kernel to cnts_f64
			apt_int mygrid_xyzoffset = zbounds_halo.first * glo_grid_info.nxy;
			for( apt_int z = -1*a; z <= a; z++ ) {
				apt_int localzoff = (1*a + z)*SQR(kernelwidth);
				apt_int globalzoff = (ixiyiz[2] + z)*glo_grid_info.nxy;
				for( apt_int y = -1*a; y <= a; y++ ) {
					apt_int localyzoff = (1*a + y)*kernelwidth + localzoff;
					apt_int globalyzoff = (ixiyiz[1] + y)*glo_grid_info.nx + globalzoff;
					for( apt_int x = -1*a; x <= a; x++ ) {
						apt_int local_xyz = (1*a + x) + localyzoff;
						size_t local_here = local_xyz;
						size_t local_vlrow = nelements*local_here;
						apt_int global_xyz = ((ixiyiz[0] + x) + globalyzoff) - mygrid_xyzoffset;
						size_t global_here = global_xyz;
						size_t global_vlrow = nelements*global_here;
						for( size_t elj = 0; elj < nelements; elj++ ) {
							cnts_f64[global_vlrow+elj] += f64[local_vlrow+elj];
						}
					}
				}
			}

			//cout << "Processed bin " << bin << "\n";
		} //next bin

		//##MK::BEGIN DEBUG
		#pragma omp critical
		{
			cout << "Thread " << omp_get_thread_num() << " delocalization results !" << "\n";
			double cnts_sum = 0.;
			vector<double> cnts_el = vector<double>( nelements, 0. );
			for( size_t vxl = 0; vxl < cnts_f64.size()/nelements; vxl++ ) {
				for( size_t elj = 0; elj < nelements; elj++ ) {
					cnts_el[elj] += cnts_f64[(nelements*vxl)+elj];
					cnts_sum += cnts_f64[(nelements*vxl)+elj];
				}
			}
			for( size_t elj = 0; elj < nelements; elj++ ) {
				cout << "elj " << elj << "\t\t" << setprecision(32) << cnts_el[elj] << "\n";
			}
			cout << "cnts_sum " << setprecision(32) << cnts_sum << "\n";
		}
		//##MK::END DEBUG
	}
}


void threadedDelocalizer::init_and_load_distributing( voxelgrid const & entire_grid, vector<apt_uint> const & ions_per_z,
	const apt_int halfsize, const double sigma, const int thread_num, const int num_threads )
{
	//split the 3D voxelgrid entire_grid into a set of thread-local voxel grids, the idea is we split along the longest, i.e. z axis
	//all voxel of the entire_grid are part of the local_grids, the local_grids have an additional to and bottom halo in along z direction
	//to store smeared out intensity of the kernel /within kernel halfsize
	//thereby one can get the accumulated intensities by successively adding values from thread MASTER up to thread NTHREADS-1

	kernel_halfsize = halfsize;
	kernel_sigma = sigma;

	glo_grid_info = voxelgrid( entire_grid );

	//find a job partitioning whereby the threads get a sufficient amount of work
	//apt_int nz_halo = glo_grid_info.nz;
	apt_uint nz_ions = 0;
	for( auto it = ions_per_z.begin(); it != ions_per_z.end(); ++it ) {
		nz_ions += *it;
	}
	apt_uint nz_target = ( nz_ions / (apt_uint) num_threads >= 1 ) ? (nz_ions / (apt_uint) num_threads) : 1;
	apt_int zcurrent = 1 + halfsize; //in the global grid the first layer with ions is at this z position
	//this is by virtue of construction because a box about the point cloud has been equipped with a guard zone of the halfsize + 1
	participating = false; //convince each thread that it should take part in solving the task
	zbounds_halo = pair<apt_int, apt_int>( 0, 0 );
	zbounds_ions = pair<apt_int, apt_int>( 0, 0 );
	for( apt_int thr = 0; thr < num_threads; thr++ ) {
		//we partition the tasks starting from the master with its sub-grid at the top of the tip and define further regions by successively moving to the bottom of the tip
		if ( thr < (num_threads-1) ) { //not the last thread who takes the remainder
			apt_int myion_budget = 0;
			apt_int iz = zcurrent;
			for(      ; iz <= (glo_grid_info.nz - (halfsize + 1));    ) {
				if ( (myion_budget + ions_per_z[iz]) <= nz_target ) { //accept the next layer
					myion_budget += ions_per_z[iz];
					iz++;
				}
				else { //take at least that one even if it is larger than target
					//otherwise say we have a given total the larger the number of threads the lower becomes their target causing all but the last thread to reject
					//work which is again though unbalance, so better take a layer
					iz++;
					break;
				}
			}

			if ( thr == thread_num ) {
				participating = true;
				zbounds_ions = pair<apt_int, apt_int>( zcurrent, iz );
				zbounds_halo = pair<apt_int, apt_int>( zcurrent - halfsize, iz + halfsize );
				break;
			}
			zcurrent = iz;
		}
		else { //take the remainder
			//catch the case that not everything was distributed, if there is sth take it
			if ( (zcurrent + 1) <= (glo_grid_info.nz - (halfsize + 1)) ) { //still enough for me and the next one
				if ( thr == thread_num ) { //found my portion
					participating = true;
					zbounds_ions = pair<apt_int, apt_int>( zcurrent, (glo_grid_info.nz - (halfsize + 1)) );
					zbounds_halo = pair<apt_int, apt_int>( zcurrent - halfsize, glo_grid_info.nz - 1 );
					break;
				}
			}
			//catch the case that nothing is there any longer
			else { //(zcurrent + nz_target) > (glo_grid_info.nz - (halfsize + 1))
				//nothing left
			}
			break;
		}
	}

	loc_grid_info = voxelgrid( entire_grid ); //only because we have only a single thread !

	/*
	apt_uint retval = 0;
	for( apt_int iz = zbounds_ions.first; iz < zbounds_ions.second; iz++ ) {
		retval += ions_per_z[iz];
	}
	*/
	if( participating == true ) {
		try {
			cnts_f64 = vector<double>( ((size_t) glo_grid_info.nxy*(zbounds_halo.second-zbounds_halo.first)), 0. );
		}
		catch(bad_alloc &croak)
		{
			cerr << "Allocation of cnts_f64 for thread " << thread_num << " failed !" << "\n";
			status = MYDELOC_TMP_ALLOC_ERROR;
		}
	}
	//return retval;
}


void threadedDelocalizer::build_bvh_ityp( vector<apt_uint>* const cand, vector<p3d> const & pos )
{
	if ( participating == false || status != MYDELOC_SUCCESS ) {
		return;
	}

	myions = vector<binidx_ionidx>();
	for( auto it = cand->begin(); it != cand->end(); it++ ) {
		size_t binxyz = glo_grid_info.where_xyz( pos[*it] );
		if ( binxyz != -1 ) {
			//aabb covers guard zones and processed region of the point cloud
			//include only those ions which are in bins of a particular z range
			apt_int iz = glo_grid_info.where_z_no_checks( binxyz );
			if ( iz >= zbounds_ions.first && iz < zbounds_ions.second ) {
				myions.push_back( binidx_ionidx((apt_uint) binxyz, *it) );
				//collect data to reduce cache misses in kernel computation phase
			}
			else {
				if ( iz == -1 ) {
					status = MYDELOC_BINNING_ERROR;
					/*
					#pragma omp critical
					{
						cerr << "Thread " << omp_get_thread_num() << " ion " << *it << " ends up in bin -1 from where_z_no_checks, this indicates there is a problem with the local binning procedure !" << "\n";
					}
					*/
				}
			}
		}
		else {
			status = MYDELOC_BINNING_ERROR;
			/*
			#pragma omp critical
			{
				cerr << "Thread " << omp_get_thread_num() << " ion " << *it << " ends up in bin -1 from where_xyz, this indicates there is a problem with the local binning procedure !" << "\n";
			}
			return;
			*/
		}
	}

	//find how many ions per bin sorting first by binidx thereafter by ionidx to improve access on pp3 array
	sort( myions.begin(), myions.end(), SortBinIdxThenIonIdxAscending );

	bvh_mgn = vector<bin_mgn>();
	sortedpoints = vector<p3dm1>();
	if ( myions.size() > 0 ) {
		bin_mgn current = bin_mgn( 0, 0, myions.at(0).binidx );
		for( size_t j = 0; j < myions.size();    ) {
			if ( myions[j].binidx == current.binidx ) {
				current.onepastend++;
			}
			else {
				bvh_mgn.push_back( current );
				current = bin_mgn( current.onepastend, current.onepastend, myions.at(j).binidx );
				current.onepastend++;
			}
			size_t ii = (size_t) myions[j].ionidx;
			//we concentrate the cache misses here, so that while machining off the ions evaluating the kernel
			//we do not want to trash us so frequently the cache, and most importantly, we want to have the data closer to the executing core in memory
			points.push_back( pos[ii] );
			j++;
		}
		//dont forget to push the last one
		bvh_mgn.push_back( current );
	}
}


void threadedDelocalizer::compute_delocalization_ityp()
{
	//delocalize all ions of predefined ityp within predefined region
	if ( participating == false || status != MYDELOC_SUCCESS ) {
		return;
	}
	apt_real l = glo_grid_info.dx; //##MK::always using cubic voxel currently
	double L = (double) l;
	apt_int a = kernel_halfsize;
	apt_int kernelwidth = (2*a)+1;
	double aL = (double) a * L;
	double sigma = kernel_sigma;
	double sqrtHalfPi = sqrt(MYPI/2.0);
	double sqrt2 = sqrt(2.0);
	//normalization constant to assure that the contribution of the entire truncated Gaussian in the kernel adds up to 1 so that
	//the "essence" of each ion is completely accounted for
	//IVAS/AP Suite does a so called atomic decomposition so that one C:2 molecular iontype gives 2 counts for C
	//##MK::here is where the multiplier should come
	double A = 1.0 / (	(sqrtHalfPi*2*1.0*sigma * erf(aL/(sqrt2*1.0*sigma))) *
						(sqrtHalfPi*2*1.0*sigma * erf(aL/(sqrt2*1.0*sigma))) *
						(sqrtHalfPi*2*0.5*sigma * erf(aL/(sqrt2*0.5*sigma))));

	for( size_t bin = 0; bin < bvh_mgn.size(); bin++ ) {
		bin_mgn thisbin = bvh_mgn[bin];
		vector<apt_int> ixiyiz = glo_grid_info.where_xyz_no_checks( thisbin.binidx );

		//define cache-friendly thread local kernel which we fill with contributions from all the ions which have an "essence" to contribute in this kernel region
		vector<double> f64 = vector<double>( CUBE(kernelwidth), 0. );

		//now we have a bunch of ions in the same bin so we can reuse some temporaries during the kernel evaluation
		for( size_t idx = thisbin.start; idx < thisbin.onepastend; idx++ ) {
			double ddx = glo_grid_info.where_x_no_checks_f64( points[idx].x );
			double ddy = glo_grid_info.where_y_no_checks_f64( points[idx].y );
			double ddz = glo_grid_info.where_z_no_checks_f64( points[idx].z );

			//delocalize this ion via analytical integration of the 3D integral for each voxel in the kernel
			for( apt_int z = -1*a; z <= a; z++ ) {
				double zz = (double) z;
				double zmi = ( (zz - ddz)*L > -aL ) ? (zz - ddz)*L : -aL;
				double zmx = ( (zz + 1.0 - ddz)*L < aL ) ? (zz + 1.0 - ddz)*L  : aL;
				apt_int zoff = (1*a + z)*SQR(kernelwidth);
				for( apt_int y = -1*a; y <= a; y++ ) {
					double yy = (double) y;
					double ymi = ( (yy - ddy)*L > -aL ) ? (yy - ddy)*L : -aL;
					double ymx = ( (yy + 1.0 - ddy)*L < aL ) ? (yy + 1.0 - ddy)*L  : aL;
					apt_int yzoff = (1*a + y)*kernelwidth + zoff;
					for( apt_int x = -1*a; x <= a; x++ ) {
						double xx = (double) x;
						double xmi = ( (xx - ddx)*L > -aL ) ? (xx - ddx)*L : -aL;
						double xmx = ( (xx + 1.0 - ddx)*L < aL ) ? (xx + 1.0 - ddx)*L  : aL;
						apt_int xyzpos = (1*a + x) + yzoff;
						//integrated essence of the ion in this voxel
						double vl = A *
							(sqrtHalfPi*1.0*sigma*( erf(xmx/(sqrt2*1.0*sigma)) - erf(xmi/(sqrt2*1.0*sigma)) )  ) *
							(sqrtHalfPi*1.0*sigma*( erf(ymx/(sqrt2*1.0*sigma)) - erf(ymi/(sqrt2*1.0*sigma)) )  ) *
							(sqrtHalfPi*0.5*sigma*( erf(zmx/(sqrt2*0.5*sigma)) - erf(zmi/(sqrt2*0.5*sigma)) )  );

						//bookkeeping
						size_t kernelpos = (size_t) xyzpos;
						f64[kernelpos] += vl;
					} //next kernel x
				} //next kernel y
			} //next kernel z
		} //next ion also in this bin

		//transfer all result for the kernel to cnts_f64
		apt_int mygrid_xyzoffset = zbounds_halo.first * glo_grid_info.nxy;
		for( apt_int z = -1*a; z <= a; z++ ) {
			apt_int localzoff = (1*a + z)*SQR(kernelwidth);
			apt_int globalzoff = (ixiyiz[2] + z)*glo_grid_info.nxy;
			for( apt_int y = -1*a; y <= a; y++ ) {
				apt_int localyzoff = (1*a + y)*kernelwidth + localzoff;
				apt_int globalyzoff = (ixiyiz[1] + y)*glo_grid_info.nx + globalzoff;
				for( apt_int x = -1*a; x <= a; x++ ) {
					apt_int local_xyz = (1*a + x) + localyzoff;
					size_t local_here = (size_t) local_xyz;
					apt_int global_xyz = ((ixiyiz[0] + x) + globalyzoff) - mygrid_xyzoffset;
					size_t global_here = global_xyz;
					cnts_f64[global_here] += f64[local_here];
				} //x
			} //y
		} //z
	} //next bin

	/*
	//##MK::BEGIN DEBUG
	#pragma omp critical
	{
		double cnts_sum = 0.;
		for( size_t vxl = 0; vxl < cnts_f64.size(); vxl++ ) {
			cnts_sum += cnts_f64[vxl];
		}
		cout << "\t\t" << omp_get_thread_num() << "\t" << setprecision(32) << cnts_sum << "\n";
	}
	//##MK::END DEBUG
	*/
}
