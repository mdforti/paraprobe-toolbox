/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_COMPOSITION_HDL_H__
#define __PARAPROBE_NANOCHEM_COMPOSITION_HDL_H__

#include "PARAPROBE_NanochemInterfaceHdl.h"


class compositionHdl
{
public:
	compositionHdl();
	~compositionHdl();

	bool multithreaded_composition_analysis_with_autorois(
			composition_task_info const & tskifo,
			rangeTable & rng,
			LinearTimeVolQuerier<p3dmidx> & ionbvh,
			vector<tri3d> const & edge_model_triangles,
			vector<tri3d> const & feature_model_triangles,
			vector<p3d> const & edge_model_facet_normals,
			vector<apt_uint> const & feature_model_facet_patchids );

	bool multithreaded_composition_analysis_with_autorois(
			composition_task_info & tskifo,
			rangeTable & rng,
			LinearTimeVolQuerier<p3dmidx> & ionbvh,
			vector<tri3d> const & edge_model_triangles);

	size_t write_automated_rois_for_composition_h5(
			composition_task_info const & tskifo );

	vector<roi_rotated_cylinder*> rois_cyl;
	//##MK::vector<roi_rotated_cuboid*> rois_obb;
};

#endif
