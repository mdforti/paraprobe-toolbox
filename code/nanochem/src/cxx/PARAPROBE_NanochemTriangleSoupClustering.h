/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_TRIANGLESOUP_CLUSTERING_H__
#define __PARAPROBE_NANOCHEM_TRIANGLESOUP_CLUSTERING_H__

#include "PARAPROBE_NanochemGPU.h"


class triangleSoupClustering
{
public:
	triangleSoupClustering();
	~triangleSoupClustering();

	bool build_triangles_rtree();
	apt_uint clustering_triangle_distance_based( const apt_real eps );

	vector<p3d> vrts;
	vector<tri3u> fcts;
	vector<p3d> nrms; //##MK::component average of the three adjacent vertex normals, renormalized, but not flipped in any way !
	vector<unrm_info> nrms_quality;
	//first: SQR(magnitude of guiding gradient)
	//second: cosine of the angle between, initialized to FMX to indicate no information is available

	vector<apt_uint> fcts2objid;
	apt_uint nobjects;

private:
	Tree* rtree;
};

#endif
