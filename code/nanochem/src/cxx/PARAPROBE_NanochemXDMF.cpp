/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemXDMF.h"


nanochem_xdmf::nanochem_xdmf()
{

}

nanochem_xdmf::~nanochem_xdmf()
{

}


int nanochem_xdmf::create_autorois_cylinder_file( const string xmlfn,
		const apt_uint simid, const apt_uint tskid,
		const size_t nrois, const size_t ngons, const string h5fn )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		string fwslash = "/";

		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"roi cylinder set\" GridType=\"Uniform\">" << "\n";
		//rois are cylinders, cylinders are geometric primitives which cannot be visualized in xdmf out of the box
		//instead we approximate the cylinders - only for the visualization - as regular ngons.
		//specifically we approximate the lateral surface via a regular 360-gon faces , which gives
		//a reasonably smooth approximation. In this case, the ngons is a polyhedron with 360 side faces and a top and bottom face
		//so in total the topology information is stored as a xdmf polyhedron keyword (3), number of vertices for the top face (360),
		//followed by 360 vertex ids, a set of 360 blocks of planar quads follows, each specified by xdmf polyhedron keyword 3, number of vertices (4) and
		//followed by four respective vertex ids, the last block is again structured like the one for the top face
		//thus in total

		apt_uint entry_id = 1;
		string prefix = "/entry" + to_string(entry_id) + "/oned_profile/xdmf_cylinder";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nrois * (ngons + 2) << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nrois * ((1+1+ngons) + ngons*(1+1+4) + (1+1+ngons)) << " 1\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5fn << ":" << prefix + "/xdmf_topology" << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nrois * (ngons * 2) << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5fn << ":" << prefix + "/vertices" << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";
		//each regular ngons is closed so has top, bottom, and ngons faces, realized here as repeated ids for each face belonging to the polyhedron

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Cell\" Name=\"RoiCylinderID\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nrois * (1 + ngons + 1) << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5fn << ":" << prefix + "/identifier" << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		/*
		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Cell\" Name=\"RoiCloseToTheEdge\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nrois * (1 + ngons + 1) << " 1\" DataType=\"UChar\" Precision=\"1\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5fn << ":" << prefix + "/edge_contact" << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
		*/

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Cell\" Name=\"RoiNumberOfIons\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nrois * (1 + ngons + 1) << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5fn << ":" << prefix + "/number_of_ions" << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Cell\" Name=\"RoiNumberOfAtoms\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nrois * (1 + ngons + 1) << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5fn << ":" << prefix + "/number_of_atoms" << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";


		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}
