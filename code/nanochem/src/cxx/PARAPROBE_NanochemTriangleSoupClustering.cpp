/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemTriangleSoupClustering.h"


triangleSoupClustering::triangleSoupClustering()
{
	vrts = vector<p3d>();
	fcts = vector<tri3u>();
	nrms = vector<p3d>();
	nrms_quality = vector<unrm_info>();
	fcts2objid = vector<apt_uint>();
	nobjects = 0;
	rtree = NULL;
}


triangleSoupClustering::~triangleSoupClustering()
{
	vrts = vector<p3d>();
	fcts = vector<tri3u>();
	nrms = vector<p3d>();
	nrms_quality = vector<unrm_info>();
	fcts2objid = vector<apt_uint>();
	nobjects = 0;
	if ( rtree != NULL ) {
		delete rtree; rtree = NULL;
	}
}


bool triangleSoupClustering::build_triangles_rtree()
{
	//eventually executed locally for each thread
	double tic = omp_get_wtime();

	//builds an AABBTree / RTree of triangles with respect to the global space the triangles occupy in $\mathcal{R}^3$
	if ( fcts.size() < 1 ) {
		cout << "triangleSoupClustering::build_triangles_rtree thread " << omp_get_thread_num() << " there are no fcts to build an RTree for !" << "\n";
		return true;
	}
	if ( fcts.size() >= (size_t) IMX ) {
		cerr << "triangleSoupClustering::build_triangles_rtree thread " << omp_get_thread_num() << " currently at most " << IMX-1 << " triangles are supported !" << "\n";
		return false;
	}
	if ( rtree != NULL ) {
		cerr << "triangleSoupClustering::build_triangles_rtree thread " << omp_get_thread_num() << " an Rtree already exists !" << "\n";
		return false;
	}
	try {
		rtree = new class Tree( fcts.size() );
	}
	catch (bad_alloc &trisoupexc) {
		cerr << "triangleSoupClustering::build_triangles_rtree thread " << omp_get_thread_num() << " unable to allocate memory for the rtree !" << "\n";
		return false;
	}

	apt_real xmi = RMX;
	apt_real xmx = RMI;
	apt_real ymi = RMX;
	apt_real ymx = RMI;
	apt_real zmi = RMX;
	apt_real zmx = RMI;
	apt_uint triID = 0;
	for ( auto it = fcts.begin(); it != fcts.end(); it++ ) {
		//##MK::fattening to assure that edge touching of boxes results in inclusion tests
		xmi = fmin( fmin(vrts[it->v1].x, vrts[it->v2].x), vrts[it->v3].x );
		xmx = fmax( fmax(vrts[it->v1].x, vrts[it->v2].x), vrts[it->v3].x );
		ymi = fmin( fmin(vrts[it->v1].y, vrts[it->v2].y), vrts[it->v3].y );
		ymx = fmax( fmax(vrts[it->v1].y, vrts[it->v2].y), vrts[it->v3].y );
		zmi = fmin( fmin(vrts[it->v1].z, vrts[it->v2].z), vrts[it->v3].z );
		zmx = fmax( fmax(vrts[it->v1].z, vrts[it->v2].z), vrts[it->v3].z );

		rtree->insertParticle( triID,
				trpl(xmi - EPSILON, ymi - EPSILON, zmi - EPSILON),
				trpl(xmx + EPSILON, ymx + EPSILON, zmx + EPSILON) );
		triID++;
	}

	double toc = omp_get_wtime();

	cout << "triangleSoupClustering::build_triangles_rtree thread " << omp_get_thread_num() << " rtree built, took " << (toc-tic) << " seconds" << "\n";
	return true;
}


apt_uint triangleSoupClustering::clustering_triangle_distance_based( const apt_real eps )
{
	if ( build_triangles_rtree() == true ) {
		cout << "triangleSoupClustering::build_triangle_rtree was successful" << "\n";
	}
	else {
		cerr << "triangleSoupClustering::build_triangle_rtree failed !" << "\n";
		return UMX;
	}

	//run the clustering algorithm
	fcts2objid = vector<apt_uint>( fcts.size(), 0 );
	bool* visited = new bool[fcts.size()];
	for( size_t i = 0; i < fcts.size(); i++ ) {
		visited[i] = false;
	}
	//##MK::use UMX to mark unvisited

	//cout << "triangleSoupClustering::clustering_triangle_distance_based ..." << "\n";

	apt_uint clusterID = 0;
	apt_real R = eps; //ConfigNanochem::TriSoupClusteringMaxDistance;
	for( size_t triID = 0; triID < fcts.size(); triID++ ) { //inspect each triangle
		//mark the triangle as visited
		if ( visited[triID] == false ) {
			visited[triID] = true;
			//find all neighboring triangles to triangle triID
			tri3u thisone = fcts[triID];

			tri3d me = tri3d(
					vrts[thisone.v1].x, vrts[thisone.v1].y, vrts[thisone.v1].z,
					vrts[thisone.v2].x, vrts[thisone.v2].y, vrts[thisone.v2].z,
					vrts[thisone.v3].x, vrts[thisone.v3].y, vrts[thisone.v3].z );


			//find potential neighboring triangles, make a coarse query first using cuboids with diagonals larger than R/2 adds a little bit too many tests
			apt_real xmi = fmin( fmin(me.x1, me.x2), me.x3 );
			apt_real xmx = fmax( fmax(me.x1, me.x2), me.x3 );
			apt_real ymi = fmin( fmin(me.y1, me.y2), me.y3 );
			apt_real ymx = fmax( fmax(me.y1, me.y2), me.y3 );
			apt_real zmi = fmin( fmin(me.z1, me.z2), me.z3 );
			apt_real zmx = fmax( fmax(me.z1, me.z2), me.z3 );
			hedgesAABB e_aabb( 	trpl( xmi - R - EPSILON, ymi - R - EPSILON, zmi - R - EPSILON),
								trpl( xmx + R + EPSILON, ymx + R + EPSILON, zmx + R + EPSILON) );
			vector<apt_uint> nbids = rtree->query( e_aabb );
			//identify what are the corresponding triangles to check the minimum analytical distance for to check if they are really within R
			vector<tri3d> refTri;
			for ( size_t ii = 0; ii < nbids.size(); ii++ ) {
				apt_uint nbtriID = nbids[ii];

				tri3u fcts_nb = fcts[nbtriID];
				refTri.push_back( tri3d(
						vrts[fcts_nb.v1].x, vrts[fcts_nb.v1].y, vrts[fcts_nb.v1].z,
						vrts[fcts_nb.v2].x, vrts[fcts_nb.v2].y, vrts[fcts_nb.v2].z,
						vrts[fcts_nb.v3].x, vrts[fcts_nb.v3].y, vrts[fcts_nb.v3].z ) );
			}
			//cout << "triID\t\t" << triID << "\t\t" << "nbors.size()" << "\t\t" << nbors.size() << "\n";

			//assign clusterID
			clusterID++;
			fcts2objid[triID] = clusterID;

			//process neighboring triangles
			size_t k = 0;
			while ( k < nbids.size() )
			{
				unsigned int nbtriID = nbids[k];
				if ( visited[nbtriID] == false ) {
					tri3d T1 = refTri[k];
					tri3u t2 = fcts[nbtriID];
					tri3d T2 = tri3d( 	vrts[t2.v1].x, vrts[t2.v1].y, vrts[t2.v1].z,
										vrts[t2.v2].x, vrts[t2.v2].y, vrts[t2.v2].z,
										vrts[t2.v3].x, vrts[t2.v3].y, vrts[t2.v3].z  ); //triangles[nbtriID];
					//check if shortest distance between any two points on triangles[triID] and triangles[ntriID] is not more than R
					//like we are standing at the shore of the Matotschkin Schar on Nowaja Semlja
					//PQP_REAL TriDist(PQP_REAL P[3], PQP_REAL Q[3], const PQP_REAL S[3][3], const PQP_REAL T[3][3])
					PQP_REAL PP[3] = {0.0, 0.0, 0.0};
					PQP_REAL QQ[3] = {0.0, 0.0, 0.0};
					PQP_REAL SS[3][3];
					SS[0][0] = T1.x1; SS[0][1] = T1.y1; SS[0][2] = T1.z1;
					SS[1][0] = T1.x2; SS[1][1] = T1.y2; SS[1][2] = T1.z2;
					SS[2][0] = T1.x3; SS[2][1] = T1.y3; SS[2][2] = T1.z3;
					PQP_REAL TT[3][3];
					TT[0][0] = T2.x1; TT[0][1] = T2.y1; TT[0][2] = T2.z1;
					TT[1][0] = T2.x2; TT[1][1] = T2.y2; TT[1][2] = T2.z2;
					TT[2][0] = T2.x3; TT[2][1] = T2.y3; TT[2][2] = T2.z3;

					if ( TriDist( PP, QQ, SS, TT ) <= R ) {
						//mark the neighbor as associated with that cluster
						visited[nbtriID] = true;
						fcts2objid[nbtriID] = clusterID;

						//filter the neighboring triangles of neighbor nbtriID
						apt_real nbxmi = fmin( fmin( T2.x1, T2.x2 ), T2.x3 );
						apt_real nbxmx = fmax( fmax( T2.x1, T2.x2 ), T2.x3 );
						apt_real nbymi = fmin( fmin( T2.y1, T2.y2 ), T2.y3 );
						apt_real nbymx = fmax( fmax( T2.y1, T2.y2 ), T2.y3 );
						apt_real nbzmi = fmin( fmin( T2.z1, T2.z2 ), T2.z3 );
						apt_real nbzmx = fmax( fmax( T2.z1, T2.z2 ), T2.z3 );
						hedgesAABB nb_aabb( trpl( nbxmi - R - EPSILON, nbymi - R - EPSILON, nbzmi - R - EPSILON),
												trpl( nbxmx + R + EPSILON, nbymx + R + EPSILON, nbzmx + R + EPSILON) );

						vector<apt_uint> nbnbids = rtree->query( nb_aabb );
						for( auto jt = nbnbids.begin(); jt != nbnbids.end(); jt++ ) {
							nbids.push_back( *jt );
							refTri.push_back( T2 ); //MK::this is a subtility of such DBScan traversal algorithms
							//when they are written using a recursive but for-loop implementation
							//then the reference triangle to check the analytical distance for also changes
							//here this is the reference triangle
							//so we can imagine we crawl over the triangle of the triangle soup and check
							//for each new ensemble of neighbors whether these neighbors are not fartherer away from their current
							//reference triangle
						} //finished merging all new neighbors and their reference triangles
					}
				}
				//increment
				k++;
			} //end of traversal of neighboring triangles to me within at most an analytical minimum distance of R

			//cout << "triID\t\t" << triID << "\t\t" << "nbids.size() " << "\t\t" << nbids.size() << "\n";
		}
	} //next unvisited triangle

	nobjects = clusterID;
	cout << "A total of " << clusterID << " connected regions were found" << "\n";

	if ( visited != NULL ) {
		delete [] visited; visited = NULL;
	}

	return clusterID;
}
