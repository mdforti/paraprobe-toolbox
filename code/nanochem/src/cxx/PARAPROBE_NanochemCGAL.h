/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_CGAL_INTERFACE_H__
#define __PARAPROBE_NANOCHEM_CGAL_INTERFACE_H__


#include "PARAPROBE_ConfigNanochem.h"

//If the macro CGAL_HAS_THREADS is not defined, then CGAL assumes it can use any thread-unsafe code (such as static variables).
//By default, this macro is not defined, unless BOOST_HAS_THREADS or _OPENMP is defined. It is possible to force its definition on the command line,
//and it is possible to prevent its default definition by setting CGAL_HAS_NO_THREADS from the command line.
#define CGAL_HAS_THREADS
//https://doc.cgal.org/latest/Manual/preliminaries.html#Preliminaries_thread_safety
#define I_ASSUME_CGAL_PMP_IS_THREADSAFE

//https://doc.cgal.org/latest/Polygon_mesh_processing/Polygon_mesh_processing_2orient_polygon_soup_example_8cpp-example.html#a3

#define TRIANGLE_SOUP_ANALYSIS_ERROR						-1
#define TRIANGLE_SOUP_ANALYSIS_NO_SUPPORT					-2
#define TRIANGLE_SOUP_ANALYSIS_INSUFFICIENT_SUPPORT			-3
#define TRIANGLE_SOUP_ANALYSIS_ERROR_REPAIRING				-4
#define TRIANGLE_SOUP_ANALYSIS_SELFINTERSECTING				-5
#define TRIANGLE_SOUP_ANALYSIS_BORDEREDGES					-6
#define TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH_FAIRING_ERROR	-7
#define TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH_CLOSURE_ERROR	-8

#define TRIANGLE_SOUP_ANALYSIS_POLYHEDRON_CLOSED			 0
#define TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH_WITH_PROXY		 1


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>

#include <CGAL/optimal_bounding_box.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>

#include <CGAL/Polygon_mesh_processing/measure.h>

#include <CGAL/Polygon_mesh_processing/repair_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
#include <CGAL/Real_timer.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>
#include <boost/property_map/property_map.hpp>


//#include <algorithm>
#include <array>
//#include <iostream>
//#include <vector>
typedef CGAL::Exact_predicates_inexact_constructions_kernel     K;
typedef K::FT                                                   FT;
typedef K::Point_3                                              Point; //Point_3;
typedef K::Vector_3												Vector;
typedef CGAL::Surface_mesh<Point> 								Mesh; //<Point_3> Mesh;
typedef std::array<FT, 3>                                       Custom_point;
typedef std::vector<std::size_t>                                CGAL_Polygon;
namespace PMP = CGAL::Polygon_mesh_processing;

/*
//##MK::already defined in paraprobe-utils/CGALInterface.h
struct Array_traits
{
	struct Equal_3
	{
		bool operator()(const Custom_point& p, const Custom_point& q) const {
			return (p == q);
		}
	};
	struct Less_xyz_3
	{
		bool operator()(const Custom_point& p, const Custom_point& q) const {
			return std::lexicographical_compare(p.begin(), p.end(), q.begin(), q.end());
		}
	};
	Equal_3 equal_3_object() const {
		return Equal_3();
	}
	Less_xyz_3 less_xyz_3_object() const {
		return Less_xyz_3();
	}
};
*/

#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
typedef CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3> Polyhedron;
typedef vector<size_t> 											Polygon;

typedef boost::graph_traits<Polyhedron>::vertex_descriptor vertex_descriptor;
typedef boost::graph_traits<Polyhedron>::face_descriptor   face_descriptor;


//surface mesh creation in prep for obb approximation and actual obb approximation
/*
#include <CGAL/Simple_cartesian.h>
typedef CGAL::Simple_cartesian<apt_real> OBBPrepK;
typedef CGAL::Surface_mesh<OBBPrepK::Point_3> OBBPrepMesh;
typedef OBBPrepMesh::Vertex_index vertex_descriptor;
typedef OBBPrepMesh::Face_index face_descriptor;
*/


//minimum ellipsoids
#include <CGAL/Cartesian_d.h>
#include <CGAL/MP_Float.h>
#include <CGAL/point_generators_d.h>
#include <CGAL/Approximate_min_ellipsoid_d.h>
#include <CGAL/Approximate_min_ellipsoid_d_traits_d.h>
typedef CGAL::Cartesian_d<double>                              Kernel;
typedef CGAL::MP_Float                                         ET;
typedef CGAL::Approximate_min_ellipsoid_d_traits_d<Kernel, ET> Traits;
typedef Traits::Point                                          AppPoint;
typedef std::vector<AppPoint>                                  Point_list;
typedef CGAL::Approximate_min_ellipsoid_d<Traits>              AME;


//point-in-tetrahedron
typedef CGAL::Polyhedron_3<K> MyTetrahedron;
#include <CGAL/Side_of_triangle_mesh.h>



//debug includes for testing automated fitting of planes for interfacial excess computations
/////////////////////////////////////////////////////////////////////////////////////////pca
#include <CGAL/Simple_cartesian.h>
#include <CGAL/linear_least_squares_fitting_3.h>
//#include <vector>
//typedef double                      	FT;
typedef CGAL::Simple_cartesian<double>  IE_K;
//typedef K::Line_3                   	Line;
typedef IE_K::Plane_3                  	IE_Plane;
typedef IE_K::Point_3                  	IE_Point;
//typedef K::Triangle_3               	Triangle;

/////////////////////////////////////////////////////////////////////////////////////////mesh slicer
//namespace PMP = CGAL::Polygon_mesh_processing;
//typedef CGAL::Exact_predicates_inexact_constructions_kernel    K;
//typedef K::Point_3                                             Point3; --> Point
//typedef CGAL::Surface_mesh<Point3>                             Mesh;

#include <CGAL/Polygon_mesh_slicer.h>
#include <CGAL/AABB_halfedge_graph_segment_primitive.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>

typedef std::vector<Point> 										Polyline_type;
typedef std::list<Polyline_type> 								Polylines;
typedef CGAL::AABB_halfedge_graph_segment_primitive<Mesh> 		HGSP;
typedef CGAL::AABB_traits<K, HGSP>    							AABB_traits;
typedef CGAL::AABB_tree<AABB_traits>  							AABB_tree;

/*
#include <CGAL/Polygon_2.h>
typedef CGAL::Point_2<K> 										Point2;
typedef CGAL::Polygon_2<K> 										Polygon_2;
typedef Polygon_2::Vertex_iterator 								VertexIterator;
typedef Polygon_2::Edge_const_iterator 							EdgeIterator;

#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Polygon_2_algorithms.h>

typedef CGAL::Polygon_with_holes_2<K> Polygon_with_holes_2;
using std::cout; using std::endl;
*/

/////////////////////////////////////////////////////////////////////////////////////////triangulate polyline
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_mesh_processing/triangulate_hole.h>
//#include <CGAL/utility.h>
//#include <vector>
//#include <iterator>
//typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
//typedef Kernel::Point_3                                     Point;

/////////////////////////////////////////////////////////////////////////////////////////polyline to surface mesh


/////////////////////////////////////////////////////////////////////////////////////////isotropic remeshing
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/remesh.h>
#include <CGAL/Polygon_mesh_processing/border.h>
//#include <CGAL/Polygon_mesh_processing/IO/polygon_mesh_io.h>
#include <boost/iterator/function_output_iterator.hpp>
//#include <fstream>
//#include <vector>
//#include <string>
//typedef CGAL::Exact_predicates_inexact_constructions_kernel   K;
//typedef CGAL::Surface_mesh<K::Point_3>                        Mesh;
typedef boost::graph_traits<Mesh>::halfedge_descriptor        halfedge_descriptor;
typedef boost::graph_traits<Mesh>::edge_descriptor            edge_descriptor;
//namespace PMP = CGAL::Polygon_mesh_processing;

/////////////////////////////////////////////////////////////////////////////////////////compute vrts, fcts normals
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Surface_mesh.h>
////#include <CGAL/Polygon_mesh_processing/compute_normal.h>
//#include <CGAL/Polygon_mesh_processing/IO/polygon_mesh_io.h>
//#include <fstream>
//#include <iostream>
//typedef CGAL::Exact_predicates_inexact_constructions_kernel       K;
//typedef K::Point_3                                                Point;
//typedef K::Vector_3                                               Vector;
//typedef CGAL::Surface_mesh<Point>                                 Surface_mesh; --> Mesh
typedef boost::graph_traits<Mesh>::vertex_descriptor      		sm_vertex_descriptor;
typedef boost::graph_traits<Mesh>::face_descriptor        		sm_face_descriptor;
//namespace PMP = CGAL::Polygon_mesh_processing;

/////////////////////////////////////////////////////////////////////////////////////////check for self-intersections
#include <CGAL/Polygon_mesh_processing/self_intersections.h>


/////////////////////////////////////////////////////////////////////////////////////////hole filling
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/triangulate_hole.h>
#include <CGAL/Polygon_mesh_processing/border.h>
//#include <CGAL/Polygon_mesh_processing/IO/polygon_mesh_io.h>
#include <boost/lexical_cast.hpp>
//#include <iostream>
//#include <iterator>
//#include <string>
#include <tuple>
//#include <vector>
//typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
//typedef Kernel::Point_3                                     Point;
//typedef CGAL::Surface_mesh<Point>                           Mesh;
//typedef boost::graph_traits<Mesh>::vertex_descriptor        vertex_descriptor;
//typedef boost::graph_traits<Mesh>::halfedge_descriptor      halfedge_descriptor;
//typedef boost::graph_traits<Mesh>::face_descriptor          face_descriptor;
//namespace PMP = CGAL::Polygon_mesh_processing;

#endif
