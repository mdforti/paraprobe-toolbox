/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_XDMFHDL_H__
#define __PARAPROBE_NANOCHEM_XDMFHDL_H__

#include "PARAPROBE_NanochemHDF5.h"


class nanochem_xdmf : public xdmfBaseHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	nanochem_xdmf();
	~nanochem_xdmf();

	int create_autorois_cylinder_file( const string xmlfn, const apt_uint simid, const apt_uint tskid,
			const size_t nrois, const size_t ngons, const string h5fn );

//private:
};

#endif
