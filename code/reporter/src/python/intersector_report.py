#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Python post-processing of frequently executed analyses for the intersector tool
#

import numpy as np
import h5py


class ReporterIntersector():
    """Support the user with using results from paraprobe-intersector runs."""

    def __init__(self, results_file, entry_id=1):
        self.tool_name = "Intersector"
        self.tool_hash = "0"
        self.results_file = results_file
        self.entry_id = entry_id
        self.results = {}

    def get_coprecipitation_stats_rielli_primig(self, task_id=1):
        """Specialized reporting of coprecipitates by V. V. Rielli et al."""
        with h5py.File(self.results_file, "r") as h5r:
            dsnm = f"/entry{self.entry_id}/v_v_spatial_correlation{task_id}/" \
                   f"coprecipitation_analysis/cluster_statistics"
            if dsnm not in h5r:
                raise LookupError(f"{dsnm} does not exist in {self.results_file} !")
            stats = h5r[dsnm][:, :]
            if stats[0, 0] != 0:
                raise ValueError(f"{dsnm} incorrectly formatted, first entry should be 0!")
            ntotal = stats[0, 1]
            print(f"There are {stats[0, 1]} clusters in total")
            for i in np.arange(1, np.shape(stats)[0]):
                nlet = stats[i, 1]
                fraction = np.around(
                    np.float32(nlet) / np.float32(ntotal) * 100., decimals=3)
                print(f"There are {nlet} {i}-lets, {fraction}%")
            if np.shape(stats)[0] >= 3:
                nlet_higher_order = np.sum(stats[3:np.shape(stats)[0], 1])
                fraction = np.around(
                    np.float32(nlet_higher_order) / np.float32(ntotal) * 100., decimals=3)
                print(f"i.e. {nlet_higher_order} higher-order nlets, {fraction}%")

            # report also how many current and next monoliths
            dsnm = f"/entry{self.entry_id}/v_v_spatial_correlation{task_id}/" \
                f"coprecipitation_analysis/cluster_composition"
            if dsnm not in h5r:
                raise LookupError(f"{dsnm} does not exist in {self.results_file} !")
            cluster_composition = h5r[dsnm][:, :]
            # 0-th column how many members in cluster of current_set
            # 1-th column how many members in cluster of next_set
            # 2-th column how many members in cluster in total
            # as many rows as clusters
            mask = cluster_composition[:, 2] == 1
            mask &= cluster_composition[:, 0] == 1
            n_curr = np.sum(mask)
            fraction = np.around(
                np.float32(n_curr) / np.float32(ntotal) * 100., decimals=3)
            print(f"Current monoliths {n_curr}, {fraction}%")
            del mask
            mask = cluster_composition[:, 2] == 1
            mask &= cluster_composition[:, 1] == 1
            n_next = np.sum(mask)
            fraction = np.around(
                np.float32(n_next) / np.float32(ntotal) * 100., decimals=3)
            print(f"Next monoliths {n_next}, {fraction}%")
