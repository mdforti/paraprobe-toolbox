/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_TETRAHEDRALIZE_H__
#define __PARAPROBE_INTERSECTOR_TETRAHEDRALIZE_H__


#include "PARAPROBE_IntersectorGPU.h"

/*
//this function uses functionalities of TetGen which is under an AGPLV3+
//use tetgen, specify custom location
//#include "../../../thirdparty/mandatory/tetgen/tetgen1.5.1/tetgen.h"
#include "tetgen.h"
*/

class tetrahedralizer
{
public:

	tetrahedralizer();
	~tetrahedralizer();

	/*
	bool tessellate( vector<p3d> const & vrts_in, vector<tri3u> const & fcts_in, vector<p3d> & vrts_out, vector<quad3u> & cells_out );
	*/

	double dt_tessellate;
	bool is_tessellated;
};


#endif
