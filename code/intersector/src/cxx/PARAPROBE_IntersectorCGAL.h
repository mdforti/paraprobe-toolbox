/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_CGAL_INTERFACE_H__
#define __PARAPROBE_INTERSECTOR_CGAL_INTERFACE_H__

#include "CONFIG_Intersector.h"

//If the macro CGAL_HAS_THREADS is not defined, then CGAL assumes it can use any thread-unsafe code (such as static variables).
//By default, this macro is not defined, unless BOOST_HAS_THREADS or _OPENMP is defined. It is possible to force its definition on the command line,
//and it is possible to prevent its default definition by setting CGAL_HAS_NO_THREADS from the command line.
#define CGAL_HAS_THREADS
//https://doc.cgal.org/latest/Manual/preliminaries.html#Preliminaries_thread_safety
#define I_ASSUME_CGAL_PMP_IS_THREADSAFE


//definitions required for analyses based on the work of S. Hornus to detect overlap between two arbitrary tetrahedra


//definitions required for computing the intersection volume of two arbitrary tetrahedra via nef polyhedra functions of CGAL

//intersection volume and mesh of intersection region using nef polyhedra
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/boost/graph/convert_nef_polyhedron_to_polygon_mesh.h>
/*
#include <iostream>
#include <sstream>
#include <fstream>
*/
typedef CGAL::Exact_predicates_exact_constructions_kernel Exact_kernel;
typedef CGAL::Polyhedron_3<Exact_kernel> YourPolyhedron;
typedef CGAL::Surface_mesh<Exact_kernel::Point_3> Surface_mesh;
typedef Exact_kernel::Point_3 MyPoint;
typedef CGAL::Nef_polyhedron_3<Exact_kernel> Nef_polyhedron;

#include <CGAL/Polygon_mesh_processing/measure.h>
namespace PMP = CGAL::Polygon_mesh_processing;

/*
//tetDisjointCGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Point_3.h>
#include <CGAL/intersections.h>
//#include <CGAL/convex_hull_3.h>
//#include <CGAL/Polyhedron_3.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_3 Point_3;
typedef K::Triangle_3 Triangle_3;
typedef K::Tetrahedron_3 Tetrahedron_3;
typedef CGAL::Polyhedron_3<K> MyPolyhedron;
*/

#endif
