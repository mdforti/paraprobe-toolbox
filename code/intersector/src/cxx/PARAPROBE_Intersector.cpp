/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_IntersectorHdl.h"


bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-intersector" );
	if ( ConfigIntersector::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void execute_object_intersection_analysis_tasks( const int r, const int nr, char** pargv )
{
	//allocate process-level worker instance. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	intersectorHdl* isct = NULL;
	int localhealth = 1;
	try {
		isct = new intersectorHdl;
		isct->set_myrank(r);
		isct->set_nranks(nr);
		//isct->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating intersectorHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board, prep results file in case of multiple processes?

	//MPI_Barrier( MPI_COMM_WORLD );
	if ( isct->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << isct->get_myrank() << " has recognized that not all data have arrived at the master !" << "\n";
		delete isct; isct = NULL; return;
	}
	
	isct->execute_v_v_workpackage();

	//release resources
	delete isct; isct = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "intersector" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Intersector.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "intersector", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize();
		return 0;
	}
	else {
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		execute_object_intersection_analysis_tasks( r, nr, argv );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-intersector is implemented for a single process only !" << "\n";
	}

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-intersector success" << endl;
	}
	return 0;
}
