/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_STRUCTS_H__
#define __PARAPROBE_INTERSECTOR_STRUCTS_H__

#include "PARAPROBE_IntersectorPQPInterface.h"


/*
struct timedep_graph_node
{
	double volume_sum;
	apt_uint atom_total_sum;
	apt_uint atom_cand_sum;
	apt_uint t;
	timedep_graph_node() : volume_sum(0.), atom_total_sum(0), atom_cand_sum(0), t(0) {}
	timedep_graph_node( const double _vol, const apt_uint _atm_total_sum, const apt_uint _atm_cand_sum, const apt_uint _step ) :
		volume_sum(_vol), atom_total_sum(_atm_total_sum), atom_cand_sum(_atm_cand_sum), t(_step) {}
};

ostream& operator << (ostream& in, timedep_graph_node const & val);


inline bool SortAscTimeDepNodes( const timedep_graph_node & a, const timedep_graph_node & b )
{
	return a.t < b.t;
}
*/

#define LINK_TYPE_VOLUMETRIC_OVERLAP			0x00
#define LINK_TYPE_PROXIMITY						0x01
#define LINK_TYPE_UNKNOWN						0xFF

struct forward_link
{
	apt_uint src_id;
	apt_uint trg_id;
	unsigned char type;
	unsigned char pad1;
	unsigned char pad2;
	unsigned char pad3;
	//apt_real weight;
	forward_link() :
		src_id(UMX), trg_id(UMX), type(LINK_TYPE_UNKNOWN), pad1(0x00), pad2(0x00), pad3(0x00) {}
	forward_link( const apt_uint _srcid, const apt_uint _trgid, const unsigned char _type ) :
		src_id(_srcid), trg_id(_trgid), type(_type), pad1(0x00), pad2(0x00), pad3(0x00) {}
};


ostream& operator << (ostream& in, forward_link const & val);


inline bool SortForwardLinks( const forward_link & a, const forward_link & b )
{
	return a.src_id < b.src_id || (a.src_id == b.src_id && a.trg_id < b.trg_id);
}


struct polyhedron_triangulated_surface_mesh
{
	roi_polyhedron* msh;
	Tree* bvh;
	//map<size_t, apt_real> obj_ovrlp_idx_vol; 		//the indices of those objects in objs_next with which this obj_curr overlaps and telling how large the overlap volume is
	//vector<apt_uint> obj_closeby_idx; 			//the indices of those objects in objs_next which do intersect/overlap/or have a proximity relation to this obj_curr
	//but have at least one point on their surface that lies closer to another point on the surface of objs_curr than proximity
	vector<apt_uint> support;						//indices of points supporting the object inside or on its boundary, (not the polyhedron msh) !
	set<apt_uint> support_dict; 					//set used for log(obj_support_pids_dict.size() tests if two objects share a specific support point
	polyhedron_triangulated_surface_mesh() :
		msh(NULL), bvh(NULL), support(vector<apt_uint>()), support_dict(set<apt_uint>()) {}
	//obj_ovrlp_idx_vol(map<size_t, apt_real>()), obj_closeby_idx(vector<apt_uint>()),
};


size_t hash_pair_to_key( const apt_uint a, const apt_uint b );


size_t hash_pair_to_key( pair<apt_uint, apt_uint> ab );


pair<apt_uint, apt_uint> unhash_key_to_pair( const size_t key );


#endif
