v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-distancer source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigDistancer.h/.cpp
 2. PARAPROBE_DistancerStructs.h/.cpp
 3. PARAPROBE_DistancerHDF5.h/.cpp (obsolete)
 4. PARAPROBE_DistancerXDMF.h/.cpp (obsolete)
 5. PARAPROBE_DistancerTriTreeHdl.h/.cpp
 6. PARAPROBE_DistancerCPU.h/.cpp (obsolete)
 7. PARAPROBE_DistancerGPU.h/.cu (obsolete)
 8. PARAPROBE_DistancerItrvQryDistIfoHdl.h/.cpp
 9. PARAPROBE_DistancerHdl.h/.cpp
10. PARAPROBE_Distancer.cpp
