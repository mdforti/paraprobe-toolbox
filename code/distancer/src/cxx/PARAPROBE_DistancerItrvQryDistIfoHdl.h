/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_DISTANCER_ITRVQRYDISTIFOHDL_H__
#define __PARAPROBE_DISTANCER_ITRVQRYDISTIFOHDL_H__

#include "PARAPROBE_DistancerGPU.h"


class itrvQryTreeHdl;

//#define when you wish to monitor how much time the code spends with detecting individual triangles
//#define MONITOR_TRIANGLE_COUNT

struct dinfo
{
#ifdef MONITOR_TRIANGLE_COUNT
	double dt;
	apt_uint wkld;
#endif
	apt_uint ionID;
	apt_uint triID;
	apt_real d;
#ifdef MONITOR_TRIANGLE_COUNT
	dinfo() : dt(0.), wkld(UMX), ionID(UMX), triID(UMX), d(RMX) {}
	dinfo( const double _dt, const apt_uint _wkld,
			const apt_uint _ionID, const apt_real _d ) :
		dt(_dt), wkld(_wkld), ionID(_ionID), triID(UMX), d(_d) {}
	dinfo( const double _dt, const apt_uint _wkld,
			const apt_uint _ionID, const apt_uint _triID, const apt_real _d ) :
		dt(_dt), wkld(_wkld), ionID(_ionID), triID(_triID), d(_d) {}
#else
	dinfo() : ionID(UMX), triID(UMX), d(RMX) {}
	dinfo( const apt_uint _ionID, const apt_real _d ) :
			ionID(_ionID), triID(UMX), d(_d) {}
	dinfo( const apt_uint _ionID, const apt_uint _triID, const apt_real _d ) :
		ionID(_ionID), triID(_triID), d(_d) {}
#endif
};


class itrvQryDistIfoHdl
{
	//a class for managing a grid of cubic voxels for which
	//coarse distancing information for each voxel is stored
	//to assist computations of point to triangles by giving
	//successively shorter on average becoming query distances
	//to remove number of triangles to test for points
	//within voxels
public:
	itrvQryDistIfoHdl();
	~itrvQryDistIfoHdl();

	void init_voxelization( aabb3d const & box, const apt_real edgelength );
	void point_cloud_voxelize( vector<p3d> const & pts, vector<p3dinfo> const & ifo );
	apt_real get_query_distifo( const size_t vxlID );

	itrvQryTreeHdl* owner;
	voxelgrid gridinfo;
	vector<apt_uint> querycnt;			//how many ions in this bin
	vector<apt_real> queryifo;			//the desired coarse distance to the closest triangle to be used to reduce point-triangle distancing test
	apt_real defaultifo;
};


class itrvQryTreeHdl
{

public:
	itrvQryTreeHdl();
	~itrvQryTreeHdl();

	void compute_distances( tritreeHdl const & tri3dbvh );

	itrvQryDistIfoHdl* previous;
	itrvQryDistIfoHdl* current;

	//##MK::
	/*
	cpu_p3d_to_tri3d_distancer cpu_computor;
	gpu_p3d_to_tri3d_distancer gpu_computor;
	*/
};


#endif
