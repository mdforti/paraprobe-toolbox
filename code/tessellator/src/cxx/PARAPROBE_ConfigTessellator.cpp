/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigTessellator.h"


ostream& operator << (ostream& in, edge_distances_info const & val)
{
	in << "h5fn " << val.h5fn << "\n";
	in << "dsnm " << val.dsnm << "\n";
	return in;
}


ostream& operator << (ostream& in, tess_task_info const & val)
{
	in << "Tessellation task info" << "\n";
	in << "TaskID " << val.taskid << "\n";
	if ( val.hasVolume == true )
		in << "(Voronoi) cell has volume " << "yes" << "\n";
	else
		in << "(Voronoi) cell has volume " << "no" << "\n";
	if ( val.hasNeighbors == true )
		in << "(Voronoi) cell has neighbors " << "yes" << "\n";
	else
		in << "(Voronoi) cell has neighbors " << "no" << "\n";
	if ( val.hasGeometry == true )
		in << "(Voronoi) cell has geometry " << "yes" << "\n";
	else
		in << "(Voronoi) cell has geometry " << "no" << "\n";
	if ( val.hasWeinbergVector == true )
		in << "(Voronoi) cell has Weinberg (p)vector " << "yes" << "\n";
	else
		in << "(Voronoi) cell has Weinberg (p)vector " << "no" << "\n";
	if ( val.hasEdgeThresholds == true )
		in << "(Voronoi) cell has edge thresholds values when a facet becomes part of an edge of the dataset " << "yes" << "\n";
	else
		in << "(Voronoi) cell has edge threshold values when a facet becomes part of an edge of the dataset " << "no" << "\n";
	return in;
}


string ConfigTessellator::InputfileDataset = "";
string ConfigTessellator::ReconstructionDatasetName = "";
string ConfigTessellator::MassToChargeDatasetName = "";
string ConfigTessellator::InputfileIonTypes = "";
string ConfigTessellator::IonTypesGroupName = "";

edge_distances_info ConfigTessellator::InputfileIonToEdgeDistances = edge_distances_info();

WINDOWING_METHOD ConfigTessellator::WindowingMethod = ENTIRE_DATASET;
vector<roi_sphere> ConfigTessellator::WindowingSpheres = vector<roi_sphere>();
vector<roi_rotated_cylinder> ConfigTessellator::WindowingCylinders = vector<roi_rotated_cylinder>();
vector<roi_rotated_cuboid> ConfigTessellator::WindowingCuboids = vector<roi_rotated_cuboid>();
vector<roi_polyhedron> ConfigTessellator::WindowingPolyhedra = vector<roi_polyhedron>();
lival<apt_uint> ConfigTessellator::LinearSubSamplingRange = lival<apt_uint>( 0, 1, UMX );
match_filter<unsigned char> ConfigTessellator::IontypeFilter = match_filter<unsigned char>();
match_filter<unsigned char> ConfigTessellator::HitMultiplicityFilter = match_filter<unsigned char>();

vector<tess_task_info> ConfigTessellator::TessellationTasks = vector<tess_task_info>();

apt_real ConfigTessellator::GuardZoneFactor = 5.0;
apt_int ConfigTessellator::IonsPerBlock = 5;


bool ConfigTessellator::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigSurfacer::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		//##MK::currently this tool assumes and works only with a single analysis task defined

		apt_uint number_of_processes = 1;
		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id);
		for ( apt_uint tskid = 0; tskid < number_of_processes; tskid++ ) {
			//##MK::currently this tool assumes and works only with a single analysis task defined
			cout << "Reading configuration for task " << tskid << "\n";
			//apt_uint proc_id = tskid + 1;

			TessellationTasks = vector<tess_task_info>();
			TessellationTasks.push_back( tess_task_info() );
			TessellationTasks.back().taskid = tskid;

			grpnm = "/entry" + to_string(entry_id) + "/tessellate";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/reconstruction/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileDataset = path_handling( path );
				cout << "InputfileDataset " << InputfileDataset << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";
				path = "";
				if ( h5r.nexus_read( grpnm + "/ranging/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonTypes = path_handling( path );
				cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
				if ( h5r.nexus_read( grpnm + "/ranging/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
		    	cout << "IonTypesGroupName " << IonTypesGroupName << "\n";
			}

			//optional spatial filtering
			WindowingMethod = ENTIRE_DATASET;
			grpnm = "/entry" + to_string(entry_id) + "/tessellate/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				WindowingMethod = UNION_OF_PRIMITIVES;

				grpnm = "/entry" + to_string(entry_id) + "/tessellate/spatial_filter/ellipsoid_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								WindowingSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/tessellate/spatial_filter/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								WindowingCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/tessellate/spatial_filter/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								WindowingCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << WindowingSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << WindowingCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << WindowingCuboids.size() << "\n";
			}
			//bitmask currently not implemented

			//optional sub-sampling filter for ion/evaporation ID
			grpnm = "/entry" + to_string(entry_id) + "/tessellate/evaporation_id_filter";
			if( h5r.nexus_path_exists( grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/linear_range_min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			grpnm = "/entry" + to_string(entry_id) + "/tessellate/iontype_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			grpnm = "/entry" + to_string(entry_id) + "/tessellate/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << HitMultiplicityFilter << "\n";

			InputfileIonToEdgeDistances = edge_distances_info();
			grpnm = "/entry" + to_string(entry_id) + "/tessellate/surface_distance";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToEdgeDistances.h5fn = path_handling( path );
				cout << "InputfileIonToEdgeDistances filename " << InputfileIonToEdgeDistances.h5fn << "\n";
				string dsnm = "";
				if ( h5r.nexus_read( grpnm + "/distance", dsnm ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToEdgeDistances.dsnm = dsnm;
				cout << "InputfileIonToEdgeDistances dataset_name " << InputfileIonToEdgeDistances.dsnm << "\n";
			}

			grpnm = "/entry" + to_string(entry_id) + "/tessellate";
			string method = "";
			if ( h5r.nexus_read( grpnm + "/method", method ) != MYHDF5_SUCCESS ) { return false; }
			if ( method.compare("default") == 0 ) {
				//##MK::set mode
				cout << "Computing Voronoi tessellation of ion point cloud" << "\n";
			}
			else if ( method.compare("control_point") == 0 ) {
				cerr << "Tessellation method control point currently not implemented" << "\n";
				return false;
			}
			else {
				cerr << "Unknown tessellation method !" << "\n";
				return false;
			}

			unsigned char option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_cell_volume", option ) != MYHDF5_SUCCESS ) { return false; }
			TessellationTasks.back().hasVolume = ( option == 0x01 ) ? true : false;
			cout << "has_cell_volume " << (int) option << "\n";

			option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_cell_neighbors", option ) != MYHDF5_SUCCESS ) { return false; }
			TessellationTasks.back().hasNeighbors = ( option == 0x01 ) ? true : false;
			cout << "has_cell_neighbors " << (int) option << "\n";

			option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_cell_geometry", option ) != MYHDF5_SUCCESS ) { return false; }
			TessellationTasks.back().hasGeometry = ( option == 0x01 ) ? true : false;
			cout << "has_cell_geometry " << (int) option << "\n";

			option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_cell_edge_detection", option ) != MYHDF5_SUCCESS ) { return false; }
			TessellationTasks.back().hasEdgeThresholds = ( option == 0x01 ) ? true : false;
			cout << "has_cell_edge_detection " << (int) option << "\n";

			option = 0x00;
			TessellationTasks.back().hasWeinbergVector = ( option == 0x01 ) ? true : false;
			cout << "has_weinberg_vector " << (int) option << "\n";

			cout << TessellationTasks.back() << "\n";
		}

		//sensible defaults
		cout << "GuardZoneFactor " << GuardZoneFactor << "\n";
		cout << "IonsPerBlock " << IonsPerBlock << "\n";

		return true;
	}
	//special developer case SimID == 0

	return false;
}
