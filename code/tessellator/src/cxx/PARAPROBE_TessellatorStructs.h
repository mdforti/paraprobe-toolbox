/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_TESSELLATOR_STRUCTS_H__
#define __PARAPROBE_TESSELLATOR_STRUCTS_H__


#include "PARAPROBE_ConfigTessellator.h"

//cell eventual domain wall contact status codes
#define CELL_WAS_NOT_PROCESSED				0xFF
#define CELL_HAS_GLOBAL_WALLCONTACT			0xBB
#define CELL_HAS_LOCAL_WALLCONTACT			0xAA
#define CELL_HAS_NO_WALLCONTACT				0x00

struct wallinfo
{
	//captures logical information which threadlocal domain walls cut the cell
	bool xmi_touch;				//aka left
	bool xmx_touch;				//aka right
	bool ymi_touch;				//aka front
	bool ymx_touch;				//aka rear
	bool zmi_touch;				//aka bottom
	bool zmx_touch;				//aka top
	bool global_touch;			//if true cell hits global domain wall
	bool local_touch;			//if true cell hits local domain wall, indicating the halo region was not large enough
	//if halo_insufficient == true re-tessellation with larger halo guard width is required
	wallinfo() : 	xmi_touch(false), xmx_touch(false),
					ymi_touch(false), ymx_touch(false),
					zmi_touch(false), zmx_touch(false),
					global_touch(false), local_touch(false) {}
	wallinfo(const bool _xmi, const bool _xmx, const bool _ymi, const bool _ymx,
			const bool _zmi, const bool _zmx, const bool _glo_touch, const bool _loc_touch ) :
				xmi_touch(_xmi), xmx_touch(_xmx), ymi_touch(_ymi), ymx_touch(_ymx),
				zmi_touch(_zmi), zmx_touch(_zmx), global_touch(_glo_touch), local_touch(_loc_touch) {}
};

ostream& operator << (ostream& in, wallinfo const & val);


struct cellinfo
{
	double volume;			//cell volume
	apt_uint evapid;		//evaporation ID of the ion associated with this cell
	apt_real edgedist;		//distance of the ion to the edge, if present, otherwise RMX
	unsigned short rankid;
	unsigned short threadid;
	bool ghost;				//a cell is a ghost if it is the halo region, ghost need not to be computed,
							//they are only kept so that cells in the interior of the guarded zone remain without contact to the
							//domain wall
	bool success;
	wallinfo wallcontact;

	cellinfo() : volume(MYZERO), evapid(UMX), edgedist(RMX), rankid(MASTER), threadid(MASTER),
			ghost(false), success(true), wallcontact(wallinfo()) {}
	cellinfo( const apt_uint _evapid, const apt_real _dist, const unsigned short _mpiid, const unsigned short _ompid, const bool _ghost ) :
			volume(MYZERO), evapid(_evapid), edgedist(_dist), rankid(_mpiid), threadid(_ompid), ghost(_ghost),
			success(true), wallcontact(wallinfo()) {}
	cellinfo( const double _v, const apt_uint _evapid, const apt_real _dist,
			const unsigned short _mpiid, const unsigned short _ompid, const bool _ghost, wallinfo const & _wallifo ) :
			volume(_v), evapid(_evapid), edgedist(_dist), rankid(_mpiid), threadid(_ompid),
			ghost(_ghost), success(true), wallcontact(_wallifo.xmi_touch, _wallifo.xmx_touch,
				_wallifo.ymi_touch, _wallifo.ymx_touch,
				_wallifo.zmi_touch, _wallifo.zmx_touch,
				_wallifo.global_touch, _wallifo.local_touch) {}
};

ostream& operator << (ostream& in, cellinfo const & val);


struct cellgeom
{
	size_t offset_vertices;			//ID offset vertices
	size_t offset_facets;			//ID offset facets
	apt_uint evapid;				//evaporation ID of the ion associated with this cell
	apt_uint n_vertices;
	apt_uint n_facets;

	cellgeom() : offset_vertices(0), offset_facets(0), evapid(UMX), n_vertices(0), n_facets(0) {}
	cellgeom( const size_t _overts, const size_t _ofcts,
			const apt_uint _evapid, const apt_uint _nverts, const apt_uint _nfcts ) :
		offset_vertices(_overts), offset_facets(_ofcts),
		evapid(_evapid), n_vertices(_nverts), n_facets(_nfcts) {}
};

ostream& operator << (ostream& in, cellgeom const & val);

#endif
