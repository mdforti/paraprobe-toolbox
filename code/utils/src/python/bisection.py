#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#


def bisection(array, value):
    """Bisect array for value.

    Given an ``array`` , and given a ``value`` , returns an index j
    such that ``value`` is between array[j]
    and array[j+1]. ``array`` must be monotonic increasing.
    j=-1 or j=len(array) is returned
    to indicate that ``value`` is out of range below and above respectively.
    """
    # taken from https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
    n = len(array)
    if value < array[0]:
        return -1
    if value > array[n - 1]:
        return n
    jl = 0  # initialize lower
    ju = n - 1  # and upper limits
    while ju - jl > 1:  # if we are not yet done,
        jm = (ju + jl) >> 1  # compute a midpoint with a bitshift
        if value >= array[jm]:
            jl = jm  # and replace either the lower limit
        else:
            ju = jm  # or the upper limit, as appropriate
        # repeat until the test condition is satisfied
    if value == array[0]:  # edge cases at bottom
        return 0
    if value == array[n - 1]:  # and top
        return n - 1
    return jl
