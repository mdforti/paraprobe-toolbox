#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Defining numerics and precision

import os
import numpy as np

# iontype handling
UNKNOWNTYPE = 0
MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION = 32


# precision
RMX = np.finfo(np.float32).max
EPSILON = 1.0e-5

TOOLS_IN_TOOLBOX = ["transcoder", "ranger", "selector", "spatstat",
                    "distancer", "surfacer", "tessellator", "intersector",
                    "nanochem", "clusterer"]

SUPPORTED_DATATYPES = [tuple, list, np.ndarray, bool, str, int, float]


def mythrow(mess):
    """Throw customized error message in case of a value error."""
    raise ValueError(f"{mess} does not exist !")


def get_file_size(file_name):
    print(f"{np.around(os.path.getsize(file_name)/1024/1024, decimals=3)} MiB")


def get_std(toolname, jobid):
    return (f"PARAPROBE.{toolname.capitalize()}.SimID.{jobid}.stdout.txt",
            f"PARAPROBE.{toolname.capitalize()}.SimID.{jobid}.stderr.txt")


# hdf five
MYHDF5_COMPRESSION_DEFAULT = 1
