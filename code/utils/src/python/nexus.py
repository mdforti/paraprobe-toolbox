#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Utility functions related to handling NeXus nomad-parser-nexus path formatting.

import re


def specific_nxdl_path(partially_specific_nxdl_path):
    """Forward-iterate along NXDL path and replace instances with specific names."""
    specific_nxdl_path = ""
    tree = partially_specific_nxdl_path.split("/")
    for substring in tree[1:]:
        # extract path to be used at each level e.g. ENTRY[entry1] becomes entry1
        # does the path in this nesting level indicate we have a specific thing?
        matches = re.findall(r"\[.+?\]", substring)
        if matches is None or matches == []:
            specific_nxdl_path += "/" + substring
        else:
            if len(matches) != 1:
                raise ValueError("List matches needs to be only one entry!")
            stripped_substring = matches[0][1:len(matches[0]) - 1]
            if len(stripped_substring) == 0:
                raise ValueError("Sub-string must not be an empty string!")
            specific_nxdl_path += "/" + stripped_substring
    return specific_nxdl_path


def nxdl_path_resolve_parent_of_attribute(specific_nxdl_path):
    """Check which parent owns the attribute, if at all."""
    if specific_nxdl_path.count("@") > 1:
        raise ValueError("NXDL path must not contain more than one @ if at all!")
    last_at = specific_nxdl_path.find("@")
    if last_at > -1:
        return specific_nxdl_path[0:last_at - 1]
    return specific_nxdl_path


def nxdl_path_resolve_name_of_attribute(specific_nxdl_path):
    """Check the name of the attribute, if at all."""
    if specific_nxdl_path.count("@") > 1:
        raise ValueError("NXDL path must not contain more than one @ if at all!")
    last_at = specific_nxdl_path.find("@")
    if last_at > -1:
        return specific_nxdl_path[last_at:]
    return ""


def nxdl_path_resolves_unit(specific_nxdl_path):
    if specific_nxdl_path.count("@") > 1:
        raise ValueError("NXDL path must not contain more than one @ if at all!")
    last_at = specific_nxdl_path.find("@")
    if last_at > -1:
        return specific_nxdl_path[0:last_at - 1]
    return specific_nxdl_path


class NodeInfo():
    """Provide specific datatype of and if specify if entry is required or optional."""

    def __init__(self, dtype=None, occurs=None):
        self.dtype = str  # default NX_CHAR
        self.occurs = "required"  # application definition default is required
        if dtype is not None:
            # check if dtype specifies a datatype
            if not isinstance(dtype, type):
                raise TypeError(f"Argument {dtype} needs to be a type !")
            self.dtype = dtype
        if occurs is not None:
            allowed = ["required", "optional"]
            if occurs not in allowed:
                raise ValueError(f"Argument occurs needs to be in {allowed}!")
            self.occurs = occurs
