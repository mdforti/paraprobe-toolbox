/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_VoxelTree.h"


template <class T>
LinearTimeVolQuerier<T>::LinearTimeVolQuerier()
{
	gridinfo = voxelgrid();
	buckets = vector<vector<T>*>();
}

template LinearTimeVolQuerier<p3dmidx>::LinearTimeVolQuerier();


template <class T>
LinearTimeVolQuerier<T>::LinearTimeVolQuerier( aabb3d const & box, const apt_real _di )
{
	gridinfo = voxelgrid();
	buckets = vector<vector<T>*>();
	/*
	#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
		bucketsinfo.clear();
	#endif
	*/

	gridinfo = voxelgrid( box, _di );
	cout << "LinearTimeVolQuerier constructor" << "\n";
	cout << gridinfo << "\n";

	try {
		buckets = vector<vector<T>*>( gridinfo.nxyz, NULL );
	}
	catch(bad_alloc &croak)
	{
		cerr << "Allocation of buckets for linear time volume querier failed !" << "\n";
		gridinfo = voxelgrid();
		return;
	}

	/*
	#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
		bucketsinfo.reserve( mdat.nxyz );
	#endif
	for( apt_int z = 0; z < gridinfo.nz; ++z ) {	//storing in implicit x,y,z order
		for( apt_int y = 0; y < gridinfo.ny; ++y ) {
			for( apt_int x = 0; x < gridinfo.nx; ++x ) {
				#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
					bucketsinfo.push_back( NULL );
				#endif
			}
		}
	}
	*/

}

template LinearTimeVolQuerier<p3dmidx>::LinearTimeVolQuerier( aabb3d const & box, const apt_real _di );


template <class T>
LinearTimeVolQuerier<T>::~LinearTimeVolQuerier()
{
	gridinfo = voxelgrid();

	for( size_t b = 0; b < buckets.size(); b++ ) {
		delete buckets[b];
		buckets[b] = NULL;
	}
	buckets = vector<vector<T>*>();

	/*
	#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
		nb = bucketsinfo.size();
		for(size_t b = 0; b < nb; ++b) {
			delete bucketsinfo.at(b);
			bucketsinfo.at(b) = NULL;
		}
		//##MK::reset also mdat to when attempting to reutilize data structure
		bucketsinfo.clear();
	#endif
	*/
}

template LinearTimeVolQuerier<p3dmidx>::~LinearTimeVolQuerier();


template <class T>
void LinearTimeVolQuerier<T>::add( T const & p )
{
	p3d here = p3d( p.x, p.y, p.z );
	size_t ixyz = gridinfo.where_xyz( here );
	if ( ixyz != -1 ) {
		if ( buckets[ixyz] != NULL ) { //we fill more in buckets than we have already instantiated than instantiating empty buckets
			buckets[ixyz]->push_back( p );
		}
		else { //instantiate a new bucket
			try {
				buckets[ixyz] = new vector<T>();
				buckets[ixyz]->push_back( p );
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation error when attempting instantiation of bucket " << ixyz << " !" << "\n";
				return;
			}
		}
	}
	else {
		cerr << "LinearwTimeVolQuerier:add logical inconsistence, a point is outside the grid !" << "\n";
	}
}

template void LinearTimeVolQuerier<p3dmidx>::add( p3dmidx const & p );


/*
#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
void spacebucket::add_atom_info( const pos p, const unsigned short ifo )
{
	//is point in box which the buckets discretize?
	if ( p.x >= mdat.box.xmi && p.x <= mdat.box.xmx &&
			p.y >= mdat.box.ymi && p.y <= mdat.box.ymx &&
				p.z >= mdat.box.zmi && p.z <= mdat.box.zmx ) { //most likely case
		//in which bucket?
		size_t xx = floor((p.x - mdat.box.xmi) / mdat.box.xsz * static_cast<apt_xyz>(mdat.nx));
		size_t yy = floor((p.y - mdat.box.ymi) / mdat.box.ysz * static_cast<apt_xyz>(mdat.ny));
		size_t zz = floor((p.z - mdat.box.zmi) / mdat.box.zsz * static_cast<apt_xyz>(mdat.nz));

		size_t thisone = xx + yy*mdat.nx + zz*mdat.nxy;

		if ( buckets.at(thisone) != NULL ) { //most likely case until all buckets have been touched
			buckets.at(thisone)->push_back( p );
		}
		else {
			try {
				buckets.at(thisone) = new vector<pos>;
			}
			catch (bad_alloc &ompcroak) {
				complaining( MASTER, "Allocation error in add atom info to spacebucket atom part"); return;
			}
			buckets.at(thisone)->push_back( p );
		}

		if ( bucketsinfo.at(thisone) != NULL ) {
			bucketsinfo.at(thisone)->push_back( ifo );
		}
		else {
			try {
				bucketsinfo.at(thisone) = new vector<unsigned short>;
			}
			catch (bad_alloc &ompcroak) {
				complaining( MASTER, "Allocation error in add atom info to spacebucket info part"); return;
			}
			bucketsinfo.at(thisone)->push_back( ifo );
		}
	}
	else { //MK::must not be encountered!
		cerr << "Point pos " << p.x << ";" << p.y << ";" << p.z << " not placed into spatialbucket" << endl;
	}
}
#endif
*/


template <class T>
void LinearTimeVolQuerier<T>::query_noclear_nosort( aabb3d & box, vector<T> & candidates )
{
	//does neither clear prior processing nor sort the candidates output array
	apt_int sxmi = gridinfo.where_x( box.xmi );
	apt_int sxmx = gridinfo.where_x( box.xmx );
	apt_int symi = gridinfo.where_y( box.ymi );
	apt_int symx = gridinfo.where_y( box.ymx );
	apt_int szmi = gridinfo.where_z( box.zmi );
	apt_int szmx = gridinfo.where_z( box.zmx );
	if ( sxmi != IMI && sxmi != IMX &&
			sxmx != IMI && sxmx != IMX &&
				symi != IMI && symi != IMX &&
					symx != IMI && symx != IMX &&
						szmi != IMI && szmi != IMX &&
							szmx != IMI && szmx != IMX ) {
		//scan only buckets within [simi, simx]
		for( apt_int iz = szmi; iz <= szmx; iz++) {
			size_t zoff = (size_t) iz * gridinfo.nxy;
			for( apt_int iy = symi; iy <= symx; iy++ ) {
				size_t yzoff = (size_t) iy * gridinfo.nx + zoff;
				for( apt_int ix = sxmi; ix <= sxmx; ix++) {
					size_t thisone = (size_t) ix + yzoff;
					if ( buckets.at(thisone) != NULL ) {
						for( auto it = buckets[thisone]->begin(); it != buckets[thisone]->end(); ++it) {
							if ( box.is_inside( *it ) == true ) {
								candidates.push_back( *it );
							}
						}
					} //done checking all candidates within this bucket
				}
			}
		}
	}
}

template void LinearTimeVolQuerier<p3dmidx>::query_noclear_nosort( aabb3d & box, vector<p3dmidx> & candidates );
