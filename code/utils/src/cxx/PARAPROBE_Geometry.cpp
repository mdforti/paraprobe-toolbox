/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_Geometry.h"


apt_real closestPointOnTriangle( const tri3d face, const p3d src )
{
	/*
		Distance Between Point and Triangle in 3D
		David Eberly
		Geometric Tools, LLC
		http://www.geometrictools.com/
		Copyright
		c 1998-2016. All Rights Reserved.
		Created: September 28, 1999
		Last Modified: March 1, 2008
		vector3 closesPointOnTriangle( const vector3 *triangle, const vector3 &sourcePosition )
	*/
	v3d edge0( face.x2-face.x1, face.y2-face.y1, face.z2-face.z1 );
	v3d edge1( face.x3-face.x1, face.y3-face.y1, face.z3-face.z1 );
	v3d v0( face.x1-src.x, face.y1-src.y, face.z1-src.z );

	apt_real a = SQR(edge0.u) + SQR(edge0.v) + SQR(edge0.w); //edge0.dot( edge0 );
	apt_real b = edge0.u*edge1.u + edge0.v*edge1.v + edge0.w*edge1.w; //edge0.dot( edge1 );
	apt_real c = SQR(edge1.u) + SQR(edge1.v) + SQR(edge1.w); //edge1.dot( edge1 );
	apt_real d = edge0.u*v0.u + edge0.v*v0.v + edge0.w*v0.w; //edge0.dot( v0 );
	apt_real e = edge1.u*v0.u + edge1.v*v0.v + edge1.w*v0.w; //edge1.dot( v0 );

	apt_real det = a*c - b*b;
	apt_real s = b*e - c*d;
	apt_real t = b*d - a*e;

	if ( s + t < det ) {
		if ( s < 0.f ) {
			if ( t < 0.f ) { //region 4
				if ( d < 0.f ) {
					s = CLAMP( -d/a, 0.f, 1.f );
					t = 0.f;
				}
				else {
					s = 0.f;
					t = CLAMP( -e/c, 0.f, 1.f );
				}
			}
			else {//region 3
				s = 0.f;
				t = CLAMP( -e/c, 0.f, 1.f );
			}
		}
		else if ( t < 0.f ) { //region 5
			s = CLAMP( -d/a, 0.f, 1.f );
			t = 0.f;
		}
		else { //region 0
			apt_real invDet = 1.f / det;
			s *= invDet;
			t *= invDet;
		}
	}
	else {
		if ( s < 0.f ) { //region 2
			apt_real tmp0 = b+d;
			apt_real tmp1 = c+e;
			if ( tmp1 > tmp0 ) {
				apt_real numer = tmp1 - tmp0;
				apt_real denom = a-2*b+c;
				s = CLAMP( numer/denom, 0.f, 1.f );
				t = 1-s;
			}
			else {
				t = CLAMP( -e/c, 0.f, 1.f );
				s = 0.f;
			}
		}
		else if ( t < 0.f ) { //region 6
			if ( a+d > b+e ) {
				apt_real numer = c+e-b-d;
				apt_real denom = a-2*b+c;
				s = CLAMP( numer/denom, 0.f, 1.f );
				t = 1-s;
			}
			else {
				s = CLAMP( -e/c, 0.f, 1.f );
				t = 0.f;
			}
		}
		else { //region 1
			apt_real numer = c+e-b-d;
			apt_real denom = a-2*b+c;
			s = CLAMP( numer/denom, 0.f, 1.f );
			t = 1.f - s;
		}
	}

	//closest point is cp
	p3d cp( face.x1 + s*edge0.u + t*edge1.u, face.y1 + s*edge0.v + t*edge1.v, face.z1 + s*edge0.w + t*edge1.w );

	//so return SQR of difference to avoid sqrt in the code
	return ( SQR(cp.x-src.x) + SQR(cp.y-src.y) + SQR(cp.z-src.z) );
}


apt_sphere circumsphere_about_triangle( const tri3d tri )
{
	//compute the center and radius of a sphere which circumscribes the triangle tri
	//##https://en.wikipedia.org/wiki/Circumscribed_circle#Higher_dimensions
	//##MK::use operator overloading
	//bring vertices to origin, x3,y3,z3 as c
	p3d a = p3d( tri.x1 - tri.x3, tri.y1 - tri.x3, tri.z1 - tri.z3 );
	p3d b = p3d( tri.x2 - tri.x3, tri.y2 - tri.x3, tri.z2 - tri.z3 );
	p3d ab = p3d( a.x - b.x, a.y - b.y, a.z - b.z );
	
	apt_real a_nrm = vnorm( a );
	apt_real b_nrm = vnorm( b );
	apt_real ab_nrm = vnorm( ab );
	p3d cr_ab = cross( a, b );
	apt_real cr_ab_nrm = vnorm( cr_ab );
	
	p3d abba = p3d( SQR(a_nrm)*b.x - SQR(b_nrm)*a.x, SQR(a_nrm)*b.y - SQR(b_nrm)*a.y, SQR(a_nrm)*b.z - SQR(b_nrm)*a.z );
	p3d abba_ab = cross( abba, cr_ab );
	
	apt_sphere res = apt_sphere();
	res.x = ( abba_ab.x / (2.0*SQR(cr_ab_nrm)) ) + tri.x3;
	res.y = ( abba_ab.y / (2.0*SQR(cr_ab_nrm)) ) + tri.y3;
	res.z = ( abba_ab.z / (2.0*SQR(cr_ab_nrm)) ) + tri.z3;	
	res.R = ( a_nrm * b_nrm * ab_nrm ) / (2.0 * cr_ab_nrm ) + EPSILON;
	
	return res;	
}


apt_real Normalize( p3d & v, bool robust )
{
	if (robust) {
		apt_real maxAbsComp = fabs(v.x);
		if ( fabs(v.y) > maxAbsComp ) {
			maxAbsComp = fabs(v.y);
		}
		if ( fabs(v.z) > maxAbsComp ) {
			maxAbsComp = fabs(v.z);
		}
		apt_real length = MYZERO; //##MK::
		if (maxAbsComp > (apt_real) MYZERO) {
			v.x /= maxAbsComp;
			v.y /= maxAbsComp;
			v.z /= maxAbsComp;
			length = sqrt(SQR(v.x)+SQR(v.y)+SQR(v.z));
			v.x /= length;
			v.y /= length;
			v.z /= length;
			length *= maxAbsComp;
		}
		else {
			v.x = (apt_real) MYZERO;
			v.y = (apt_real) MYZERO;
			v.z = (apt_real) MYZERO;
		}
		return length;
	}
	else {
		apt_real length = sqrt(SQR(v.x)+SQR(v.y)+SQR(v.z));
		if (length > (apt_real) MYZERO) {
			v.x /= length;
			v.y /= length;
			v.z /= length;
		}
		else {
			v.x = (apt_real) MYZERO;
			v.y = (apt_real) MYZERO;
			v.z = (apt_real) MYZERO;
		}
		return length;
	}
}


apt_real Orthonormalize( apt_int numInputs, vector<p3d> & v, bool robust )
{
	if ( v.size() == 3 && numInputs == 3 ) {
		apt_real minLength = Normalize(v[0], robust);
		for ( apt_int i = 1; i < numInputs; ++i) {
			for ( apt_int j = 0; j < i; ++j) {
				apt_real dotprod = dot(v[i], v[j]);
				v[i].x -= v[j].x * dotprod;
				v[i].y -= v[j].y * dotprod;
				v[i].z -= v[j].z * dotprod;
			}
			apt_real length = Normalize(v[i], robust);
			if (length < minLength) {
				minLength = length;
			}
		}
		return minLength;
	}
	return (apt_real) MYZERO;
}


apt_real ComputeOrthogonalComplement( apt_int numInputs, vector<p3d> & v, bool robust )
{
	// Compute a right-handed orthonormal basis for the orthogonal complement
	// of the input vectors.  The function returns the smallest length of the
	// unnormalized vectors computed during the process.  If this value is
	// nearly zero, it is possible that the inputs are linearly dependent
	// (within numerical round-off errors).  On input, numInputs must be 1 or
	// 2 and v[0] through v[numInputs-1] must be initialized.  On output, the
	// vectors v[0] through v[2] form an orthonormal set.
	if (numInputs == 1) {
		if (fabs(v[0].x) > fabs(v[0].y)) {
			v[1] = p3d(-v[0].z, (apt_real) MYZERO, +v[0].x);
		}
		else {
			v[1] = p3d((apt_real) MYZERO, v[0].z, -v[0].y);
		}
		numInputs = 2;
	}

	if (numInputs == 2) {
		v[2] = cross(v[0], v[1]);
		return Orthonormalize(3, v, robust);
	}

	return (apt_real) MYZERO;
}


bool DiskOverlapsPoint( p2d const & Q, apt_real const & radius)
{
	return dot(Q,Q) <= SQR(radius);
}


bool DiskOverlapsSegment( p2d const & Q0, p2d const & Q1, apt_real const & radius)
{
	apt_real sqrRadius = radius * radius;
	p2d direction = p2d(Q0.x-Q1.x, Q0.y-Q1.y);
	apt_real dotprod = dot(Q0, direction);
	if ( dotprod <= static_cast<apt_real>(MYZERO)) {
		return dot(Q0, Q0) <= sqrRadius;
	}
	apt_real sqrLength = dot(direction, direction);
	if (dotprod >= sqrLength) {
		return dot(Q1, Q1) <= sqrRadius;
	}

	dotprod = dotperp(direction, Q0);
	return dotprod * dotprod <= sqrLength * sqrRadius;
}


bool DiskOverlapsPolygon( size_t numVertices, vector<p2d> const & Q, apt_real const & radius)
{
	// Test whether the polygon contains (0,0).
	apt_real const zero = static_cast<apt_real>(MYZERO);
	size_t positive = 0, negative = 0;
	size_t i0, i1;
	for (i0 = numVertices - 1, i1 = 0; i1 < numVertices; i0 = i1++) {
		apt_real dot = dotperp(Q[i0], p2d(Q[i0].x - Q[i1].x, Q[i0].y - Q[i1].y) );
		if (dot > zero) {
			++positive;
		}
		else if (dot < zero) {
			++negative;
		}
	}
	if (positive == 0 || negative == 0) {
		// The polygon contains (0,0), so the disk and polygon overlap.
		return true;
	}

	// Test whether any edge is overlapped by the polygon.
	for (i0 = numVertices - 1, i1 = 0; i1 < numVertices; i0 = i1++) {
		if (DiskOverlapsSegment(Q[i0], Q[i1], radius)) {
			return true;
		}
	}
	return false;
}


bool intersectTriangleCylinder( const tri3d face, const apt_cylinder cyl )
{
	//check if triangle and cylinder overlap
	//Get a right-handed orthonormal basis from the cylinder axis
	//Eberly defines cylinder as axis as a line starting from the center of the cylinder pointing in the positive direction of the cylinder
	/*
	 Result operator()(Triangle3<Real> const& triangle, Cylinder3<Real> const& cylinder)
	{
		// Get a right-handed orthonormal basis from the cylinder axis
		// direction.
		std::array<Vector3<Real>, 3> basis{};  // {U2,U0,U1}
		basis[0] = cylinder.axis.direction;
		ComputeOrthogonalComplement(1, basis.data());

		// Compute coordinates of the triangle vertices in the coordinate
		// system {C;U0,U1,U2}, where C is the cylinder center and U2 is
		// the cylinder direction. The basis {U0,U1,U2} is orthonormal and
		// right-handed.
		std::array<Vector3<Real>, 3> P{};
		for (size_t i = 0; i < 3; ++i)
		{
			Vector3<Real> delta = triangle.v[i] - cylinder.axis.origin;
			P[i][0] = Dot(basis[1], delta);  // x[i]
			P[i][1] = Dot(basis[2], delta);  // y[i]
			P[i][2] = Dot(basis[0], delta);  // z[i]
		}
	 */

	vector<p3d> basis = vector<p3d>(3, p3d());  // {U2,U0,U1}
	p3d C = p3d(0.5*(cyl.p1.x+cyl.p2.x),0.5*(cyl.p1.y+cyl.p2.y),0.5*(cyl.p1.z+cyl.p2.z)); //this is the new origin
	p3d axis = p3d(cyl.p2.x-cyl.p1.x, cyl.p2.y-cyl.p1.y, cyl.p2.z-cyl.p1.z);
	apt_real SQRlen = SQR(axis.x)+SQR(axis.y)+SQR(axis.z);
	apt_real nrmlen = MYZERO;
	if ( SQRlen >= EPSILON ) {
		apt_real cyl_height = sqrt(SQRlen);
		nrmlen = MYONE / cyl_height;
		p3d axis_direction = p3d(nrmlen*axis.x, nrmlen*axis.y, nrmlen*axis.z);
		basis[0] = axis_direction;
		ComputeOrthogonalComplement(1, basis);

		// Compute coordinates of the triangle vertices in the coordinate
		// system {C;U0,U1,U2}, where C is the cylinder center and U2 is
		// the cylinder direction. The basis {U0,U1,U2} is orthonormal and
		// right-handed.
		//std::array<Vector3<Real>, 3> P{};
		vector<apt_real> P = vector<apt_real>( 3*3, MYZERO );
		p3d delta = p3d();
		delta = p3d(face.x1-C.x,face.y1-C.y,face.z1-C.z);
		P[3*0+0] = dot(basis[1], delta);  // x[i]
		P[3*0+1] = dot(basis[2], delta);  // y[i]
		P[3*0+2] = dot(basis[0], delta);  // z[i]
		delta = p3d(face.x2-C.x,face.y2-C.y,face.z2-C.z);
		P[3*1+0] = dot(basis[1], delta);
		P[3*1+1] = dot(basis[2], delta);
		P[3*1+2] = dot(basis[0], delta);
		delta = p3d(face.x3-C.x,face.y3-C.y,face.z3-C.z);
		P[3*2+0] = dot(basis[1], delta);
		P[3*2+1] = dot(basis[2], delta);
		P[3*2+2] = dot(basis[0], delta);

		// Sort the triangle vertices so that z[0] <= z[1] <= z[2].
		size_t j0, j1, j2;
		if (P[3*0+2] < P[3*1+2]) {
			if (P[3*2+2] < P[3*0+2]) {
				j0 = 2;
				j1 = 0;
				j2 = 1;
			}
			else if (P[3*2+2] < P[3*1+2]) {
				j0 = 0;
				j1 = 2;
				j2 = 1;
			}
			else {
				j0 = 0;
				j1 = 1;
				j2 = 2;
			}
		}
		else {
			if (P[3*2+2] < P[3*1+2]) {
				j0 = 2;
				j1 = 1;
				j2 = 0;
			}
			else if (P[3*2+2] < P[3*0+2]) {
				j0 = 1;
				j1 = 2;
				j2 = 0;
			}
			else {
				j0 = 1;
				j1 = 0;
				j2 = 2;
			}
		}

		apt_real z[3] = { P[3*j0+2], P[3*j1+2], P[3*j2+2] };

		// Maintain the xy-components and z-components separately. The
		// z-components are used for clipping against bottom and top
		// planes of the cylinder. The xy-components are used for
		// disk-containment tests x * x + y * y <= r * r.

		// Attempt an early exit by testing whether the triangle is
		// strictly outside the cylinder slab -h/2 < z < h/2.
		apt_real const hhalf = static_cast<apt_real>(0.5) * cyl_height;
		if (z[2] < -hhalf) {
			// The triangle is strictly below the bottom-disk plane of
			// the cylinder. See case 0a of Figure 1 in the PDF.
			return false;
		}

		if (z[0] > hhalf) {
			// The triangle is strictly above the top-disk plane of the
			// cylinder. See case 0b of Figure 1 in the PDF.
			return false;
		}

		// Project the triangle vertices onto the xy-plane.
		/*std::array<Vector2<apt_real>, 3> Q
		{
			Vector2<apt_real>{ P[j0][0], P[j0][1] },
			Vector2<apt_real>{ P[j1][0], P[j1][1] },
			Vector2<apt_real>{ P[j2][0], P[j2][1] }
		};
		*/
		vector<p2d> Q = vector<p2d>(3, p2d());
		Q[0] = p2d(P[3*j0+0], P[3*j0+1]);
		Q[1] = p2d(P[3*j1+0], P[3*j1+1]);
		Q[2] = p2d(P[3*j2+0], P[3*j2+1]);

		// Attempt an early exit when the triangle does not have to be
		// clipped.
		apt_real const& radius = cyl.R;
		if (-hhalf <= z[0] && z[2] <= hhalf) {
			// The triangle is between the planes of the top-disk and
			// the bottom disk of the cylinder. Determine whether the
			// projection of the triangle onto a plane perpendicular
			// to the cylinder axis overlaps the disk of projection
			// of the cylinder onto the same plane. See case 3a of
			// Figure 1 of the PDF.
			return DiskOverlapsPolygon(3, Q, radius);
		}

		// Clip against |z| <= h/2. At this point we know that z2 >= -h/2
		// and z0 <= h/2 with either z0 < -h/2 or z2 > h/2 or both. The
		// test-intersection query involves testing for overlap between
		// the xy-projection of the clipped triangle and the xy-projection
		// of the cylinder (a disk in the projection plane). The code
		// below computes the vertices of the projection of the clipped
		// triangle. The t-values of the triangle-edge parameterizations
		// satisfy 0 <= t <= 1.
		if (z[0] < -hhalf) {
			if (z[2] > hhalf) {
				if (z[1] >= hhalf) {
					// Cases 4a and 4b of Figure 1 in the PDF.
					//
					// The edge <V0,V1> is parameterized by V0+t*(V1-V0).
					// On the bottom of the slab,
					//   -h/2 = z0 + t * (z1 - z0)
					//   t = (-h/2 - z0) / (z1 - z0) = numerNeg0 / denom10
					// and on the tob of the slab,
					//   +h/2 = z0 + t * (z1 - z0)
					//   t = (+h/2 - z0) / (z1 - z0) = numerPos0 / denom10
					//
					// The edge <V0,V2> is parameterized by V0+t*(V2-V0).
					// On the bottom of the slab,
					//   -h/2 = z0 + t * (z2 - z0)
					//   t = (-h/2 - z0) / (z2 - z0) = numerNeg0 / denom20
					// and on the tob of the slab,
					//   +h/2 = z0 + t * (z2 - z0)
					//   t = (+h/2 - z0) / (z2 - z0) = numerPos0 / denom20
					apt_real numerNeg0 = -hhalf - z[0];
					apt_real numerPos0 = +hhalf - z[0];
					apt_real denom10 = z[1] - z[0];
					apt_real denom20 = z[2] - z[0];
					/*
					Vector2<apt_real> dir20 = (Q[2] - Q[0]) / denom20;
					Vector2<apt_real> dir10 = (Q[1] - Q[0]) / denom10;
					*/
					p2d dir20 = p2d((Q[2].x-Q[0].x)/denom20, (Q[2].y-Q[0].y)/denom20);
					p2d dir10 = p2d((Q[1].x-Q[0].x)/denom10, (Q[1].y-Q[0].y)/denom10);
					/*
					std::array<Vector2<apt_real>, 4> polygon
					{
						Q[0] + numerNeg0 * dir20,
						Q[0] + numerNeg0 * dir10,
						Q[0] + numerPos0 * dir10,
						Q[0] + numerPos0 * dir20
					};
					*/
					vector<p2d> polygon = vector<p2d>(4, p2d());
					polygon[0] = p2d(Q[0].x + numerNeg0 * dir20.x, Q[0].y + numerNeg0 * dir20.y);
					polygon[1] = p2d(Q[0].x + numerNeg0 * dir10.x, Q[0].y + numerNeg0 * dir10.y);
					polygon[2] = p2d(Q[0].x + numerPos0 * dir10.x, Q[0].y + numerPos0 * dir10.y);
					polygon[3] = p2d(Q[0].x + numerPos0 * dir20.x, Q[0].y + numerPos0 * dir20.y);

					return DiskOverlapsPolygon(4, polygon, radius);
				}
				else if (z[1] <= -hhalf) {
					// Cases 4c and 4d of Figure 1 of the PDF.
					//
					// The edge <V2,V0> is parameterized by V0+t*(V2-V0).
					// On the bottom of the slab,
					//   -h/2 = z2 + t * (z0 - z2)
					//   t = (-h/2 - z2) / (z0 - z2) = numerNeg2 / denom02
					// and on the tob of the slab,
					//   +h/2 = z2 + t * (z0 - z2)
					//   t = (+h/2 - z2) / (z0 - z2) = numerPos2 / denom02
					//
					// The edge <V2,V1> is parameterized by V2+t*(V1-V2).
					// On the bottom of the slab,
					//   -h/2 = z2 + t * (z1 - z2)
					//   t = (-h/2 - z2) / (z1 - z2) = numerNeg2 / denom12
					// and on the top of the slab,
					//   +h/2 = z2 + t * (z1 - z2)
					//   t = (+h/2 - z2) / (z1 - z2) = numerPos2 / denom12
					apt_real numerNeg2 = -hhalf - z[2];
					apt_real numerPos2 = +hhalf - z[2];
					apt_real denom02 = z[0] - z[2];
					apt_real denom12 = z[1] - z[2];
					/*
					Vector2<apt_real> dir02 = (Q[0] - Q[2]) / denom02;
					Vector2<apt_real> dir12 = (Q[1] - Q[2]) / denom12;
					*/
					p2d dir02 = p2d((Q[0].x-Q[2].x)/denom02, (Q[0].y-Q[2].y)/denom02);
					p2d dir12 = p2d((Q[1].x-Q[2].x)/denom12, (Q[1].y-Q[2].y)/denom12);
					/*
					std::array<Vector2<apt_real>, 4> polygon
					{
						Q[2] + numerNeg2 * dir02,
						Q[2] + numerNeg2 * dir12,
						Q[2] + numerPos2 * dir12,
						Q[2] + numerPos2 * dir02
					};
					*/
					vector<p2d> polygon = vector<p2d>(4, p2d());
					polygon[0] = p2d(Q[2].x + numerNeg2 * dir02.x, Q[2].y + numerNeg2 * dir02.y);
					polygon[1] = p2d(Q[2].x + numerNeg2 * dir12.x, Q[2].y + numerNeg2 * dir12.y);
					polygon[2] = p2d(Q[2].x + numerPos2 * dir12.x, Q[2].y + numerPos2 * dir12.y);
					polygon[3] = p2d(Q[2].x + numerPos2 * dir02.x, Q[2].y + numerPos2 * dir02.y);
					return DiskOverlapsPolygon(4, polygon, radius);
				}
				else { // -hhalf < z[1] < hhalf
					// Case 5 of Figure 1 of the PDF.
					//
					// The edge <V0,V2> is parameterized by V0+t*(V2-V0).
					// On the bottom of the slab,
					//   -h/2 = z0 + t * (z2 - z0)
					//   t = (-h/2 - z0) / (z2 - z0) = numerNeg0 / denom20
					// and on the tob of the slab,
					//   +h/2 = z0 + t * (z2 - z0)
					//   t = (+h/2 - z0) / (z2 - z0) = numerPos0 / denom20
					//
					// The edge <V1,V0> is parameterized by V1+t*(V0-V1).
					// On the bottom of the slab,
					//   -h/2 = z1 + t * (z0 - z1)
					//   t = (-h/2 - z1) / (z0 - z1) = numerNeg1 / denom01
					//
					// The edge <V1,V2> is parameterized by V1+t*(V2-V1).
					// On the top of the slab,
					//   +h/2 = z1 + t * (z2 - z1)
					//   t = (+h/2 - z1) / (z2 - z1) = numerPos1 / denom21
					apt_real numerNeg0 = -hhalf - z[0];
					apt_real numerPos0 = +hhalf - z[0];
					apt_real numerNeg1 = -hhalf - z[1];
					apt_real numerPos1 = +hhalf - z[1];
					apt_real denom20 = z[2] - z[0];
					apt_real denom01 = z[0] - z[1];
					apt_real denom21 = z[2] - z[1];
					/*
					Vector2<apt_real> dir20 = (Q[2] - Q[0]) / denom20;
					Vector2<apt_real> dir01 = (Q[0] - Q[1]) / denom01;
					Vector2<apt_real> dir21 = (Q[2] - Q[1]) / denom21;
					*/
					p2d dir20 = p2d((Q[2].x-Q[0].x)/denom20, (Q[2].y-Q[0].y)/denom20);
					p2d dir01 = p2d((Q[0].x-Q[1].x)/denom01, (Q[0].y-Q[1].y)/denom01);
					p2d dir21 = p2d((Q[2].x-Q[1].x)/denom21, (Q[2].y-Q[1].y)/denom21);
					/*
					std::array<Vector2<apt_real>, 5> polygon
					{
						Q[0] + numerNeg0 * dir20,
						Q[1] + numerNeg1 * dir01,
						Q[1],
						Q[1] + numerPos1 * dir21,
						Q[0] + numerPos0 * dir20
					};
					*/
					vector<p2d> polygon = vector<p2d>(5, p2d());
					polygon[0] = p2d(Q[0].x + numerNeg0 * dir20.x, Q[0].y + numerNeg0 * dir20.y);
					polygon[1] = p2d(Q[1].x + numerNeg1 * dir01.x, Q[1].y + numerNeg1 * dir01.y);
					polygon[2] = p2d(Q[1].x                      , Q[1].y                      );
					polygon[3] = p2d(Q[1].x + numerPos1 * dir21.x, Q[1].y + numerPos1 * dir21.y);
					polygon[4] = p2d(Q[0].x + numerPos0 * dir20.x, Q[0].y + numerPos0 * dir20.y);
					return DiskOverlapsPolygon(5, polygon, radius);
				}
			}
			else if (z[2] > -hhalf) {
				if (z[1] <= -hhalf) {
					// Cases 3b and 3c of Figure 1 of the PDF.
					//
					// The edge <V2,V0> is parameterized by V2+t*(V0-V2).
					// On the bottom of the slab,
					//   -h/2 = z2 + t * (z0 - z2)
					//   t = (-h/2 - z2) / (z0 - z2) = numerNeg2 / denom02
					//
					// The edge <V2,V1> is parameterized by V2+t*(V1-V2).
					// On the bottom of the slab,
					//   -h/2 = z2 + t * (z1 - z2)
					//   t = (-h/2 - z2) / (z1 - z2) = numerNeg2 / denom12
					apt_real numerNeg2 = -hhalf - z[2];
					apt_real denom02 = z[0] - z[2];
					apt_real denom12 = z[1] - z[2];
					/*
					Vector2<apt_real> dir02 = (Q[0] - Q[2]) / denom02;
					Vector2<apt_real> dir12 = (Q[1] - Q[2]) / denom12;
					*/
					p2d dir02 = p2d((Q[0].x-Q[2].x)/denom02, (Q[0].y-Q[2].y)/denom02);
					p2d dir12 = p2d((Q[1].x-Q[2].x)/denom12, (Q[1].y-Q[2].y)/denom12);
					/*
					std::array<Vector2<apt_real>, 3> polygon
					{
						Q[2],
						Q[2] + numerNeg2 * dir02,
						Q[2] + numerNeg2 * dir12
					};
					*/
					vector<p2d> polygon = vector<p2d>(3, p2d());
					polygon[0] = p2d(Q[2].x                      , Q[2].y                      );
					polygon[1] = p2d(Q[2].x + numerNeg2 * dir02.x, Q[2].y + numerNeg2 * dir02.y);
					polygon[2] = p2d(Q[2].x + numerNeg2 * dir12.x, Q[2].y + numerNeg2 * dir12.y);
					return DiskOverlapsPolygon(3, polygon, radius);
				}
				else { // z[1] > -hhalf
					// Case 4e of Figure 1 of the PDF.
					//
					// The edge <V0,V1> is parameterized by V0+t*(V1-V0).
					// On the bottom of the slab,
					//   -h/2 = z0 + t * (z1 - z0)
					//   t = (-h/2 - z0) / (z1 - z0) = numerNeg0 / denom10
					//
					// The edge <V0,V2> is parameterized by V0+t*(V2-V0).
					// On the bottom of the slab,
					//   -h/2 = z0 + t * (z2 - z0)
					//   t = (-h/2 - z0) / (z2 - z0) = numerNeg0 / denom20
					apt_real numerNeg0 = -hhalf - z[0];
					apt_real denom10 = z[1] - z[0];
					apt_real denom20 = z[2] - z[0];
					/*
					Vector2<apt_real> dir20 = (Q[2] - Q[0]) / denom20;
					Vector2<apt_real> dir10 = (Q[1] - Q[0]) / denom10;
					*/
					p2d dir20 = p2d((Q[2].x-Q[0].x)/denom20, (Q[2].y-Q[0].y)/denom20);
					p2d dir10 = p2d((Q[1].x-Q[0].x)/denom10, (Q[1].y-Q[0].y)/denom10);
					/*
					std::array<Vector2<apt_real>, 4> polygon
					{
						Q[0] + numerNeg0 * dir20,
						Q[0] + numerNeg0 * dir10,
						Q[1],
						Q[2]
					};
					*/
					vector<p2d> polygon = vector<p2d>(4, p2d());
					polygon[0] = p2d(Q[0].x + numerNeg0 * dir20.x, Q[0].y + numerNeg0 * dir20.y);
					polygon[1] = p2d(Q[0].x + numerNeg0 * dir10.x, Q[0].y + numerNeg0 * dir10.y);
					polygon[2] = p2d(Q[1].x                      , Q[1].y                      );
					polygon[3] = p2d(Q[2].x                      , Q[2].y                      );
					return DiskOverlapsPolygon(4, polygon, radius);
				}
			}
			else  { // z[2] == -hhalf
				if (z[1] < -hhalf) {
					// Case 1a of Figure 1 of the PDF.
					return DiskOverlapsPoint(Q[2], radius);
				}
				else {
					// Case 2a of Figure 1 of the PDF.
					return DiskOverlapsSegment(Q[1], Q[2], radius);
				}
			}
		}
		else if (z[0] < hhalf) {
			if (z[1] >= hhalf) {
				// Cases 3d and 3e of Figure 1 of the PDF.
				//
				// The edge <V0,V1> is parameterized by V0+t*(V1-V0).
				// On the top of the slab,
				//   +h/2 = z0 + t * (z1 - z0)
				//   t = (+h/2 - z0) / (z1 - z0) = numerPos0 / denom10
				//
				// The edge <V0,V2> is parameterized by V0+t*(V2-V0).
				// On the top of the slab,
				//   +h/2 = z0 + t * (z2 - z0)
				//   t = (+h/2 - z0) / (z2 - z0) = numerPos0 / denom20
				apt_real numerPos0 = +hhalf - z[0];
				apt_real denom10 = z[1] - z[0];
				apt_real denom20 = z[2] - z[0];
				/*
				Vector2<apt_real> dir10 = (Q[1] - Q[0]) / denom10;
				Vector2<apt_real> dir20 = (Q[2] - Q[0]) / denom20;
				*/
				p2d dir10 = p2d((Q[1].x-Q[0].x)/denom10, (Q[1].y-Q[0].y)/denom10);
				p2d dir20 = p2d((Q[2].x-Q[0].x)/denom20, (Q[2].y-Q[0].y)/denom20);
				/*
				std::array<Vector2<apt_real>, 3> polygon
				{
					Q[0],
					Q[0] + numerPos0 * dir10,
					Q[0] + numerPos0 * dir20
				};
				*/
				vector<p2d> polygon = vector<p2d>(3, p2d());
				polygon[0] = p2d(Q[0].x                      , Q[0].y                      );
				polygon[1] = p2d(Q[0].x + numerPos0 * dir10.x, Q[0].y + numerPos0 * dir10.y);
				polygon[2] = p2d(Q[0].x + numerPos0 * dir20.x, Q[0].y + numerPos0 * dir20.y);
				return DiskOverlapsPolygon(3, polygon, radius);
			}
			else { // z[1] < hhalf
				// Case 4f of Figure 1 of the PDF.
				//
				// The edge <V2,V0> is parameterized by V2+t*(V0-V2).
				// On the top of the slab,
				//   +h/2 = z2 + t * (z0 - z2)
				//   t = (+h/2 - z2) / (z0 - z2) = numerPos2 / denom02
				//
				// The edge <V2,V1> is parameterized by V2+t*(V1-V2).
				// On the top of the slab,
				//   +h/2 = z2 + t * (z1 - z2)
				//   t = (+h/2 - z2) / (z1 - z2) = numerPos2 / denom12
				apt_real numerPos2 = +hhalf - z[2];
				apt_real denom02 = z[0] - z[2];
				apt_real denom12 = z[1] - z[2];
				/*
				Vector2<apt_real> dir02 = (Q[0] - Q[2]) / denom02;
				Vector2<apt_real> dir12 = (Q[1] - Q[2]) / denom12;
				*/
				p2d dir02 = p2d((Q[0].x-Q[2].x)/denom02, (Q[0].y-Q[2].y)/denom02);
				p2d dir12 = p2d((Q[1].x-Q[2].x)/denom12, (Q[1].y-Q[2].y)/denom12);
				/*
				std::array<Vector2<apt_real>, 4> polygon
				{
					Q[0],
					Q[1],
					Q[2] + numerPos2 * dir12,
					Q[2] + numerPos2 * dir02
				};
				*/
				vector<p2d> polygon = vector<p2d>(4, p2d());
				polygon[0] = p2d(Q[0].x                      , Q[0].y                      );
				polygon[1] = p2d(Q[1].x                      , Q[1].y                      );
				polygon[2] = p2d(Q[2].x + numerPos2 * dir12.x, Q[2].y + numerPos2 * dir12.y);
				polygon[3] = p2d(Q[2].x + numerPos2 * dir02.x, Q[2].y + numerPos2 * dir02.y);
				return DiskOverlapsPolygon(4, polygon, radius);
			}
		}
		else { // z[0] == hhalf
			if (z[1] > hhalf) {
				// Case 1b of Figure 1 of the PDF.
				return DiskOverlapsPoint(Q[0], radius);
			}
			else {
				// Case 2b of Figure 1 of the PDF.
				return DiskOverlapsSegment(Q[0], Q[1], radius);
			}
		}

		return false;
	}

	cerr << "intersectTriangleCylinder trying to work with a very short cylinder height, which is not stable !" << "\n";
	return false;
}
