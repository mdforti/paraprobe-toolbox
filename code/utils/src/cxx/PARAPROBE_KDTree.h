/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_KDTREE_H__
#define __PARAPROBE_UTILS_KDTREE_H__

#include "PARAPROBE_AABBTree.h"


//MK::https://programmizm.sourceforge.io/blog/2011/a-practical-implementation-of-kd-trees

template <typename T, typename U>
inline apt_real euclidean_sqrd( const T & a, const U & b );


struct kd_cuboid
{
	p3d min;
	p3d max;
	kd_cuboid();
	kd_cuboid( const p3d & p1, const p3d & p2 );
	template<typename T> apt_real outside_proximity(const T & p );
};


struct kd_node
{
	apt_uint i0;
	apt_uint i1;
	apt_uint split;
	apt_real splitpos;

	kd_node() : i0(UMX), i1(UMX), split(UMX), splitpos(numeric_limits<apt_real>::max()) {}

	friend bool is_leaf(const kd_node & n)
	{
		return n.split == UMX;
	}
	friend size_t npoints( const kd_node & n )
	{
		return (n.i1 - n.i0) + 1;
	}
};


struct kd_build_task
{
	apt_uint first;
	apt_uint last;
	apt_uint node;
	apt_uint dim;

	kd_build_task() : first(0), last(0), node(UMX), dim(0) {}
	kd_build_task( const apt_uint _first, const apt_uint _last, const apt_uint _node, const apt_uint _dim ) :
		first(_first), last(_last), node(_node), dim(_dim) {}
};


struct kd_traverse_task
{
	kd_cuboid bx;
	apt_uint node;
	apt_uint dim;
	apt_real d;
	kd_traverse_task();
	kd_traverse_task( kd_cuboid _bx, apt_uint _node, apt_uint _dim, apt_real _d );
	~kd_traverse_task();
};


/*
template<typename T>
struct kd_scan_task
{
	kd_cuboid<T> bx;
	apt_uint node;
	apt_uint dim;
	kd_scan_task();
	~kd_scan_task();
};
*/


template<typename T>
inline void kd_expand( p3d & min, p3d & max, const T & p );


template <typename T>
struct kd_lower_x
{
	kd_lower_x( const vector<T> & p );
	bool operator()( size_t i1, size_t i2 ) const;
	const vector<T> & points;
};


template <typename T>
struct kd_lower_y
{
	kd_lower_y( const vector<T> & p );
	bool operator()( size_t i1, size_t i2 ) const;
	const vector<T> & points;
};


template <typename T>
struct kd_lower_z
{
	kd_lower_z( const vector<T> & p );
	bool operator()( size_t i1, size_t i2 ) const;
	const vector<T> & points;
};


struct sqrd_mltply
{
	apt_real sqrd;		//squared distance
	apt_uint mult;		//multiplicity

	sqrd_mltply() : sqrd(RMX), mult(0) {}
	sqrd_mltply( const apt_real _sqrd, const apt_uint _mult) : sqrd(_sqrd), mult(_mult) {}
};


inline bool SortAscSquaredDistance( const sqrd_mltply & a, const sqrd_mltply & b )
{
	return a.sqrd < b.sqrd;
}


//the generic kdTree can operate on
//template parameter T specifies which struct point (with mark data) objects the tree holds (p3d, p3dm1, or p3dm5)
//template parameter U specifies the type of object used for queries (p3d, p3dm5)
//template parameter V specifies the return type of the query function
template <typename T>
class kdTree
{
public:
	kdTree();
	~kdTree();

	void kd_build(
		vector<T> const & pts, vector<apt_uint> & prms, const apt_uint leaf_size, const apt_uint mark = UNKNOWNTYPE );
	void kd_pack(
		vector<T> const & pts, vector<apt_uint> const & prms );
	bool kd_verify();

	//append to existent properly initialized collector array
	void kd_query_exclude_target( const T target, const apt_real sqrR, vector<apt_real> & cand );
	//returns squared distance
	void kd_query_exclude_target( const T target, const apt_real sqrR, vector<sqrd_mltply> & cand );
	//returns squared distance and target multiplicity
	void kd_query_exclude_target( const T target, const apt_real sqrR, vector<p3d> & cand );
	//returns squared distance and target multiplicity


	vector<kd_node> nodes;
	vector<T> points;
	aabb3d domain;
	apt_uint m1;
};


/*
struct kd_tree
{
	p3d min;
	p3d max;
	vector<node> nodes;

	kd_tree() : min(p3d()), max(p3d()) {}
	void build( vector<p3d> const & points, vector<size_t> & permutate, const size_t leaf_size );
	void pack( vector<size_t> const & permutate, vector<p3d> const & in, vector<p3d> & out );

	void range_rball_append_external(
			const p3d target, vector<p3d> const & sortedpoints,
			const apt_real radius_sqrd, vector<p3d> & out );

	inline p3d get_min();
	inline p3d get_max();

	bool verify_p3d(vector<p3d> const & sortedpoints);

	void get_allboundingboxes( vector<scan_task> & out );
	void display_nodes();
	size_t get_treememory_consumption();
};
*/


#endif
