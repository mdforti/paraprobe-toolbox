/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PARAPROBE_UTILS_OPENSSLINTERFACE_H_
#define PARAPROBE_UTILS_OPENSSLINTERFACE_H_

#include "PARAPROBE_ArgsAndGitSha.h"

//##MK::https://gist.github.com/arrieta/7d2e196c40514d8b5e9031f2535064fc
#include <cstdlib>
/*
#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdexcept>
*/

// OpenSSL Library
extern "C" {
#include <openssl/evp.h>
}

string SHA256( const string path );


#endif
