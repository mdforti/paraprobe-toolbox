/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_BOOSTINTERFACE_H__
#define __PARAPROBE_UTILS_BOOSTINTERFACE_H__

#include "PARAPROBE_GPUParallelization.h"

//#define UTILIZE_BOOST
#ifdef UTILIZE_BOOST
	//boost
	#include <boost/algorithm/string.hpp>
	#include <boost/math/special_functions/bessel.hpp>
	using namespace boost;
#else
	//C++ processing of regular expression to avoid having to use boost for now
	#include <regex>
	using std::regex;
	using std::string;
	using std::sregex_token_iterator;
#endif

#endif
