/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_XDMFBASEHDL_H__
#define __PARAPROBE_UTILS_XDMFBASEHDL_H__


#include "PARAPROBE_IonCloudMemory.h"

#define XDMF_HEADER_LINE1				"<?xml version=\"1.0\" ?>"
#define XDMF_HEADER_LINE2				"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>"
#define XDMF_HEADER_LINE3				"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">"

#define WRAPPED_XDMF_SUCCESS				+1
#define WRAPPED_XDMF_IOFAILED				-1

#define XDMF_INTERIOR						0
#define XDMF_EXTERIOR						1

struct xdmf_grid
{
	string name;
	string type;
	xdmf_grid() : name(""), type("Uniform") {}
	xdmf_grid( const string _nm) : name(_nm), type("Uniform") {}
};


struct xdmf_topo
{
	string type;
	string number_of_elements;
	string dimensions;
	string numbertype;
	string precision;
	string format;
	string link;
	xdmf_topo() : type(""), number_of_elements(""), dimensions(""),
			numbertype(""), precision(""), format(""), link("") {}
};


struct xdmf_geom
{
	string geometrytype;
	string dimensions;
	string numbertype;
	string precision;
	string format;
	string link;
	xdmf_geom() : geometrytype(""), dimensions(""),
			numbertype(""), precision(""), format(""), link("") {}
	xdmf_geom( vector<size_t> const & _dims,
			const enum MYHDF5_DATATYPES _dtyp, const string _lnk );
};


struct xdmf_attr
{
	string attrtype;
	string center;
	string name;
	string dimensions;
	string datatype;
	string precision;
	string format;
	string link;
	xdmf_attr() : attrtype(""), center(""), name(""), dimensions(""),
			datatype(""), precision(""), format(""), link("") {}
};


struct xdmf_coll_entry
{
	xdmf_grid grid;
	xdmf_topo topo;
	xdmf_geom geom;
	xdmf_attr attr;
	xdmf_coll_entry() : grid(xdmf_grid()), topo(xdmf_topo()),
			geom(xdmf_geom()), attr(xdmf_attr()) {}
};


class xdmfBaseHdl
{
	//coordinating instance handling all (sequential) writing of XDMF metafiles detailing HDF5 additional metadata
	//for visualization for Paraview or VisIt
public:
	xdmfBaseHdl();
	~xdmfBaseHdl();

	void add_grid_uniform( const string nm );
	void add_topology_triangle( size_t nelem, enum MYHDF5_DATATYPES dtyp, const string h5link );
	void add_topology_mixed( size_t nelem, vector<size_t> const & dims, enum MYHDF5_DATATYPES dtyp, const string h5link );
	void add_geometry_xyz( vector<size_t> const & dims, enum MYHDF5_DATATYPES dtyp, const string h5link );
	void add_attribute_scalar( const string nm, const string center, vector<size_t> const & dims,
			enum MYHDF5_DATATYPES dtyp, const string h5link );
	void add_attribute_vector( const string nm, const string center, vector<size_t> const & dims,
			enum MYHDF5_DATATYPES dtyp, const string h5link );
	void write( const string xmlfn );

	void init_collection();
	void add_collection_entry();
	//always adding to the last
	void add_collection_grid_uniform( const string nm );
	void add_collection_topology_mixed( size_t nelem, vector<size_t> const & dims, enum MYHDF5_DATATYPES dtyp, const string h5link );
	void add_collection_geometry_xyz( vector<size_t> const & dims, enum MYHDF5_DATATYPES dtyp, const string h5link );
	void add_collection_attribute_scalar( const string nm, const string center, vector<size_t> const & dims,
				enum MYHDF5_DATATYPES dtyp, const string h5link );
	void write_collection( const string xmlfn );

	xdmf_grid grid;
	xdmf_topo topo;
	xdmf_geom geom;
	vector<xdmf_attr> attrs;

	//collections
	vector<xdmf_coll_entry> collection;

	ofstream xdmfout;
};


p3d leftmultiply( t3x3 const & a, p3d const & b );


p3d ArbitraryRotate( p3d const & axis, const apt_real theta );


void create_ngonal_prism( p3d const & p1, p3d const & p2, apt_real const & radius,
		vector<apt_real> & vertices_out, vector<apt_uint> & topology_out, const apt_uint _nfacets );

#endif
