/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_BITMANGLING_H__
#define __PARAPROBE_UTILS_BITMANGLING_H__

#include "PARAPROBE_FilePathHandling.h"


//low level stuff for performing bitmasking operations
#include <bitset>
#define BIT_SET(a,b) ((a) |= (1ULL<<(b)))
//in paraprobe-toolbox 0x01 means used/true, 0x00 means filtered out/false

//https://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit
//https://linuxhint.com/bit-masking-cpp/

class BitPacker
{
public:
	BitPacker();
	~BitPacker();

	//pack arrays which store boolean information into a representation
	//which takes a single bit only instead of an unsigned char to reduce
	//the memory consumption
	void bool_to_bitpacked_uint8( bool const * in, const size_t n_in, vector<unsigned char> & out );
	void uint8_to_bitpacked_uint8( vector<unsigned char> const & in, vector<unsigned char> & out );
};

#endif
