/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_CPUPARALLELIZATION_H__
#define __PARAPROBE_UTILS_CPUPARALLELIZATION_H__

#include "PARAPROBE_OSInterface.h"

#include <mpi.h>
#include <omp.h>

#define MASTER									0
#define SINGLETHREADED							1
#define	SINGLEPROCESS							1

#define	MPI_COMM_WORLD_OMP_GET_NUM_THREADS		1

/*
//implicitly performance affecting choices, file read ahead system related settings
#define SEQIO_READ_CACHE						((10)*(1024)*(1024)) //bytes
#define MPIIO_READ_CACHE						((10)*(1024)*(1024)) //bytes

#ifdef EMPLOY_SINGLE_PRECISION
	#define SIMDREGISTER_WIDTH					(8) //elements assuming eight 32 bit floats to fit in 256bit wide SIMD register of contemporary processor
#else
	#define SIMDREGISTER_WIDTH					(4) //256bit can take four 64bit double at a time
#endif
*/

#endif
