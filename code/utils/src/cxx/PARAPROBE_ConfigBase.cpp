/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigBase.h"


WINDOWING_METHOD ConfigBase::WindowingMethod = ENTIRE_DATASET;
vector<roi_sphere> ConfigBase::WindowingSpheres = vector<roi_sphere>();
vector<roi_rotated_cylinder> ConfigBase::WindowingCylinders = vector<roi_rotated_cylinder>();
vector<roi_rotated_cuboid> ConfigBase::WindowingCuboids = vector<roi_rotated_cuboid>();
vector<roi_polyhedron> ConfigBase::WindowingPolyhedra = vector<roi_polyhedron>();

lival<apt_uint> ConfigBase::LinearSubSamplingRange = lival<apt_uint>( 0, 1, UMX );

match_filter<unsigned char> ConfigBase::IontypeFilter = match_filter<unsigned char>();
match_filter<unsigned char> ConfigBase::HitMultiplicityFilter = match_filter<unsigned char>();


void ConfigBase::load_spatial_filter( const string nx5fn, const string prefix )
{
}


void ConfigBase::load_sub_sampling_filter( const string nx5fn, const string prefix )
{
}


void ConfigBase::load_ion_type_filter( const string nx5fn, const string prefix )
{
}


void ConfigBase::load_hit_multiplicity_filter( const string nx5fn, const string prefix )
{
}
