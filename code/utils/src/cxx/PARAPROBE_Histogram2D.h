/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_HISTOGRAMTWOD_H__
#define __PARAPROBE_UTILS_HISTOGRAMTWOD_H__

#include "PARAPROBE_Precipitates.h"


class histogram
{
public :
	//double precision counting
	histogram();
	histogram( lival<double> const & iv );
	~histogram();
	
	void add_inside( const apt_real x, const apt_uint cnt );
	void add_upper( const apt_uint cnt );
	void add_dump_yes( const apt_real x );
	void add_counts( histogram const & hst );
	void normalize();

	double report( const apt_uint b );
	double left( const apt_uint b );
	double center( const apt_uint b );
	double right( const apt_uint b );
	void report();

	double cnts_lowest;
	vector<double> cnts;
	double cnts_highest;
	double cnts_unexpected;

	lival<double> bounds;
	double tmp;
	apt_uint nbins;
};


#endif
