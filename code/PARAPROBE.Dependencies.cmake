cmake_minimum_required(VERSION 3.18.4)

################################################################################################################################
##USER INTERACTION##############################################################################################################
##in this section modifications and local paths need to be modified by the user#################################################
################################################################################################################################

#choose if conda for deployment/production or a local installation with manually compiled C/C++ binaries for developers
#pass -DLOCAL_INSTALL to cmake to 
set(CONDA_INSTALL OFF)
set(LOCAL_INSTALL ON)

#choose which system to use
set(USE_FAIRMAT_DOCKER OFF)
set(USE_WINWSL2_OR_LINUX ON)

#SPECIFY THE HOME/ROOT DIRECTORY OF THIS LOCAL PARAPROBE-TOOLBOX INSTANCE
if(USE_FAIRMAT_DOCKER)
	#for linux-webtop
	# set(MYPROJECTPATH "/paraprobe/paraprobe-toolbox/")
	#for jupyter/datasciencelab
	set(MYPROJECTPATH "/home/jovyan/work/paraprobe/paraprobe-toolbox")
endif()
if(USE_WINWSL2_OR_LINUX)
	# set(MYPROJECTPATH "<<YOURPATH>>/paraprobe-toolbox")
	set(MYPROJECTPATH "${CMAKE_CURRENT_LIST_DIR}/..")
endif()

# GNU compiler if DGNUCOMPILER=ON if OFF use Intel oneAPI compiler

################################################################################################################################
##AUTOMATED SECTION ############################################################################################################
##in this section modifications and local paths need to be modified by the user#################################################
################################################################################################################################

#INTEGRATE THE HASH OF THE PARENT COMMIT FROM WHICH THIS VERSION WAS CREATED
#update GitSHA of the paraprobe repository prior commit, i.e. pre-processing upon compilation pulling last GitSHA
#we cannot update the current GitSHA as this would create a circular dependency, namely once we have commit, i.e. know the file
# set(GITSHA "0")
message([STATUS] "MYGITSHA: ${GITSHA}")
string(REGEX REPLACE "-" "_" MYCLEANGITSHA ${GITSHA}) #'-' is an invalid character in a C preprocessor macro
message([STATUS] "MYCLEANGITSHA: ${MYCLEANGITSHA}")
add_definitions("-DGITSHA=${MYCLEANGITSHA}")

#SPECIFY LOCATION OF UTILITIES
set(MYUTILSPATH "${MYPROJECTPATH}/code/utils/build/CMakeFiles/utils.dir/src/cxx/")

#SPECIFY WHERE THE DEPENDENCIES/LIBRARIES ARE WHICH MOST OF THE TOOLS NEED
#expert users can modify these paths to let them point to other local versions of specific dependencies
set(MYBOOSTPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/boost/boost_1_85_0")
set(MYCGALPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/cgal/CGAL-5.6.1")
set(MYEIGENPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/eigen/eigen-3.4.0")
set(MYFFTWPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/fftw/fftw-3.3.10/localinstallation")
#set(MYGMPPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/gmp/")
#set(MYMPFRPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/mpfr/")
set(MYHDFPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/hdf5/CMake-hdf5-1.14.2/build/HDF5-1.14.2-Linux/HDF_Group/HDF5/1.14.2")
set(MYHDFLINKFLAGS "-L${MYHDFPATH}/lib/ ${MYHDFPATH}/lib/libhdf5_hl.a ${MYHDFPATH}/lib/libhdf5.a ${MYHDFPATH}/lib/libz.a -ldl")
#set(MYTETGENPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/tetgen/tetgen1.6.0")
set(MYVOROXXPATH "${MYPROJECTPATH}/code/thirdparty/mandatory/voroxx/voro")

#DOCUMENT WHICH LEVELS OF PARALLELIZATION THE TOOLS OF THIS VERSION USE
#CPU parallelization: specify that C/C++ paraprobe tools are MPI programs and use OpenMP by default
set(EMPLOY_PARALLELISM_MPI ON)
set(EMPLOY_PARALLELISM_OMP ON)
#GPU parallelization: specify if to use and where to find CUDA to compile in GPU code
set(EMPLOY_PARALLELISM_CUDA OFF)

#SPECIFY THE PATH TO A LOCAL CUDA INSTALLATION TO USE FOR GPU FUNCTIONALITIES
#by default GPU functionalities are switched off
if(EMPLOY_PARALLELISM_CUDA)
	set(MYCUDAHOME "")
	include_directories("${MYCUDAHOME}/inc")
	link_directories("${MYCUDAHOME}/lib64")
	set(MYCUFFTLINKFLAGS "-I${MYCUDAHOME}/inc -L${MYCUDAHOME}/lib64 -lcufft")
else()
	set(MYCUDAHOME "")
	set(MYCUFFTLINKFLAGS "")
endif()
