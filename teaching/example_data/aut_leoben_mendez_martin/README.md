Please download the dataset from here:

Ceguerra, AV (2021) Supplementary material: APT test cases.
Available at http://dx.doi.org/10.25833/3ge0-y420
(retrieved February 23, 2021).

The SHA256 checksum of the reconstructed dataset and range files are:
R21_07575-v02.pos
66bab07cc649236bc63cec29bb67375e226edc4f92fde00313c6f203d43bf4ca

R21_07575-v02.rrng
fee7128f30ed88f7e115ec30fbbd1f3c0962d55772849d702cc3917056f61c93

Once you have the two files you can either use this notebook as to try your
installation and familiarize with the tools or use the notebook as a template
and customize it so that you can create your own analyses.
